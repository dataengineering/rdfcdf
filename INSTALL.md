# Installation to Existing Sesame deployment (http://rdf4j.org/sesame/2.7/docs/users.docbook?view#section-sesame-server-installation).


## Directories:

* target = output of maven, must contain jars and lib folder with dependencies.
* tomcat.dir = the directory of tomcat.
* sesame.dir = the directory of sesame distribution. (Where console bin is located)
* aduna.dir = default $HOME/.aduna

## Compilation

    mvn clean package

## Copy dependency libraries except Sesame:

    cp ${target}/lib/*.jar ${tomcat.dir}/webapps/openrdf-sesame/WEB-INF/lib
    cp ${target}/lib/*.jar ${tomcat.dir}/webapps/openrdf-workbench/WEB-INF/lib

## Copy netcdf toolkit 

    mkdir -p ${tomcat.dir}/webapps/openrdf-sesame/lib
    cp ${target}/netcdf-toolkit-${version}.jar ${tomcat.dir}/webapps/openrdf-sesame/lib
    or if this does not work
    cp ${target}/netcdf-toolkit-${version}.jar ${tomcat.dir}/webapps/openrdf-sesame/WEB-INF/lib

## Restart tomcat

    service tomcat restart

## Copy repository templates and lib for the console

    cp ${target}/classes/sesame-server/templates/* ${aduna.dir}/openrdf-sesame-console/templates
    cp ${target}/netcdf-toolkit-${version}.jar ${sesame.dir}/lib

## Start a console

    sh ${sesame.dir}/bin/console.sh


## Console commands

    > connect http://${ip}:8080/openrdf-sesame.
    > create netcdf.
    --- fill ${netcdfstore_id}, ${netcdfstore_name}, netcdf absolute path, uri base.
    > open ${netcdfstore_name}
    > sparql select * { ?s ?p ?o }.
