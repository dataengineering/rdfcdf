

/*

*/

package nl.wur.alterra.semagrow.file;

 

import java.util.ArrayList;
import java.util.Date;

 

/**

*

* @author Rande001

*/

public class SemagrowQueryBlock  {

    Float east =  null;
    Float west = null;
    Float south = null;
    Float north = null;
    Date from;
    Date to;
    ArrayList<String> files;
    ArrayList<String> vars;

 

    public SemagrowQueryBlock() {

        vars = new ArrayList<>();

        files = new ArrayList<>();

    }

 

    public void addFile(String f) {

        files.add(f);

    }

   

    public void addVar(String f) {

        vars.add(f);

    }

   

    public Float getEast() {

        return east;

    }

 

    public void setEast(Float east) {

        this.east = east;

    }

 

    public Float getWest() {

        return west;

    }

 

    public void setWest(Float west) {

        this.west = west;

    }

 

    public Float getSouth() {

        return south;

   }

 

    public void setSouth(Float south) {

        this.south = south;

    }

 

    public Float getNorth() {

        return north;

    }

 

    public void setNorth(Float north) {

        this.north = north;

    }

 

 

    public String getStartDate() {

        return "todo";

    }

 

    public String getEndDate() {

        return "todo";

    }

 

    public ArrayList<String> getFiles() {

        return files;

    }

 

    public ArrayList<String> getVars() {

        return vars;

    }

   

    /**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}



	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}



	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}



	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}



	public String nice() {

        String s = "SemagrowQueryBlock : " + new java.util.Date(System.currentTimeMillis()).toString() + "\n\n";

       

        s += "Files : \n";

       

        for (String f : getFiles()) {

            s+= "\t->"+f+"\n";

        }

       

        s += "\nVariables : \n";

       

        for (String f : getVars()) {

            s+= "\t->"+f+"\n";

        }

       

        return s;

    }

   

    

    public static SemagrowQueryBlock createTestBlock() {

//        Files

//-----

//

//http://www.trees4future.eu#1417698043005_7 -> biom

//        EPIC model output prepared for ISI-MIP Fasttrack Phase (http://www.pik-potsdam.de/isi-mip/ToU)

//        epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t.nc

       

//http://www.trees4future.eu#1417698043519_12 -? aet

//        EPIC model output prepared for ISI-MIP Fasttrack Phase (http://www.pik-potsdam.de/isi-mip/ToU)

//        epic_hadgem2-es_rcp4p5_ssp2_co2_firr_aet_whe_annual_2005_2099_t.nc

 

//http://www.trees4future.eu#1417698043753_17 -> discharge

//        WaterGAP model output prepared for ISI-MIP Fasttrack Phase (http://www.pik-potsdam.de/isi-mip/ToU)

//        watergap_hadgem2-es_rcp4p5_nosoc_dis_monthly_2005_2099_t.nc

       

//http://www.trees4future.eu#1417698043972_22 -> soil_carbon_content

//        ORCHIDEE model output prepared for ISI-MIP Fasttrack Phase (http://www.pik-potsdam.de/isi-mip/ToU)

//        orchidee_hadgem2-es_rcp8p5_nosoc_co2_csoil_monthly_2005_2099_t.nc

       

//http://www.trees4future.eu#1417698044206_27     -> eastward_wind

//        Bias corrected model input prepared for ISI-MIP Fasttrack Phase (http://www.pik-potsdam.de/isi-mip/ToU)

//        uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.nc

//

//Area

//----

//    50.25     0.25       54.75     4.75 (SWNE)

      

        SemagrowQueryBlock testBlock = new SemagrowQueryBlock();

       

        testBlock.addFile("epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t.nc");

        testBlock.addVar("biom");

       

        testBlock.addFile("epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t.nc");

        testBlock.addVar("epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t_aet");

        return testBlock;

      

      

      

 

    }

}
