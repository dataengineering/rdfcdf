/***************
<p>Title: NetCDFToStatements</p>

<p>Description:
Converts a netcdf file to Sesame Statements.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Giannis Mouchakis (SemaGrow 2014)

***************/


package gr.demokritos.iit.netcdftoolkit.sail;

import gr.demokritos.iit.netcdftoolkit.commons.NetCDFUtils;
import gr.demokritos.iit.netcdftoolkit.commons.NetCDFVocab;
import gr.demokritos.iit.netcdftoolkit.loader.Dataset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.XMLSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;


public class NetCDFToStatements
{
	static Logger logger = LoggerFactory.getLogger(NetCDFToStatements.class);

	private Dataset dataset;
	private boolean inited = false;
	private boolean writtenLat = false;
	private boolean writtenLon = false;
	private String subj_str;
	private String pred_str;
	private String obj_str;
	private boolean fastTriplefication = false;

	public NetCDFToStatements( Dataset dataset, Resource subj, URI pred, Value obj )
	{
		this.dataset = dataset;
		if (subj != null) {
			this.subj_str = subj.stringValue();
		}
		if (pred != null) {
			this.pred_str = pred.stringValue();
		}
		if (obj != null) {
			this.obj_str = obj.stringValue();
		}

	}
	
	public NetCDFToStatements(Dataset dataset, Resource subj, URI pred, Value obj, boolean fastTriplefication) {
		this.dataset = dataset;
		this.fastTriplefication = fastTriplefication;
		if (subj != null) {
			this.subj_str = subj.stringValue();
		}
		if (pred != null) {
			this.pred_str = pred.stringValue();
		}
		if (obj != null) {
			this.obj_str = obj.stringValue();
		}

	}


	public void init()
	throws IOException
	{

		
		this.writtenLat = false;
		this.writtenLon = false;
		
		this.inited = true;
	}


	public void close()
	throws IOException
	{
		
		this.writtenLat = false;
		this.writtenLon = false;
		
	}

	
	public List<Statement> getStatements( NetcdfFile ncfile, String uribase, boolean getObservations )
	throws IOException, InvalidRangeException
	{
		if( ! this.inited ) { this.init(); }
		
		List<Statement> statements = new ArrayList<Statement>();
		
		String dataset_name = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_DATA + uribase);
		String struct_name = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_STRUCT + uribase + "_struct");

		//NetcdfFile ncfile = NetcdfFile.open( netcdf_filename );

		List<Variable> var_list = ncfile.getVariables();
		List<Dimension> dim_list = ncfile.getDimensions();

		List<Variable> dimension_variables = new ArrayList<Variable>();
		List<Variable> measurement_variables = new ArrayList<Variable>();
		List<Array> dimension_data_list = new ArrayList<Array>();

		// read data for each dimension variable and add to the list.
		for (Variable variable : var_list) {
			if (isDimensionVar(variable, dim_list)) {
				// for dimension variables, record their existence
				dimension_variables.add(variable);
				// .. and the array of possible values
				// These values are the allowed indices for each dimension when reading measurements
				dimension_data_list.add( variable.read() );
			}
			else {
				// for measurement variables, only record their existence
				measurement_variables.add(variable);
			}
		}
		
		// writes the main dataset instance and its global attributes
		statements.addAll(addMetadata( ncfile, dataset_name, struct_name, var_list, dimension_variables, dim_list, dimension_data_list, uribase));
		
		/*if ( ! getObservations ) {
			return statements;
		}*/
		
		/*if (measurement_variables.size()>1) {
			logger.error("this does not work for netCDF files with more than one measurement variables");
			System.exit(1);
		}*/
		
		for (Variable measurement_variable : measurement_variables) {
			
			//add dimension dataset and struct 
			String var_dataset = dataset_name + "_" + measurement_variable.getShortName();
			URI var_dataset_uri = new URIImpl(var_dataset);
			String var_dataset_struct = var_dataset + "_struct";
			URI var_dataset_struct_uri = new URIImpl(var_dataset_struct);
			statements.add(new StatementImpl(var_dataset_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Dataset.getURI())));
			statements.add(new StatementImpl(var_dataset_uri, new URIImpl(NetCDFVocab.DatasetStructure.getURI()), var_dataset_struct_uri));
			statements.add(new StatementImpl(var_dataset_struct_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Structure.getURI())));
			String var_name = getVarStructName(measurement_variable, struct_name);
			statements.add(new StatementImpl(var_dataset_struct_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()), new URIImpl(var_name)));
			
			//order dimensions according to measurement variable's dimensions
			List<Variable> dim_var_ordered = new ArrayList<Variable>();
			// These values are the allowed indices for each dimension when reading measurements
			List<Array> dim_data_odered = new ArrayList<Array>();
				
			List<Dimension> dimensions = measurement_variable.getDimensions();
			for (Dimension dimension : dimensions) {
				for (Variable dim_var : dimension_variables) {
					if (dimension.getShortName().equals(dim_var.getShortName())) {
						dim_var_ordered.add(dim_var);
						dim_data_odered.add(dim_var.read());
						//add variable structs as components
						String var_dim_name = getVarStructName(dim_var, struct_name);
						statements.add(new StatementImpl(var_dataset_struct_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()), new URIImpl(var_dim_name)));
					}
				}
			}
			
			String m_prop_name = getPropName(measurement_variable);
			String data_type = NetCDFUtils.getXsdType(measurement_variable.getDataType());
			
			if (fastTriplefication) {//load values in memory
				Array results = measurement_variable.read();
				Long count = 0l;//start from the end to match slow writing counter
				IndexIterator it = results.getIndexIterator();
				while(it.hasNext()) {
					Object result = it.next();
					int[] indices = it.getCurrentCounter();
					logger.debug(Arrays.toString(indices));
					String obs = var_dataset + "_obs_" + count.toString();
					writeVariableValues(indices, dim_var_ordered, 
							measurement_variable, var_dataset, obs, m_prop_name,
							data_type, struct_name, getObservations, result);
						
					count++;
				}
			} else {
			
				int n = dim_data_odered.size();
				int[] indices = new int[n];
				long[] ranges = new long[n];
				int i = 0;
				for (Array array : dim_data_odered) {
					ranges[i] = array.getSize();
					i++;
				}
						
				// writes observations
				// Start off at indices = [0,...,0]
				Integer count = 0;
				do {
					logger.debug( Arrays.toString(indices) );
					String obs = var_dataset + "_obs_" + count.toString();
					statements.addAll(writeVariableValues(indices, dim_var_ordered, 
							measurement_variable, var_dataset, obs, m_prop_name,
							data_type, struct_name, getObservations));
					advanceIndices(n, indices, ranges);
					++count;
				}
				while( ! allMaxed(n, indices, ranges) );
				//do last one
				String obs = var_dataset + "_obs_" + count.toString();
				logger.debug( Arrays.toString(indices) );
				statements.addAll(writeVariableValues(indices, dim_var_ordered, 
						measurement_variable, var_dataset, obs, m_prop_name, data_type, struct_name, getObservations));		
			}
			
		}
		
		//ncfile.close();
		
		return statements;
		
	}
	
	private List<Statement> writeVariableValues( int[] indices,
			List<Variable> sensor_variables, Variable measurement_variable, 
			String dataset_name, String observation_name, String m_prop_name, 
			String data_type, String struct_name , boolean getObservations, Object result)
	throws IOException, InvalidRangeException
	{
		
		List<Statement> statements = new ArrayList<Statement>();
		URI observation_name_uri = new URIImpl(observation_name);
		//URI m_prop_name_uri = new URIImpl(m_prop_name);
		
		int x = 0;
		for (Variable variable : sensor_variables) {
			String p = getPropName(variable);
			String var_name = getVarStructName(variable, struct_name);
			String pos = getPos(var_name, indices[x]);
			if (insertStatement(observation_name, p, pos)) {
				URI p_uri = new URIImpl(p);
				URI pos_uri = new URIImpl(pos);
				statements.add(new StatementImpl(observation_name_uri, p_uri, pos_uri));
			}
			/*String value_str = sensor_variables_data.get(x).getObject(indices[x]).toString();
			if (insertStatement(observation_name, p, value_str)) {
				URI p_uri = new URIImpl(p);
				Literal value = new LiteralImpl(value_str, NetCDFUtils.getDataTypeToXMLSchema(variable.getDataType()));
				statements.add(new StatementImpl(observation_name_uri, p_uri, value));
			}*/
			x++;
		}
		
		if ( ( ! getObservations ) && insertObservation(observation_name, m_prop_name)) {
			
			URI m_prop_name_uri = new URIImpl(m_prop_name);
			
			//write measurement
			URI datatype = NetCDFUtils.getDataTypeToXMLSchema(measurement_variable.getDataType());
			String result_str = result.toString();
			if (obj_str == null) {
				statements.add(new StatementImpl(observation_name_uri, m_prop_name_uri, new LiteralImpl(result_str, datatype)));
			} else if (obj_str.equals(result_str)) {
				statements.add(new StatementImpl(observation_name_uri, m_prop_name_uri, new LiteralImpl(result_str, datatype)));
			}
			
		}
		

		statements.add(new StatementImpl(observation_name_uri, new URIImpl(
				NetCDFVocab.ObservationDataset.getURI()), new URIImpl(dataset_name)));
		
		return statements;
	}

	private List<Statement> writeVariableValues( int[] indices,
			List<Variable> sensor_variables, Variable measurement_variable, 
			String dataset_name, String observation_name, String m_prop_name, 
			String data_type, String struct_name , boolean getObservations)
	throws IOException, InvalidRangeException
	{
		
		List<Statement> statements = new ArrayList<Statement>();
		URI observation_name_uri = new URIImpl(observation_name);
		//URI m_prop_name_uri = new URIImpl(m_prop_name);
		
		int x = 0;
		for (Variable variable : sensor_variables) {
			String p = getPropName(variable);
			String var_name = getVarStructName(variable, struct_name);
			String pos = getPos(var_name, indices[x]);
			if (insertStatement(observation_name, p, pos)) {
				URI p_uri = new URIImpl(p);
				URI pos_uri = new URIImpl(pos);
				statements.add(new StatementImpl(observation_name_uri, p_uri, pos_uri));
			}
			/*String value_str = sensor_variables_data.get(x).getObject(indices[x]).toString();
			if (insertStatement(observation_name, p, value_str)) {
				URI p_uri = new URIImpl(p);
				Literal value = new LiteralImpl(value_str, NetCDFUtils.getDataTypeToXMLSchema(variable.getDataType()));
				statements.add(new StatementImpl(observation_name_uri, p_uri, value));
			}*/
			x++;
		}
		
		if ( ( ! getObservations ) && insertObservation(observation_name, m_prop_name)) {
			
			URI m_prop_name_uri = new URIImpl(m_prop_name);
			
			String sectionSpec = "";//used to read values in the grid
			for (int i = 0; i < indices.length - 1; i++) {
				sectionSpec += indices[i] + ":" + indices[i] + ", ";
			}
			sectionSpec += indices[indices.length - 1] + ":" + indices[indices.length - 1];
			
			//write measurement
			Array result = measurement_variable.read(sectionSpec);
			URI datatype = NetCDFUtils.getDataTypeToXMLSchema(measurement_variable.getDataType());
			String result_str = result.getObject(0).toString();
			if (obj_str == null) {
				statements.add(new StatementImpl(observation_name_uri, m_prop_name_uri, new LiteralImpl(result_str, datatype)));
			} else if (obj_str.equals(result_str)) {
				statements.add(new StatementImpl(observation_name_uri, m_prop_name_uri, new LiteralImpl(result_str, datatype)));
			}
			
		}
		

		statements.add(new StatementImpl(observation_name_uri, new URIImpl(
				NetCDFVocab.ObservationDataset.getURI()), new URIImpl(dataset_name)));
		
		return statements;
	}
	
	
	// Advances 'indices' to the next in sequence.
    private void advanceIndices(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i]+1 == ranges[i]) {
                indices[i] = 0;
            }
            else {
                indices[i] += 1;
                break;
            }
        }

    }

    // Tests if indices are in final position.
    private boolean allMaxed(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i] != ranges[i]-1) {
                return false;
            }
        }
        return true;

    }

 

    /**
     * Private helper for instantiating a dataset and its structure
     * and writing global attributes.
     * 
     * @param ncfile The input NetCDF file handle
     * @param dataset_name
     * @param struct_name
     * @param var_list
     * @param dimension_variables
     * @param dim_list
     * @param dimension_data_list
     * @throws IOException
     */

	private List<Statement> addMetadata(NetcdfFile ncfile,
			String dataset_name, String struct_name, List<Variable> var_list,
			List<Variable> dimension_variables, List<Dimension> dim_list, List<Array> dimension_data_list, String uribase)
			throws IOException {
		
		List<Statement> statements = new ArrayList<Statement>();
		
		URI dataset_name_uri = new URIImpl(dataset_name);
		URI struct_name_uri = new URIImpl(struct_name);
		
		//write file info
		statements.add(new StatementImpl(new URIImpl(NetCDFVocab.File.getURI()), new URIImpl(NetCDFVocab.RDFValue.getURI()), new LiteralImpl(uribase, XMLSchema.STRING)));
		statements.add(new StatementImpl(dataset_name_uri, new URIImpl(NetCDFVocab.FromFile.getURI()), new LiteralImpl(uribase, XMLSchema.STRING)));
		//write dataset and struct
		statements.add(new StatementImpl(dataset_name_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Dataset.getURI())));
		statements.add(new StatementImpl(dataset_name_uri, new URIImpl(NetCDFVocab.DatasetStructure.getURI()), struct_name_uri));
		statements.add(new StatementImpl(struct_name_uri, new URIImpl(NetCDFVocab.RDFType.getURI()),  new URIImpl(NetCDFVocab.Structure.getURI())));
				
		// Write variables
		int x = 0;
		for (Variable variable : var_list) {
			
			String prop = getPropName(variable);
			
			List<Statement> variable_statements = null;

			/*if (prop.equals(NetCDFVocab.prefixMapping.expandPrefix("cf:longitude"))) {
				String var_name = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_STRUCT + this.dataset.toString() + "_longitude");
				statements.add(new StatementImpl(struct_name_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()),  new URIImpl(var_name)));

				if (!writtenLon) {
					variable_statements = writeVariable(var_name, prop, struct_name, 0, variable, dimension_variables, dim_list, dimension_data_list);
					writtenLon = true;
				}
			} else if (prop.equals(NetCDFVocab.prefixMapping.expandPrefix("cf:latitude"))) {
				String var_name = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_STRUCT + this.dataset.toString() + "_latitude");
				statements.add(new StatementImpl(struct_name_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()),  new URIImpl(var_name)));
				if (!writtenLat) {
					variable_statements = writeVariable(var_name, prop, struct_name, 1, variable, dimension_variables, dim_list, dimension_data_list);
					writtenLat = true;
				}
			} else {*/
				String var_name = getVarStructName(variable, struct_name);
				statements.add(new StatementImpl(struct_name_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()),  new URIImpl(var_name)));
				variable_statements = writeVariable(var_name, prop, struct_name, x, variable, dimension_variables, dim_list, dimension_data_list);
			//}
			
			statements.addAll(variable_statements);
			
			x++;
		}
		
		List<Statement> global_att_statements = writeGlobalAttributes(ncfile, dataset_name, struct_name);
		statements.addAll(global_att_statements);
		
		return statements;
		
    }
	
 
    /**
     * Private helper checking if a variable is a dimension.
     * 
     * @param variable
     * @param dim_list
     */
	private boolean isDimensionVar(Variable variable, List<Dimension> dim_list)
	{
		/*for( Dimension dimension : dim_list ) {
			String dimName = dimension.getShortName();
			if( (dimName != null) && dimName.equals(variable.getShortName()) )
			{ return true; }
		}
		return false;*/
		
		if (variable.getRank() < 2) {
			return true;
		} else return false;
	}
	
	/**
	 * 
	 * method to get the property name for a variable. 
	 * 
	 * @param variable
	 * @return the property name. if variable has standard name then 
	 * name=cf:&lt;stantard_name&gt; else name=sgdata:&lt;short_name&gt;
	 */
	public static String getPropName(Variable variable) {
		String prop = null;
		Attribute name_attr = variable.findAttribute("standard_name");
		if (name_attr == null) {
			prop = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_DATA + variable.getShortName());
		} else {
			prop = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.CF + name_attr.getStringValue());
		}
		return prop;
	}
	
	
	private List<Statement> writeVariable(String var_name, String prop, String struct_name, int order, Variable variable, List<Variable> dimension_variables, List<Dimension> dim_list, List<Array> dimension_data_list) throws IOException {
				
		//out.write("\n" + struct_name + " " + NetCDFVocab.strStructureVariable + " " + var_name + " .");
		
		List<Statement> statements = new ArrayList<Statement>();
		
		URI var_name_uri = new URIImpl(var_name);
		URI prop_uri = new URIImpl(prop);
		
		statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Variable.getURI())));
		statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.VariableOrder.getURI()), new LiteralImpl(((Integer)order).toString(), XMLSchema.INT)));
		statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.VariableProperty.getURI()), prop_uri));


		if (isDimensionVar(variable, dim_list)) {
			statements.add(new StatementImpl( prop_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Dimension.getURI())));
		} else {
			statements.add(new StatementImpl( prop_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Measure.getURI())));
		}
		statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.VariableRequired.getURI()), new LiteralImpl("1", XMLSchema.BOOLEAN)));
		statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.VariableAttachment.getURI()), new URIImpl(NetCDFVocab.Dataset.getURI())));
		statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.ShortName.getURI()), new LiteralImpl(variable.getShortName(), XMLSchema.STRING)));
		
		//datatype
		String type = NetCDFUtils.getDataTypeToString(variable.getDataType());
		String typePrefix = type + ":";
		statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.DataType.getURI()), new URIImpl(NetCDFVocab.prefixMapping.expandPrefix(typePrefix))));
		
		// Codelist
		int index = dimension_variables.indexOf(variable);
		if (index != -1) {//if variable is dimension
			
			// make up codelist instance name
			String cl = var_name + "_codelist";
			URI cl_uri = new URIImpl(cl);
			statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.VariableCodelist.getURI()), cl_uri));
			
			//String clr = "\n" + cl + " " + NetCDFVocab.strCodelistRoot + " ";
			
			Array var_values = dimension_data_list.get(index);
			Long size = var_values.getSize();
			for (Integer i = 0; i < size; i++) {			
				String value = var_values.getObject(i).toString();
				String pos = getPos(var_name, i);
				URI pos_uri = new URIImpl(pos);
				statements.add(new StatementImpl(cl_uri, new URIImpl(NetCDFVocab.CodelistRoot.getURI()), pos_uri));
				URI datatype = NetCDFUtils.getDataTypeToXMLSchema(variable.getDataType());
				statements.add(new StatementImpl(pos_uri, new URIImpl(NetCDFVocab.RDFValue.getURI()), new LiteralImpl(value, datatype)));
				statements.add(new StatementImpl(pos_uri, new URIImpl(NetCDFVocab.Index.getURI()), new LiteralImpl(i.toString(), XMLSchema.LONG)));
				/*if (insertStatement(cl, NetCDFVocab.CodelistRoot.getURI(), NetCDFVocab.prefixMapping.expandPrefix(typePrefix + value))) {
					statements.add(new StatementImpl(cl_uri, new URIImpl(NetCDFVocab.CodelistRoot.getURI()), new URIImpl(NetCDFVocab.prefixMapping.expandPrefix(typePrefix + value))));
				}*/
			}
			statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.DimensionSize.getURI()), new LiteralImpl(size.toString(), XMLSchema.LONG)));
				
		}
		
		List<Statement> vat_attr_statements = writeVariableAttributes(variable, var_name);
		statements.addAll(vat_attr_statements);
		
		return statements;
		
	}
	
	private List<Statement> writeGlobalAttributes(NetcdfFile ncfile, String dataset_name, String struct_name) throws IOException {
		
		List<Attribute> attribute_list = ncfile.getGlobalAttributes();
		List<Statement> statements = new ArrayList<Statement>();
		
		for (Attribute attribute : attribute_list) {

			String attr_name = attribute.getShortName();
			Array attribute_values = attribute.getValues();
			String attr_resource_str;
			com.hp.hpl.jena.rdf.model.Resource r;
			r = NetCDFVocab.KnownNetCDFProperties.get(attr_name);
			if (r == null) {
				attr_resource_str = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_DATA + attr_name);
			} else {
				//attr_resource_str = "<" + r.getURI() + ">";
				attr_resource_str = r.getURI();
			}
			String spec = struct_name + "_" + attr_name + "_spec";
			
			URI struct_name_uri = new URIImpl(struct_name);
			URI spec_uri = new URIImpl(spec);
			URI attr_resource_str_uri = new URIImpl(attr_resource_str);
			
			statements.add(new StatementImpl(struct_name_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()), spec_uri));
			statements.add(new StatementImpl(spec_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Variable.getURI())));
			statements.add(new StatementImpl(spec_uri, new URIImpl(NetCDFVocab.VariableProperty.getURI()), attr_resource_str_uri));
			statements.add(new StatementImpl(spec_uri, new URIImpl(NetCDFVocab.ShortName.getURI()), new LiteralImpl(attribute.getShortName(), XMLSchema.STRING)));
			statements.add(new StatementImpl(attr_resource_str_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Attribute.getURI())));
			
			// write values
			for (int i = 0; i < attribute_values.getSize(); i++) {
				//write two triples for values for now and decide later...
				statements.add(new StatementImpl(spec_uri, new URIImpl(NetCDFVocab.AttrValue.getURI()), new LiteralImpl(
						StringEscapeUtils.escapeHtml4(attribute_values
								.getObject(i).toString()), NetCDFUtils.getDataTypeToXMLSchema(attribute.getDataType()))));
				//FIXME:must escape slashes also.
				
				statements.add(new StatementImpl(new URIImpl(dataset_name),
						attr_resource_str_uri, new LiteralImpl(
								StringEscapeUtils.escapeHtml4(attribute_values
										.getObject(i).toString()), NetCDFUtils.getDataTypeToXMLSchema(attribute.getDataType()))));
				//FIXME:must escape slashes also.
			}
			
		}
		
		return statements;
		
	}
	
	
	private List<Statement> writeVariableAttributes(Variable variable, String var_name) throws IOException {
		
		List<Attribute> attribute_list = variable.getAttributes();
		List<Statement> statements = new ArrayList<Statement>();
		
		for (Attribute attribute : attribute_list) {
			
			String attr_name = attribute.getShortName();
			Array attribute_values = attribute.getValues();
			String attr_resource_str = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_DATA + attr_name);
			String spec = var_name + "_" + attr_name + "_spec";
			
			URI var_name_uri = new URIImpl(var_name);
			URI spec_uri = new URIImpl(spec);
			URI attr_resource_str_uri = new URIImpl(attr_resource_str);
			
			statements.add(new StatementImpl(var_name_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()), spec_uri));
			statements.add(new StatementImpl(spec_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Variable.getURI())));
			statements.add(new StatementImpl(spec_uri, new URIImpl(NetCDFVocab.VariableProperty.getURI()), attr_resource_str_uri));
			statements.add(new StatementImpl(spec_uri, new URIImpl(NetCDFVocab.ShortName.getURI()), new LiteralImpl(attribute.getShortName(), XMLSchema.STRING)));
			statements.add(new StatementImpl(attr_resource_str_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Attribute.getURI())));
			
			// write values
			for (int i = 0; i < attribute_values.getSize(); i++) {
				//write two triples for values for now and decide later...
				statements.add(new StatementImpl(spec_uri, new URIImpl(NetCDFVocab.AttrValue.getURI()), new LiteralImpl(
						StringEscapeUtils.escapeHtml4(attribute_values
								.getObject(i).toString()), NetCDFUtils.getDataTypeToXMLSchema(attribute.getDataType()))));
				//FIXME:must escape slashes also.
				
				statements.add(new StatementImpl(var_name_uri,
						attr_resource_str_uri, new LiteralImpl(
								StringEscapeUtils.escapeHtml4(attribute_values
										.getObject(i).toString()), NetCDFUtils.getDataTypeToXMLSchema(attribute.getDataType()))));
				//FIXME:must escape slashes also.
			}
			
		}
		return statements;
	}
	
	
	private boolean insertStatement(String subj_netcdf, String pred_netcdf, String obj_netcdf) {
		
		boolean insert = false;
		
		if (subj_str != null && pred_str != null && obj_str != null) {
			if (subj_str.equals(subj_netcdf) && pred_str.equals(pred_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str != null && obj_str == null) {
			if (subj_str.equals(subj_netcdf) && pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str == null && obj_str != null) {
			if (subj_str.equals(subj_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str == null && obj_str == null) {
			if (subj_str.equals(subj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str != null && obj_str != null) {
			if (pred_str.equals(pred_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str != null && obj_str == null) {
			if (pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str == null && obj_str != null) {
			if (obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str == null && obj_str == null) {
			insert = true;
		}

		return insert;
		
	}
	
	private boolean insertObservation(String subj_netcdf, String pred_netcdf) {
		
		boolean insert = false;
		
		if (subj_str != null && pred_str != null) {
			if (subj_str.equals(subj_netcdf) && pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str == null) {
			if (subj_str.equals(subj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str != null) {
			if (pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str == null) {
			insert = true;
		}

		return insert;
		
	}

	private String getVarStructName(Variable variable, String struct_name) {
		return struct_name + "_" + variable.getShortName();
	}
	
	private String getPos(String var_name, int i) {
		return var_name + "_pos_" + i;
	}

}
