/***************
<p>Title: NetCDFToStatements</p>

<p>Description:
Converts a netcdf file to Sesame Statements.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Giannis Mouchakis (SemaGrow 2014)

***************/


package gr.demokritos.iit.netcdftoolkit.sail;

import gr.demokritos.iit.netcdftoolkit.commons.NetCDFUtils;
import gr.demokritos.iit.netcdftoolkit.commons.NetCDFVocab;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;


public class NetCDFToStatementsThreaded  implements Runnable {
	static Logger logger = LoggerFactory.getLogger(NetCDFToStatementsThreaded.class);

	private String subj_str;
	private String pred_str;
	private String obj_str;
	private BlockingQueue<Statement> queue;
	private NetcdfFile ncfile;
	private String uribase;
	private boolean fastTriplefication = false;


	public NetCDFToStatementsThreaded(Resource subj, URI pred,
			Value obj, BlockingQueue<Statement> queue,
			NetcdfFile ncfile, String uribase) {
		if (subj != null) {
			this.subj_str = subj.stringValue();
		}
		if (pred != null) {
			this.pred_str = pred.stringValue();
		}
		if (obj != null) {
			this.obj_str = obj.stringValue();
		}
		this.queue = queue;
		//this.ncfile = ncfile;
		try {
			//open again the file to avoid strange exception in thread some times
			this.ncfile = NetcdfFile.open(ncfile.getLocation());
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.uribase = uribase;

	}
	
	public NetCDFToStatementsThreaded(Resource subj, URI pred,
			Value obj, BlockingQueue<Statement> queue,
			NetcdfFile ncfile, String uribase, boolean fastTriplefication) {
		this.fastTriplefication = fastTriplefication;
		if (subj != null) {
			this.subj_str = subj.stringValue();
		}
		if (pred != null) {
			this.pred_str = pred.stringValue();
		}
		if (obj != null) {
			this.obj_str = obj.stringValue();
		}
		this.queue = queue;
		//this.ncfile = ncfile;
		try {
			//open again the file to avoid strange exception in thread some times
			this.ncfile = NetcdfFile.open(ncfile.getLocation());
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.uribase = uribase;

	}

	
	@Override
	public void run() {
		
		String dataset_name = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_DATA + uribase);
		String struct_name = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_STRUCT + uribase + "_struct");

		List<Variable> var_list = ncfile.getVariables();

		List<Variable> dimension_variables = new ArrayList<Variable>();
		List<Variable> measurement_variables = new ArrayList<Variable>();

		// read data for each dimension variable and add to the list.
		for (Variable variable : var_list) {
			if (isDimensionVar(variable)) {
				dimension_variables.add(variable);
			}
			else {
				measurement_variables.add(variable);
			}
		}
		
		for (Variable measurement_variable : measurement_variables) {
			
			//add dimension dataset and struct 
			String var_dataset = dataset_name + "_" + measurement_variable.getShortName();
			URI var_dataset_uri = new URIImpl(var_dataset);
			String var_dataset_struct = var_dataset + "_struct";
			URI var_dataset_struct_uri = new URIImpl(var_dataset_struct);
			try {
				queue.put(new StatementImpl(var_dataset_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Dataset.getURI())));
				queue.put(new StatementImpl(var_dataset_uri, new URIImpl(NetCDFVocab.DatasetStructure.getURI()), var_dataset_struct_uri));
				queue.put(new StatementImpl(var_dataset_struct_uri, new URIImpl(NetCDFVocab.RDFType.getURI()), new URIImpl(NetCDFVocab.Structure.getURI())));
				String var_name = getVarStructName(measurement_variable, struct_name);
				queue.put(new StatementImpl(var_dataset_struct_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()), new URIImpl(var_name)));
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			
			//order dimensions according to measurement variable's dimensions
			List<Variable> dim_var_ordered = new ArrayList<Variable>();
			// These values are the allowed indices for each dimension when reading measurements
			List<Array> dim_data_odered = new ArrayList<Array>();
				
			List<Dimension> dimensions = measurement_variable.getDimensions();
			for (Dimension dimension : dimensions) {
				for (Variable dim_var : dimension_variables) {
					if (dimension.getShortName().equals(dim_var.getShortName())) {
						dim_var_ordered.add(dim_var);
						try {
							dim_data_odered.add(dim_var.read());
						} catch (IOException e) {
							e.printStackTrace();
						}
						//add variable structs as components
						String var_name = getVarStructName(dim_var, struct_name);
						try {
							queue.put(new StatementImpl(var_dataset_struct_uri, new URIImpl(NetCDFVocab.StructureVariable.getURI()), new URIImpl(var_name)));
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
			
			String m_prop_name = getPropName(measurement_variable);
			String data_type = NetCDFUtils.getXsdType(measurement_variable.getDataType());
			
			if (fastTriplefication) {//load values in memory
				try {
					Array results = measurement_variable.read();
					Long count = 0l;//start from the end to match slow writing counter
					IndexIterator it = results.getIndexIterator();
					while(it.hasNext()) {
						Object result = it.next();
						int[] indices = it.getCurrentCounter();
						logger.debug(Arrays.toString(indices));
						String obs = var_dataset + "_obs_" + count.toString();
						writeVariableValues(indices, dim_var_ordered, 
								measurement_variable, var_dataset, obs, m_prop_name,
								data_type, struct_name, result);
						
						count++;
					}
				} catch (IOException | InvalidRangeException | InterruptedException e) {
					logger.error("exception while getting observations", e);
				}
			} else {//read values one by one
			
				int n = dim_data_odered.size();
				int[] indices = new int[n];
				long[] ranges = new long[n];
				int i = 0;
				for (Array array : dim_data_odered) {
					ranges[i] = array.getSize();
					i++;
				}
						
				// writes observations
				// Start off at indices = [0,...,0]
				Integer count = 0;
				do {
					logger.debug( Arrays.toString(indices) );
					String obs = var_dataset + "_obs_" + count.toString();
					try {
						writeVariableValues(indices, dim_var_ordered, 
								measurement_variable, var_dataset, 
								obs, m_prop_name, data_type, struct_name);
					} catch (IOException e) {
						e.printStackTrace();
					} catch (InvalidRangeException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					advanceIndices(n, indices, ranges);
					++count;
				}
				while( ! allMaxed(n, indices, ranges) );
				//do last one
				String obs = var_dataset + "_obs_" + count.toString();
				logger.debug( Arrays.toString(indices) );
				try {
					writeVariableValues(indices, dim_var_ordered, 
							measurement_variable, var_dataset, 
							obs, m_prop_name, data_type, struct_name);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InvalidRangeException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			ncfile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
		private void writeVariableValues( int[] indices, List<Variable> sensor_variables, 
				Variable measurement_variable, String dataset_name, String observation_name, 
				String m_prop_name, String data_type, String struct_name, Object result )
		throws IOException, InvalidRangeException, InterruptedException
		{
			
			URI observation_name_uri = new URIImpl(observation_name);
			//URI m_prop_name_uri = new URIImpl(m_prop_name);
			
			int x = 0;
			for (Variable variable : sensor_variables) {
				String p = getPropName(variable);
				String var_name = getVarStructName(variable, struct_name);
				String pos = getPos(var_name, indices[x]);
				if (insertStatement(observation_name, p, pos)) {
					URI p_uri = new URIImpl(p);
					URI pos_uri = new URIImpl(pos);
					queue.put(new StatementImpl(observation_name_uri, p_uri, pos_uri));
				}
				x++;
			}
			
			if (insertObservation(observation_name, m_prop_name)) {
				URI m_prop_name_uri = new URIImpl(m_prop_name);
				URI datatype = NetCDFUtils.getDataTypeToXMLSchema(measurement_variable.getDataType());
				String result_str = result.toString();
				if (obj_str == null) {
					queue.put(new StatementImpl(observation_name_uri, m_prop_name_uri, new LiteralImpl(result_str, datatype)));
				} else if (obj_str.equals(result_str)) {
					queue.put(new StatementImpl(observation_name_uri, m_prop_name_uri, new LiteralImpl(result_str, datatype)));
				}
				
			}
			queue.put(new StatementImpl(observation_name_uri, new URIImpl(
					NetCDFVocab.ObservationDataset.getURI()), new URIImpl(dataset_name)));
		}
	

	private void writeVariableValues( int[] indices, List<Variable> sensor_variables, 
			Variable measurement_variable, String dataset_name, String observation_name, 
			String m_prop_name, String data_type, String struct_name )
	throws IOException, InvalidRangeException, InterruptedException
	{
		
		URI observation_name_uri = new URIImpl(observation_name);
		//URI m_prop_name_uri = new URIImpl(m_prop_name);
		
		int x = 0;
		for (Variable variable : sensor_variables) {
			String p = getPropName(variable);
			String var_name = getVarStructName(variable, struct_name);
			String pos = getPos(var_name, indices[x]);
			if (insertStatement(observation_name, p, pos)) {
				URI p_uri = new URIImpl(p);
				URI pos_uri = new URIImpl(pos);
				queue.put(new StatementImpl(observation_name_uri, p_uri, pos_uri));
			}
			x++;
		}
		
		if (insertObservation(observation_name, m_prop_name)) {
			
			URI m_prop_name_uri = new URIImpl(m_prop_name);
			
			String sectionSpec = "";//used to read values in the grid
			for (int i = 0; i < indices.length - 1; i++) {
				sectionSpec += indices[i] + ":" + indices[i] + ", ";
			}
			sectionSpec += indices[indices.length - 1] + ":" + indices[indices.length - 1];
			
			//write measurement
			Array result = measurement_variable.read(sectionSpec);
			URI datatype = NetCDFUtils.getDataTypeToXMLSchema(measurement_variable.getDataType());
			String result_str = result.getObject(0).toString();
			if (obj_str == null) {
				queue.put(new StatementImpl(observation_name_uri, m_prop_name_uri, new LiteralImpl(result_str, datatype)));
			} else if (obj_str.equals(result_str)) {
				queue.put(new StatementImpl(observation_name_uri, m_prop_name_uri, new LiteralImpl(result_str, datatype)));
			}
			
		}
		
		queue.put(new StatementImpl(observation_name_uri, new URIImpl(
				NetCDFVocab.ObservationDataset.getURI()), new URIImpl(dataset_name)));
		
	}
	
	
	// Advances 'indices' to the next in sequence.
    private void advanceIndices(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i]+1 == ranges[i]) {
                indices[i] = 0;
            }
            else {
                indices[i] += 1;
                break;
            }
        }

    }

    // Tests if indices are in final position.
    private boolean allMaxed(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i] != ranges[i]-1) {
                return false;
            }
        }
        return true;

    }


    /**
     * Private helper checking if a variable is a dimension.
     * 
     * @param variable
     * @param dim_list
     */
	private boolean isDimensionVar(Variable variable) {
		if (variable.getRank() < 2) {
			return true;
		} else return false;
	}
	
	/**
	 * 
	 * method to get the property name for a variable. 
	 * 
	 * @param variable
	 * @return the property name. if variable has standard name then 
	 * name=cf:&lt;stantard_name&gt; else name=sgdata:&lt;short_name&gt;
	 */
	public static String getPropName(Variable variable) {
		String prop = null;
		Attribute name_attr = variable.findAttribute("standard_name");
		if (name_attr == null) {
			prop = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.NS_DATA + variable.getShortName());
		} else {
			prop = NetCDFVocab.prefixMapping.expandPrefix(NetCDFVocab.CF + name_attr.getStringValue());
		}
		return prop;
	}
	
	
	private boolean insertStatement(String subj_netcdf, String pred_netcdf, String obj_netcdf) {
		
		boolean insert = false;
		
		if (subj_str != null && pred_str != null && obj_str != null) {
			if (subj_str.equals(subj_netcdf) && pred_str.equals(pred_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str != null && obj_str == null) {
			if (subj_str.equals(subj_netcdf) && pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str == null && obj_str != null) {
			if (subj_str.equals(subj_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str == null && obj_str == null) {
			if (subj_str.equals(subj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str != null && obj_str != null) {
			if (pred_str.equals(pred_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str != null && obj_str == null) {
			if (pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str == null && obj_str != null) {
			if (obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str == null && obj_str == null) {
			insert = true;
		}

		return insert;
		
	}
	
	private boolean insertObservation(String subj_netcdf, String pred_netcdf) {
		
		boolean insert = false;
		
		if (subj_str != null && pred_str != null) {
			if (subj_str.equals(subj_netcdf) && pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str == null) {
			if (subj_str.equals(subj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str != null) {
			if (pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str == null) {
			insert = true;
		}

		return insert;
		
	}

	private String getVarStructName(Variable variable, String struct_name) {
		return struct_name + "_" + variable.getShortName();
	}
	
	private String getPos(String var_name, int i) {
		return var_name + "_pos_" + i;
	}

}
