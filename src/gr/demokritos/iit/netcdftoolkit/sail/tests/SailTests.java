/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.sail.tests;

import static org.junit.Assert.assertTrue;
import gr.demokritos.iit.netcdftoolkit.config.NetCDFStoreConfig;
import gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.LinkedHashModel;
import org.openrdf.model.util.ModelUtil;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.config.RepositoryImplConfig;
import org.openrdf.repository.config.RepositoryRegistry;
import org.openrdf.repository.sail.config.SailRepositoryConfig;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.UnsupportedRDFormatException;
import org.openrdf.sail.config.SailImplConfig;

import ucar.ma2.InvalidRangeException;

/**
 * @author Giannis Mouchakis
 *
 */
public class SailTests {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws RepositoryConfigException, RepositoryException, RDFParseException, UnsupportedRDFormatException, IOException {
		String netcdf_file = "resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.nc";
		SailImplConfig sailConfig = new NetCDFStoreConfig(netcdf_file, "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t");
        RepositoryImplConfig repositoryConfig = new SailRepositoryConfig(sailConfig);
        Repository repo = RepositoryRegistry.getInstance().get(repositoryConfig.getType()).getRepository(repositoryConfig);
        repo.initialize();
        RepositoryConnection conn = repo.getConnection();
        RepositoryResult<Statement> results = conn.getStatements(null, null, null, true);
        Model model_sail = new LinkedHashModel();
        while(results.hasNext()) {
        	model_sail.add(results.next());
        }
        results.close();
        conn.close();
        repo.shutDown();
        
        InputStream inputStream = new FileInputStream("resources/triplefied.ttl");
        RDFFormat format = Rio.getParserFormatForFileName("resources/triplefied.ttl");
        Model model_test_file = Rio.parse(inputStream, "", format);
        inputStream.close();
        
        //assertTrue(ModelUtil.equals(model_sail, model_test_file));
        
	}
	
	@Test
	public void compareTriplefiers() throws RepositoryConfigException, RepositoryException, RDFParseException, UnsupportedRDFormatException, IOException, InvalidRangeException, RDFHandlerException {
				
		String netcdf_file = "resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.nc";
		String ttl_file = "resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.ttl";
		
		NetCDFToRDF netcdf_to_rdf = new NetCDFToRDF( ttl_file , null);
		netcdf_to_rdf.init();
		netcdf_to_rdf.add( netcdf_file, "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t" );
		netcdf_to_rdf.close();
		
		
		SailImplConfig sailConfig = new NetCDFStoreConfig(netcdf_file, "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t");
        RepositoryImplConfig repositoryConfig = new SailRepositoryConfig(sailConfig);
        Repository repo = RepositoryRegistry.getInstance().get(repositoryConfig.getType()).getRepository(repositoryConfig);
        repo.initialize();
        RepositoryConnection conn = repo.getConnection();
        RepositoryResult<Statement> results = conn.getStatements(null, null, null, true);
        Model model_sail = new LinkedHashModel();
        while(results.hasNext()) {
        	model_sail.add(results.next());
        }
        results.close();
        conn.close();
        repo.shutDown();
        
        InputStream inputStream = new FileInputStream(ttl_file);
        RDFFormat format = Rio.getParserFormatForFileName(ttl_file);
        Model model_triplefier = Rio.parse(inputStream, "", format);
        inputStream.close();
                
        Iterator<Statement> sail_it = model_sail.iterator();
        int i = 0; 
        int x = 0;
        while (sail_it.hasNext()) {
        	Statement sail_stmt = sail_it.next();
        	if (model_triplefier.contains(sail_stmt.getSubject(), sail_stmt.getPredicate(), sail_stmt.getObject())) {
        		x++;
        	} else {
        		System.out.println(sail_stmt);
        		i++;
        	}

        }
        System.out.println(i);
        System.out.println(x);
        
        Iterator<Statement> triplefier_it2 = model_triplefier.iterator();
        int i2 = 0; 
        int x2 = 0;
        while (triplefier_it2.hasNext()) {
        	Statement triplefier_stmt = triplefier_it2.next();
        	if (model_sail.contains(triplefier_stmt.getSubject(), triplefier_stmt.getPredicate(), triplefier_stmt.getObject())) {
        		x2++;
        	} else {
        		System.out.println(triplefier_stmt);
        		i2++;
        	}
        }
        System.out.println(i2);
        System.out.println(x2);
        
        System.out.println(model_sail.size());
        System.out.println(model_triplefier.size());
        
        //assertTrue(ModelUtil.equals(model_sail, model_sail2));
                
	}

}
