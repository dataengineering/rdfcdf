/***************
<p>Title: NetCDFTripleSource</p>

<p>Description:
NetCDFTripleSource.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Angelos Charalambidis (SemaGrow 2014)

***************/
package gr.demokritos.iit.netcdftoolkit.sail;

import info.aduna.iteration.CloseableIteration;
import org.openrdf.model.*;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.evaluation.TripleSource;


public class NetCDFTripleSource implements TripleSource {

    private ValueFactory vf;
    private NetCDFStore store;

    public NetCDFTripleSource(NetCDFStore netCDFStore, ValueFactory factory) {
    	this.store = netCDFStore;
        this.vf = factory;
    }

    @Override
    public CloseableIteration<? extends Statement, QueryEvaluationException>
        getStatements(Resource resource, URI uri, Value value, Resource... resources) throws QueryEvaluationException {

        return store.createStatementIteration(QueryEvaluationException.class, resource, uri, value, false, resources);
    }

    @Override
    public ValueFactory getValueFactory() { return vf; }
}
