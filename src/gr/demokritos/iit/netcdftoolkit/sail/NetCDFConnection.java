/***************
<p>Title: NetCDFConnection</p>

<p>Description:
NetCDFStoreConfig.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Angelos Charalambidis (SemaGrow 2014)

***************/

package gr.demokritos.iit.netcdftoolkit.sail;

import info.aduna.iteration.CloseableIteration;

import java.io.IOException;

import info.aduna.iteration.EmptyIteration;

import org.openrdf.model.Namespace;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.Dataset;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.evaluation.EvaluationStrategy;
import org.openrdf.query.algebra.evaluation.impl.EvaluationStrategyImpl;
import org.openrdf.sail.SailException;
import org.openrdf.sail.helpers.SailConnectionBase;


public class NetCDFConnection extends SailConnectionBase {
	
	protected final NetCDFStore sail;

	public NetCDFConnection(NetCDFStore sail) throws IOException {
		super(sail);
		this.sail = sail;
	}

	@Override
	protected void closeInternal() throws SailException {
	}

	@Override
	protected CloseableIteration<? extends BindingSet, QueryEvaluationException> evaluateInternal(
			TupleExpr tupleExpr, Dataset dataset, BindingSet bindings,
			boolean includeInferred) throws SailException {
		// TODO Auto-generated method stub

        NetCDFTripleSource tripleSource = new NetCDFTripleSource(sail, sail.getValueFactory());
        EvaluationStrategy strategy = new EvaluationStrategyImpl(tripleSource);

        // do optional optimization to tupleExpr and other black magic

        try {
            return strategy.evaluate(tupleExpr, bindings);
        } catch (QueryEvaluationException e) {
            throw new SailException(e);
        }
    }

	@Override
	protected CloseableIteration<? extends Resource, SailException> getContextIDsInternal()
			throws SailException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected CloseableIteration<? extends Statement, SailException> getStatementsInternal(
			Resource subj, URI pred, Value obj, boolean includeInferred,
			Resource... contexts) throws SailException {
		
		return sail.createStatementIteration( SailException.class, subj, pred, obj, includeInferred, contexts );
	}

	@Override
	protected long sizeInternal(Resource... contexts) throws SailException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected void startTransactionInternal() throws SailException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void commitInternal() throws SailException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void rollbackInternal() throws SailException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addStatementInternal(Resource subj, URI pred, Value obj,
			Resource... contexts) throws SailException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void removeStatementsInternal(Resource subj, URI pred, Value obj,
			Resource... contexts) throws SailException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void clearInternal(Resource... contexts) throws SailException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected CloseableIteration<? extends Namespace, SailException> getNamespacesInternal()
			throws SailException {
		// TODO Auto-generated method stub
		return new EmptyIteration<Namespace,SailException>();
	}

	@Override
	protected String getNamespaceInternal(String prefix) throws SailException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void setNamespaceInternal(String prefix, String name)
			throws SailException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void removeNamespaceInternal(String prefix) throws SailException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void clearNamespacesInternal() throws SailException {
		// TODO Auto-generated method stub
		
	}

}
