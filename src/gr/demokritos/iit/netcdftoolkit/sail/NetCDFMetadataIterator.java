package gr.demokritos.iit.netcdftoolkit.sail;

import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.NetcdfFile;

/**
 * 
 * Iterator for NetCDFStore SAIL that only iterates through the metadata of a NetCDF file
 * 
 * @author Giannis Mouchakis
 *
 */
public class NetCDFMetadataIterator implements Iterator<Statement> {

	final private Resource subj;
	final private URI pred;
	final private Value obj;
	final private NetcdfFile ncfile;
	final private String uribase;

	private Iterator<Statement> metadata_it;
	
	public NetCDFMetadataIterator(final Resource subj, final URI pred,
			final Value obj, final NetcdfFile ncfile, final String uribase) {
		this.subj = subj;	
		this.pred = pred;
		this.obj = obj;
		this.ncfile = ncfile;
		this.uribase = uribase;
		this.init();
	}
	
	/**
	 * Returns <tt>true</tt> if the iteration has more elements. (In other
	 * words, returns <tt>true</tt> if {@link #next} would return an element
	 * rather than throwing a <tt>NoSuchElementException</tt>.)
	 * 
	 * @return <tt>true</tt> if the iteration has more elements.
	 */
	@Override
	public boolean hasNext() {
		return metadata_it.hasNext();
	}

	/**
	 * Returns the next element in the iteration.
	 * 
	 * @return the next element in the iteration.
	 * @throws NoSuchElementException
	 *         if the iteration has no more elements or if it has been closed.
	 */
	@Override
	public Statement next()	throws NoSuchElementException {
		return metadata_it.next();
	}

	@Override
	public void remove() throws UnsupportedOperationException {
		throw new UnsupportedOperationException( "remove() is not supported" );
	}
	
	private void init() {
		NetCDFToStatements nts = new NetCDFToStatements(null, subj, pred, obj);//FIXME:remove null dataset.
		try {
			metadata_it = nts.getStatements(ncfile, uribase, false).iterator();
		} catch (IOException | InvalidRangeException e ) {
			e.printStackTrace();
		}
	}


}
