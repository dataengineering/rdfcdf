package gr.demokritos.iit.netcdftoolkit.sail;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;


public class NetCDFIterator implements Iterator<Statement> {

	final private Resource subj;
	final private URI pred;
	final private Value obj;
	private String subj_str;
	private String pred_str;
	private String obj_str;
	final private NetcdfFile ncfile;
	final private String uribase;

	private Statement buffer;
	private Iterator<Statement> metadata_it;
	
	private boolean fetchAll;
	private boolean fetchMetadata;
	private boolean fetchObservations;
	
	private BlockingQueue<Statement> queue;
	private Thread getStatementsThread;
	
	public NetCDFIterator(final Resource subj, final URI pred, final Value obj, final NetcdfFile ncfile, final String uribase)
	{
		this.subj = subj;
		if (this.subj != null) {
			subj_str = this.subj.stringValue();
		}
		this.pred = pred;
		if (this.pred != null) {
			pred_str = this.pred.stringValue();
		}
		this.obj = obj;
		if (this.obj != null) {
			obj_str = this.obj.stringValue();
		}
		this.ncfile = ncfile;
		this.uribase = uribase;
		
		init();
		
	}

	
	/**
	 * Returns <tt>true</tt> if the iteration has more elements. (In other
	 * words, returns <tt>true</tt> if {@link #next} would return an element
	 * rather than throwing a <tt>NoSuchElementException</tt>.)
	 * 
	 * @return <tt>true</tt> if the iteration has more elements.
	 */
	@Override
	public boolean hasNext() {
		if( buffer != null ) { 
			return true; 
		} else {
			buffer = fetch();
			return buffer != null;
		}
	}

	
	
	/**
	 * Returns the next element in the iteration.
	 * 
	 * @return the next element in the iteration.
	 * @throws NoSuchElementException
	 *         if the iteration has no more elements or if it has been closed.
	 */
	@Override
	public Statement next()	throws NoSuchElementException {
		Statement retv;
		if( buffer != null ) {
			retv = buffer;
			buffer = null;
		}
		else {
			retv = this.fetch();
			if( retv == null ) { throw new NoSuchElementException( "No more matching elements" ); }
		}
		return retv;
	}


	@Override
	public void remove() throws UnsupportedOperationException {
		throw new UnsupportedOperationException( "remove() is not supported" );
	}
	
	
	private Statement fetch() {	
		Statement fetched = null;	
		if (fetchAll) {
			if (metadata_it.hasNext()) {//still have metadata to check
				fetched = fetchMatchingMetadata();
			} else {//get observation
				fetched = fetchMatchingObservation();
			}
		} else if (fetchMetadata) {
			fetched = fetchMatchingMetadata();
		} else if (fetchObservations) {//get observation
			fetched = fetchMatchingObservation();
		}
		return fetched;
	}
	
	private void init() {
		
		if (pred == null) {//fetch all
			fetchAll = true;
			queue = new ArrayBlockingQueue<Statement>(100);
			NetCDFToStatementsThreaded netcdfToStmt = new NetCDFToStatementsThreaded(subj, pred, obj, queue, ncfile, uribase);
			getStatementsThread = new Thread(netcdfToStmt);
			getStatementsThread.start();
			NetCDFToStatements nts = new NetCDFToStatements(null, subj, pred, obj);//FIXME:remove null dataset.
			try {
				metadata_it = nts.getStatements(ncfile, uribase, false).iterator();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InvalidRangeException e) {
				e.printStackTrace();
			}
		} else {
			fetchObservations = false;//FIXME:triplefication changed so must fix fetch observations "guess".  
			List<Variable> var_list = ncfile.getVariables();
			for (Variable variable : var_list) {
				String propName = NetCDFToStatements.getPropName(variable);
				if (pred.stringValue().equals(propName)) {//fetch observations
					queue = new ArrayBlockingQueue<Statement>(100);
					NetCDFToStatementsThreaded netcdfToStmt = new NetCDFToStatementsThreaded(subj, pred, obj, queue, ncfile, uribase);
					getStatementsThread = new Thread(netcdfToStmt);
					getStatementsThread.start();
					fetchObservations = true;
					break;
				}
			}
			if ( ! fetchObservations ) {//fetch metadata
				fetchMetadata = true;
				NetCDFToStatements nts = new NetCDFToStatements(null, subj, pred, obj);//FIXME:remove null dataset.
				try {
					metadata_it = nts.getStatements(ncfile, uribase, false).iterator();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InvalidRangeException e) {
					e.printStackTrace();
				}
			}
		}	
	}
	

	private Statement fetchMatchingMetadata() {
		Statement fetched = null;
		while (metadata_it.hasNext()) {
			Statement stmt = metadata_it.next();
			if (insertStatement(
					stmt.getSubject().stringValue(), 
					stmt.getPredicate().stringValue(),
					stmt.getObject().stringValue())) {
				fetched = stmt;
				break;
			}
		}
		return fetched;
	}
	
	private Statement fetchMatchingObservation() {
		Statement fetched = null;
		if ( ! getStatementsThread.isAlive() ) {//if thread has already died get the rest from the queue
			while ( ! queue.isEmpty() ) {
				Statement stmt = queue.poll();
				if (stmt != null && insertStatement(
						stmt.getSubject().stringValue(), 
						stmt.getPredicate().stringValue(),
						stmt.getObject().stringValue())) {
					fetched = stmt;
					break;
				}
			}
		}
		while (getStatementsThread.isAlive()) {
			Statement stmt = queue.poll();
			if (stmt != null && insertStatement(
					stmt.getSubject().stringValue(), 
					stmt.getPredicate().stringValue(),
					stmt.getObject().stringValue())) {
				fetched = stmt;
				break;
			}
		}
		return fetched;
	}
	
	private boolean insertStatement(String subj_netcdf, String pred_netcdf, String obj_netcdf) {
		
		boolean insert = false;
		
		if (subj_str != null && pred_str != null && obj_str != null) {
			if (subj_str.equals(subj_netcdf) && pred_str.equals(pred_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str != null && obj_str == null) {
			if (subj_str.equals(subj_netcdf) && pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str == null && obj_str != null) {
			if (subj_str.equals(subj_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str != null && pred_str == null && obj_str == null) {
			if (subj_str.equals(subj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str != null && obj_str != null) {
			if (pred_str.equals(pred_netcdf) && obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str != null && obj_str == null) {
			if (pred_str.equals(pred_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str == null && obj_str != null) {
			if (obj_str.equals(obj_netcdf)) {
				insert = true;
			}
		} else if (subj_str == null && pred_str == null && obj_str == null) {
			insert = true;
		}

		return insert;
		
	}

}
