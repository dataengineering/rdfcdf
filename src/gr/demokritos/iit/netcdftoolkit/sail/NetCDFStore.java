/***************
<p>Title: NetCDFStore</p>

<p>Description:
NetCDFStore.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Giannis Mouchakis (SemaGrow 2014)
@author Angelos Charalambidis (SemaGrow 2014)

***************/

package gr.demokritos.iit.netcdftoolkit.sail;

import gr.demokritos.iit.netcdftoolkit.config.NetCDFStoreConfig;
import info.aduna.iteration.CloseableIteration;
import info.aduna.iteration.CloseableIteratorIteration;

import java.io.IOException;
import org.apache.commons.io.FilenameUtils;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.config.RepositoryImplConfig;
import org.openrdf.repository.config.RepositoryRegistry;
import org.openrdf.repository.sail.config.SailRepositoryConfig;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;
import org.openrdf.sail.config.SailImplConfig;
import org.openrdf.sail.helpers.SailBase;

import ucar.nc2.NetcdfFile;


public class NetCDFStore extends SailBase {
	
	private String uribase;

    private NetcdfFile ncfile;

	public NetCDFStore(String netcdf_file) throws IOException {
        ncfile = NetcdfFile.open(netcdf_file);
		this.uribase = FilenameUtils.getBaseName(netcdf_file);
	}
	
	public NetCDFStore(String netcdf_file, String uribase) throws IOException {
		ncfile = NetcdfFile.open(netcdf_file);
		this.uribase = uribase;
	}


	@Override
	public boolean isWritable() throws SailException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ValueFactory getValueFactory() {
		// TODO Auto-generated method stub
		return ValueFactoryImpl.getInstance();
	}

	@Override
	protected void shutDownInternal() throws SailException {

        if (ncfile!=null) {
            try {
                ncfile.close();
            } catch (IOException e) {
                throw new SailException(e);
            }
        }
    }

	@Override
	protected SailConnection getConnectionInternal() throws SailException {
		SailConnection sailConnection = null;
		try {
			sailConnection =  new NetCDFConnection(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sailConnection;
	}

    public NetcdfFile getNetCDFFile() { return ncfile; }

    public String getUribase() { return uribase; }

    protected <X extends Exception>
        CloseableIteration<? extends Statement, X>  createStatementIteration(
            Class<X> exceptionClass,
            Resource subj, URI pred, Value obj, boolean includeInferred,
            Resource... contexts) throws X {

    	NetCDFIterator iter = new NetCDFIterator(subj, pred, obj, ncfile, uribase);
    	CloseableIteratorIteration<? extends Statement, X> iteration = new CloseableIteratorIteration<Statement, X>(iter);
    	return iteration;
    	
    }


    /**
	 * @param args
	 * @throws RepositoryException 
	 * @throws MalformedQueryException 
	 * @throws QueryEvaluationException 
	 * @throws RepositoryConfigException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws RepositoryException, MalformedQueryException, QueryEvaluationException, RepositoryConfigException, IOException {
		
		String netcdf_file = "resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.nc";
		
		SailImplConfig sailConfig = new NetCDFStoreConfig(netcdf_file, "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t");
        RepositoryImplConfig repositoryConfig = new SailRepositoryConfig(sailConfig);
        Repository repo = RepositoryRegistry.getInstance().get(repositoryConfig.getType()).getRepository(repositoryConfig);
        repo.initialize();
        RepositoryConnection conn = repo.getConnection();
        
        String queryString = "SELECT * WHERE {?s ?p ?o . }";
        TupleQuery query = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString);

        TupleQueryResult results =  query.evaluate();  
        while (results.hasNext()) {
        	System.out.println(results.next());
        }
        
		
	}
	
}
