package gr.demokritos.iit.netcdftoolkit;

import java.io.IOException;

import ucar.ma2.InvalidRangeException;
import gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF;

public class ProduceTTLExample {

	public static void main(String[] args) throws IOException, InvalidRangeException {
		String netcdf_file = "resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.nc";
		String ttl_file = "resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.ttl";
		NetCDFToRDF netcdf_to_rdf = new NetCDFToRDF( ttl_file , null);
		netcdf_to_rdf.init();
		netcdf_to_rdf.add( netcdf_file, "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t" );
		netcdf_to_rdf.close();
	}

}
