package gr.demokritos.iit.netcdftoolkit;

import gr.demokritos.iit.netcdftoolkit.config.NetCDFStoreConfig;

import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.LinkedHashModel;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.config.RepositoryImplConfig;
import org.openrdf.repository.config.RepositoryRegistry;
import org.openrdf.repository.sail.config.SailRepositoryConfig;
import org.openrdf.sail.config.SailImplConfig;

public class NetCDFStoreExample {

	public static void main(String[] args) throws RepositoryConfigException, RepositoryException {
		String netcdf_file = "resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.nc";
		SailImplConfig sailConfig = new NetCDFStoreConfig(netcdf_file, "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t");
        RepositoryImplConfig repositoryConfig = new SailRepositoryConfig(sailConfig);
        Repository repo = RepositoryRegistry.getInstance().get(repositoryConfig.getType()).getRepository(repositoryConfig);
        repo.initialize();
        RepositoryConnection conn = repo.getConnection();
        RepositoryResult<Statement> results = conn.getStatements(null, null, null, true);
        Model model_sail = new LinkedHashModel();
        while(results.hasNext()) {
        	model_sail.add(results.next());
        }
        results.close();
        conn.close();
        repo.shutDown();
	}

}
