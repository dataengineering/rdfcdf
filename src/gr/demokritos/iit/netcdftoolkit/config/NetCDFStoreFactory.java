/***************
<p>Title: NetCDFStoreFactory</p>

<p>Description:
NetCDFStoreFactory.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Angelos Charalambidis (SemaGrow 2014)

***************/

package gr.demokritos.iit.netcdftoolkit.config;

import gr.demokritos.iit.netcdftoolkit.sail.NetCDFStore;
import org.openrdf.sail.Sail;
import org.openrdf.sail.config.SailConfigException;
import org.openrdf.sail.config.SailFactory;
import org.openrdf.sail.config.SailImplConfig;


public class NetCDFStoreFactory implements SailFactory {

    public static final String SAIL_TYPE = "netcdf:NetCDFStore";

    @Override
    public String getSailType() {
        return SAIL_TYPE;
    }

    @Override
    public SailImplConfig getConfig() {
        return new NetCDFStoreConfig();
    }

    @Override
    public Sail getSail(SailImplConfig sailImplConfig) throws SailConfigException {

        if (!SAIL_TYPE.equals(sailImplConfig.getType()) ||
            !(sailImplConfig instanceof NetCDFStoreConfig))
        {
            throw new SailConfigException("Invalid Sail type: " + sailImplConfig.getType());
        }

        NetCDFStoreConfig config = (NetCDFStoreConfig)sailImplConfig;

        String netCDFFile = config.getNetCDFFile();
        String uriBase = config.getUriBase();

        try {
            return new NetCDFStore(netCDFFile, uriBase);
        } catch(Exception e) {
            throw new SailConfigException(e);
        }
    }
}
