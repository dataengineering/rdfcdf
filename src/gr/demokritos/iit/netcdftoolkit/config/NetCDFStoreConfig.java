/***************
<p>Title: NetCDFStoreConfig</p>

<p>Description:
NetCDFStoreConfig.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Giannis Mouchakis (SemaGrow 2014)
@author Angelos Charalambidis (SemaGrow 2014)

***************/

package gr.demokritos.iit.netcdftoolkit.config;

import static gr.demokritos.iit.netcdftoolkit.config.NetCDFStoreSchema.NETCDFFILE;
import static gr.demokritos.iit.netcdftoolkit.config.NetCDFStoreSchema.URIBASE;

import org.openrdf.model.Graph;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.util.GraphUtil;
import org.openrdf.model.util.GraphUtilException;
import org.openrdf.sail.config.SailConfigException;
import org.openrdf.sail.config.SailImplConfigBase;


/**
 * Created by angel on 10/29/14.
 */
public class NetCDFStoreConfig extends SailImplConfigBase {

    private String netCDFFile;
    private String uriBase;

    public NetCDFStoreConfig() { super(NetCDFStoreFactory.SAIL_TYPE); }

    public NetCDFStoreConfig(String netCDFFile, String uriBase) {
        this();
        setNetCDFFile(netCDFFile);
        setUriBase(uriBase);
    }

    public String getNetCDFFile() { return netCDFFile; }

    public String getUriBase() { return uriBase; }

    public void setNetCDFFile(String netCDFFile) { this.netCDFFile =  netCDFFile; }

    public void setUriBase(String uriBase) { this.uriBase = uriBase; }

    @Override
    public Resource export(Graph graph) {
        // export configuration to rdf.
        Resource implNode = super.export(graph);

        if (netCDFFile != null)
            graph.add(implNode, NETCDFFILE, graph.getValueFactory().createLiteral(netCDFFile));

        if (uriBase != null)
            graph.add(implNode, URIBASE, graph.getValueFactory().createLiteral(uriBase));

        return implNode;
    }

    @Override
    public void parse(Graph graph, Resource implNode)
            throws SailConfigException {
        super.parse(graph, implNode);

        try {
            Literal netCDFFileLit = GraphUtil.getOptionalObjectLiteral(graph, implNode, NETCDFFILE);
            if (netCDFFileLit != null) {
                try {
                    setNetCDFFile((netCDFFileLit).stringValue());
                }
                catch (IllegalArgumentException e) {
                    throw new SailConfigException("String value required for " + NETCDFFILE + " property, found "
                            + netCDFFileLit);
                }
            }
            else {
                throw new SailConfigException("The " + NETCDFFILE + " property is required.");
            }

            Literal  uriBaseLit = GraphUtil.getOptionalObjectLiteral(graph, implNode, URIBASE);

            if (uriBaseLit != null) {
                try {
                    setUriBase((uriBaseLit).stringValue());
                }
                catch (IllegalArgumentException e) {
                    throw new SailConfigException("String value required for " + URIBASE + " property, found "
                            + uriBaseLit);
                }
            }

        } catch (GraphUtilException e) {
            throw new SailConfigException(e.getMessage(), e);
        }
    }


}
