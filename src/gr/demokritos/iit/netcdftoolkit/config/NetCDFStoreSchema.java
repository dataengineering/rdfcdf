package gr.demokritos.iit.netcdftoolkit.config;

import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;

/**
 * Created by angel on 10/29/14.
 */
public class NetCDFStoreSchema {

    public static final String NAMESPACE = "http://iit.demokritos.gr/config/sail/netcdf#";


    public final static URI NETCDFFILE;


    public final static URI URIBASE;

    static {
        ValueFactory factory = ValueFactoryImpl.getInstance();
        NETCDFFILE = factory.createURI(NAMESPACE, "netCDFFile");
        URIBASE = factory.createURI(NAMESPACE, "uriBase");
    }
}
