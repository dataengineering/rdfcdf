package gr.demokritos.iit.netcdftoolkit.loader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

/**
 * 
 */

/**
 * @author Giannis Mouchakis
 *
 */
public class NetCDFHeader {
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		Writer writer = new BufferedWriter( new FileWriter(args[1]) );
		writer.write("@prefix sgstruct: <http://semagrow.eu/rdf/struct/> .\n" );
		writer.write("@prefix sgdata: <http://semagrow.eu/rdf/data/> .\n" );
		writer.write("@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n" );
		writer.write("\n");
		
		File to_triplefy = new File(args[0]);
		int i = 0;
		Collection<File> files = FileUtils.listFiles(to_triplefy, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : files) {
			if (file.getName().endsWith(".nc")) {
				
				String basename = file.getName().replaceAll(".nc", "");
				String dataset_struct = "sgstruct:"+basename+"_struct";
				
				writer.write(dataset_struct + " sgdata:header \"");
				
				NetcdfFile ncfile = NetcdfFile.open(file.getAbsolutePath());
				
				for (Attribute attribute : ncfile.getGlobalAttributes()) {
					writer.write(attribute.getFullName() + " ");
					writer.write(attribute.getStringValue() + " ");
				}
				
				List<Variable> variables = ncfile.getVariables();
				for (Variable variable : variables) {
					
					writer.write(variable.getShortName()  + " ");
					
					for (Attribute attribute : variable.getAttributes()) {
						writer.write(attribute.getFullName() + " ");
						writer.write(attribute.getStringValue() + " ");
					}
				}
				
				ncfile.close();
				writer.write("\"^^xsd:string . \n");
				
				i++;
				System.out.println(i);
			}
		}
		writer.close();
	}

}
