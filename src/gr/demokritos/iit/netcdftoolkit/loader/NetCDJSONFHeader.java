/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.DateTime;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

/**
 * @author Giannis Mouchakis
 *
 */
public class NetCDJSONFHeader {
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws InvalidRangeException 
	 */
	public static void main(String[] args) throws IOException, InvalidRangeException {
		
		Writer writer = new BufferedWriter( new FileWriter(args[1]) );
		writer.write("@prefix sgstruct: <http://semagrow.eu/rdf/struct/> .\n" );
		writer.write("@prefix sgdata: <http://semagrow.eu/rdf/data/> .\n" );
		writer.write("@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n" );
		writer.write("\n");
		
		File to_triplefy = new File(args[0]);
		int i = 0;
		Collection<File> files = FileUtils.listFiles(to_triplefy, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : files) {
			if (file.getName().endsWith(".nc")) {
				String basename = file.getName().replaceAll(".nc", "");
				String triple = "sgstruct:"+basename+"_struct sgdata:jsonHeader \"";
				NetcdfFile ncfile = NetcdfFile.open(file.getAbsolutePath());
				JsonObjectBuilder jfile = Json.createObjectBuilder();
				jfile.add("src_id", UUID.randomUUID().toString());
				jfile.add("src", "isimip");
				jfile.add("dataset", basename);
				jfile.add("quality", "high");
				if (basename.equals("epic_hadgem2-es_rcp2p6_ssp2_co2_firr_yield_whe_annual_2005_2099")
						|| basename.equals("tasmax_bced_1960_1999_hadgem2-es_rcp2p6_2011-2020")
						|| basename.equals("cgms-wofost_agmerra_hist_default_firr_biom_soy_annual_1980_2010")
						|| basename.equals("cgms-wofost_agmerra_hist_default_firr_yield_soy_annual_1980_2010")
						) {
					jfile.add("downloadable", "true");
				} else {
					jfile.add("downloadable", "false");
				}
				List<Attribute> global_attributes = ncfile.getGlobalAttributes();
				for (Attribute attribute : global_attributes) {
					if (attribute.getShortName().equals("title")) {
						jfile.add("title", StringEscapeUtils.escapeHtml4(
								attribute.getValues().getObject(0).toString()));
					} else if (attribute.getShortName().equals("description")) {
						jfile.add("description", StringEscapeUtils.escapeHtml4(
								attribute.getValues().getObject(0).toString()));
					}
				}
				List<Variable> variables = ncfile.getVariables();
				JsonArrayBuilder vars = Json.createArrayBuilder();
				for (Variable variable : variables) {
					JsonObjectBuilder jvar = Json.createObjectBuilder();
					String short_name = variable.getShortName();
					jvar.add("short_name", short_name);
					List<Attribute> variable_attribures = variable.getAttributes();
					for (Attribute attribute : variable_attribures) {
						String attr_name = attribute.getShortName();
						if (attr_name.equals("long_name")) {
							jvar.add("long_name", StringEscapeUtils.escapeHtml4(
									attribute.getValues().getObject(0).toString()));
						} else if (attr_name.equals("units")) {
							jvar.add("units", StringEscapeUtils.escapeHtml4(
									attribute.getValues().getObject(0).toString()));
						}
						if (short_name.equals("lon")){
							jvar.add("from", "-179.75");
							jvar.add("to", "179.75");
						} else if (short_name.equals("lat")) {
							jvar.add("from", "89.75");
							jvar.add("to", "-89.75");
						} else if (short_name.equals("time")) {
							//String[] minMax = queryTime(basename);
							String[] minMax = transformTime(variable);
							jvar.add("from", minMax[0]);
							jvar.add("to", minMax[1]);
						}
					}
					vars.add(jvar);					
				}
				ncfile.close();
				jfile.add("vars", vars);
				JsonObject json = jfile.build();
				String json_str = StringEscapeUtils.escapeXml10(json.toString());
				triple += json_str;
				triple +="\"^^xsd:string . \n";
				writer.write(triple);
				i++;
				System.out.println(i);
			}
		}
		writer.close();
	}

	static String[] queryTime(String file) {
		String endpoint = "http://143.233.226.44:8080/SemaGrow/sparql";
		String[] minMax = {"", ""};
		String query_str="PREFIX qb: <http://purl.org/linked-data/cube#> "
				+ "PREFIX sgstruct: <http://semagrow.eu/rdf/struct/> "
				+ "PREFIX nc: <http://rdf.iit.demokritos.gr/2014/netcdf#> "
				+ "PREFIX sgdata: <http://semagrow.eu/rdf/data/> "
				+ "PREFIX  xsd: <http://www.w3.org/2001/XMLSchema#> "
				+ "SELECT ?min_value ?max_value "
				+ "{ "
				+ "<http://semagrow.eu/rdf/data/"+file+"> qb:structure ?dataset_struct . "
				+ "?dataset_struct sgdata:minTimeValue ?min_value . "
				+ "?dataset_struct sgdata:maxTimeValue ?max_value . "
				+ "} ";
		Query query = QueryFactory.create(query_str);
		ARQ.getContext().setTrue(ARQ.useSAX);
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);			
		ResultSet resultSet = qexec.execSelect();				
		while (resultSet.hasNext()) {
			QuerySolution soln = resultSet.next();			
			RDFNode min_n = soln.get("min_value");
			String min = min_n.asLiteral().getLexicalForm();
			RDFNode max_n = soln.get("max_value");
			String max = max_n.asLiteral().getLexicalForm();
			minMax[0] = min;
			minMax[1] = max;
		}	
		qexec.close();
		return minMax;
	}
	
	static String[] transformTime(Variable variable) throws IOException {
		List<Attribute> attributes = variable.getAttributes();
		String units = "";
		for (Attribute attribute : attributes) {
			if (attribute.getShortName().equals("units")) {
				units = attribute.getValue(0).toString();
			}
		}
		String[] units_spl = units.split(" ");//e.g. days since 1860-01-01 00:00:00
		String[] minMax = {"", ""};
		Array values = variable.read();
		Object min_value = values.getObject(0);
		Object max_value = values.getObject((int) (values.getSize() - 1));
		DateTime dateTime = DateTime.parse(units_spl[2]+"T"+units_spl[3]+"Z");
		if (units_spl[0].equals("years")) {
			minMax[0] = dateTime.plusYears((new Double(min_value.toString()).intValue())).toString();
			minMax[1] = dateTime.plusYears((new Double(max_value.toString()).intValue())).toString();
		} else if (units_spl[0].equals("months")) {
			minMax[0] = dateTime.plusMonths((new Double(min_value.toString()).intValue())).toString();
			minMax[1] = dateTime.plusMonths((new Double(max_value.toString()).intValue())).toString();
		} else if (units_spl[0].equals("days")) {
			minMax[0] = dateTime.plusDays((new Double(min_value.toString()).intValue())).toString();
			minMax[1] = dateTime.plusDays((new Double(max_value.toString()).intValue())).toString();
		} else if (units_spl[0].equals("hours")) {
			minMax[0] = dateTime.plusHours((new Double(min_value.toString()).intValue())).toString();
			minMax[1] = dateTime.plusHours((new Double(max_value.toString()).intValue())).toString();
		} else if (units_spl[0].equals("minutes")) {
			minMax[0] = dateTime.plusMinutes((new Double(min_value.toString()).intValue())).toString();
			minMax[1] = dateTime.plusMinutes((new Double(max_value.toString()).intValue())).toString();
		} else if (units_spl[0].equals("seconds")) {
			minMax[0] = dateTime.plusSeconds((new Double(min_value.toString()).intValue())).toString();
			minMax[1] = dateTime.plusSeconds((new Double(max_value.toString()).intValue())).toString();
		} else {
			System.err.println("Did not parse units "+units);
			return null;
		}
		return minMax;
	}
	
}
