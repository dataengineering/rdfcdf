/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import gr.demokritos.iit.netcdftoolkit.loader.Dataset;
import gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF;

import java.io.File;
import java.io.IOException;

import ucar.ma2.InvalidRangeException;

/**
 * @author Giannis Mouchakis
 *
 */
public class LoadAll {

	/**
	 * @param args
	 * @throws InvalidRangeException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InvalidRangeException {
		
		File to_triplefy = new File(args[0]);
		for (File file : to_triplefy.listFiles()) {
			if (file.getName().endsWith(".nc")) {
				String netcdf_basename = file.getAbsolutePath().replaceAll(".nc", "");
				String basename = file.getName().replaceAll(".nc", "");
				NetCDFToRDF netcdf_to_rdf = new NetCDFToRDF(netcdf_basename + ".ttl" , Dataset.ISIMIP, true);
				netcdf_to_rdf.init();
				netcdf_to_rdf.add( netcdf_basename + ".nc", basename );
				netcdf_to_rdf.close();
			}
		}
		
	}

}
