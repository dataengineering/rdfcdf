/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import gr.demokritos.iit.netcdftoolkit.loader.Dataset;
import gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF;

import java.io.IOException;

import ucar.ma2.InvalidRangeException;

/**
 * @author Giannis Mouchakis
 *
 */
public class Load5 {

	/**
	 * @param args
	 * @throws InvalidRangeException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InvalidRangeException {
		
		String netcdf_basename_aet = System.getProperty("user.home") + "/small_netcdfs/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_aet_whe_annual_2005_2099_t";
		NetCDFToRDF netcdf_to_rdf_aet = new NetCDFToRDF(netcdf_basename_aet + ".ttl" , Dataset.ISIMIP);
		netcdf_to_rdf_aet.init();
		netcdf_to_rdf_aet.add( netcdf_basename_aet + ".nc", "epic_hadgem2-es_rcp4p5_ssp2_co2_firr_aet_whe_annual_2005_2099_t" );
		netcdf_to_rdf_aet.close();
		
		String netcdf_basename_biom = System.getProperty("user.home") + "/small_netcdfs/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t";
		NetCDFToRDF netcdf_to_rdf_biom = new NetCDFToRDF(netcdf_basename_biom + ".ttl" , Dataset.ISIMIP);
		netcdf_to_rdf_biom.init();
		netcdf_to_rdf_biom.add( netcdf_basename_biom + ".nc", "epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t" );
		netcdf_to_rdf_biom.close();
		
		netcdf_basename_aet = System.getProperty("user.home") + "/small_netcdfs/orchidee_hadgem2-es_rcp8p5_nosoc_co2_csoil_monthly_2005_2099_t";
		netcdf_to_rdf_aet = new NetCDFToRDF(netcdf_basename_aet + ".ttl" , Dataset.ISIMIP);
		netcdf_to_rdf_aet.init();
		netcdf_to_rdf_aet.add( netcdf_basename_aet + ".nc", "orchidee_hadgem2-es_rcp8p5_nosoc_co2_csoil_monthly_2005_2099_t" );
		netcdf_to_rdf_aet.close();
		
		netcdf_basename_biom = System.getProperty("user.home") + "/small_netcdfs/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t";
		netcdf_to_rdf_biom = new NetCDFToRDF(netcdf_basename_biom + ".ttl" , Dataset.ISIMIP);
		netcdf_to_rdf_biom.init();
		netcdf_to_rdf_biom.add( netcdf_basename_biom + ".nc", "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t" );
		netcdf_to_rdf_biom.close();
		
		netcdf_basename_aet = System.getProperty("user.home") + "/small_netcdfs/watergap_hadgem2-es_rcp4p5_nosoc_dis_monthly_2005_2099_t";
		netcdf_to_rdf_aet = new NetCDFToRDF(netcdf_basename_aet + ".ttl" , Dataset.ISIMIP);
		netcdf_to_rdf_aet.init();
		netcdf_to_rdf_aet.add( netcdf_basename_aet + ".nc", "watergap_hadgem2-es_rcp4p5_nosoc_dis_monthly_2005_2099_t" );
		netcdf_to_rdf_aet.close();
		
	}

}
