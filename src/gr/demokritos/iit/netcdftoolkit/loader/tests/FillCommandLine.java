/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import java.io.File;
import java.io.IOException;

import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;

/**
 * @author Giannis Mouchakis
 *
 */
public class FillCommandLine {

	/**
	 * @param args
	 * @throws RepositoryException 
	 * @throws IOException 
	 * @throws RDFParseException 
	 */
	public static void main(String[] args) throws RepositoryException, RDFParseException, IOException {
		String filepath = args[0];
		String repository_name = args[1];
		Repository repo = new HTTPRepository("http://83.212.119.138:8081/openrdf-sesame", repository_name);
		repo.initialize();
		RepositoryConnection con = repo.getConnection();
		File file = new File(filepath);
		String baseURI = "";
		con.add(file, baseURI, RDFFormat.TURTLE);
		con.close();
		repo.shutDown();
	}

}
