/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import java.io.IOException;

import ucar.ma2.InvalidRangeException;
import gr.demokritos.iit.netcdftoolkit.loader.Dataset;
import gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF;

/**
 * @author Giannis Mouchakis
 *
 */
public class LoadFromCommandLine {

	/**
	 * @param args
	 * @throws InvalidRangeException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InvalidRangeException {
		
		String filepath = args[0];
		String basename = args[1];
		String netcdf_basename = filepath.replaceFirst(".nc", "");
		
		NetCDFToRDF netcdf_to_rdf = new NetCDFToRDF(netcdf_basename + ".ttl" , Dataset.ISIMIP, true);
		netcdf_to_rdf.init();
		netcdf_to_rdf.add( netcdf_basename + ".nc", basename );
		netcdf_to_rdf.close();

	}

}
