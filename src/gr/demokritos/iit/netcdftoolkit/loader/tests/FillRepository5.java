/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import java.io.File;
import java.io.IOException;

import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;

/**
 * @author Giannis Mouchakis
 *
 */
public class FillRepository5 {

	/**
	 * @param args
	 * @throws RepositoryException 
	 * @throws IOException 
	 * @throws RDFParseException 
	 */
	public static void main(String[] args) throws RepositoryException, RDFParseException, IOException {
		Repository repo = new HTTPRepository("http://83.212.119.138:8081/openrdf-sesame", "small_files");
		repo.initialize();
		RepositoryConnection con = repo.getConnection();
		File file_aet = new File(System.getProperty("user.home") + "/small_netcdfs/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_aet_whe_annual_2005_2099_t.ttl");
		File file_biom = new File(System.getProperty("user.home") + "/small_netcdfs/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t.ttl");
		File file_orchidee = new File(System.getProperty("user.home") + "/small_netcdfs/orchidee_hadgem2-es_rcp8p5_nosoc_co2_csoil_monthly_2005_2099_t.ttl");
		File file_uas = new File(System.getProperty("user.home") + "/small_netcdfs/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.ttl");
		File file_watergap = new File(System.getProperty("user.home") + "/small_netcdfs/watergap_hadgem2-es_rcp4p5_nosoc_dis_monthly_2005_2099_t.ttl");
		String baseURI = "";
		con.add(file_aet, baseURI, RDFFormat.TURTLE);
		con.add(file_biom, baseURI, RDFFormat.TURTLE);
		con.add(file_orchidee, baseURI, RDFFormat.TURTLE);
		con.add(file_uas, baseURI, RDFFormat.TURTLE);
		con.add(file_watergap, baseURI, RDFFormat.TURTLE);
		con.close();
		repo.shutDown();
	}

}
