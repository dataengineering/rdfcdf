/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import java.io.File;
import java.io.IOException;

import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;

/**
 * @author Giannis Mouchakis
 *
 */
public class FillTestRepository {

	/**
	 * @param args
	 * @throws RepositoryException 
	 * @throws IOException 
	 * @throws RDFParseException 
	 */
	public static void main(String[] args) throws RepositoryException, RDFParseException, IOException {
		Repository repo = new HTTPRepository("http://localhost:8080/openrdf-sesame", "test_creator");
		repo.initialize();
		RepositoryConnection con = repo.getConnection();
		File file = new File("resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.ttl");
		String baseURI = "";
		con.add(file, baseURI, RDFFormat.TURTLE);
		con.close();
		repo.shutDown();
	}

}
