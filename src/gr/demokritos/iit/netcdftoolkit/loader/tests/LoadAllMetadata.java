/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import gr.demokritos.iit.netcdftoolkit.loader.Dataset;
import gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import ucar.ma2.InvalidRangeException;

/**
 * @author Giannis Mouchakis
 *
 */
public class LoadAllMetadata {

	/**
	 * @param args
	 * @throws InvalidRangeException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InvalidRangeException {	
		File to_triplefy = new File(args[0]);
		String output_dir = args[1];
		int i = 0;
		Collection<File> files = FileUtils.listFiles(to_triplefy, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		System.out.println(files.size());
		for (File file : files) {
			if (file.getName().endsWith(".nc")) {
				String basename = file.getName().replaceAll(".nc", "");
				NetCDFToRDF netcdf_to_rdf = new NetCDFToRDF(output_dir+basename+".ttl" , Dataset.ISIMIP, true, false);
				netcdf_to_rdf.init();
				netcdf_to_rdf.add( file.getAbsolutePath(), basename );
				netcdf_to_rdf.close();
				i++;
			}
			System.out.println(i);
		}
	}

}
