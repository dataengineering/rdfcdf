/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import gr.demokritos.iit.netcdftoolkit.loader.Dataset;
import gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF;

import java.io.IOException;

import ucar.ma2.InvalidRangeException;

/**
 * @author Giannis Mouchakis
 *
 */
public class LoaderTest {

	/**
	 * @param args
	 * @throws InvalidRangeException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InvalidRangeException {
		
		String netcdf_basename = System.getProperty("user.home") + "/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_yield_bar_annual_2005_2099";
		
		NetCDFToRDF netcdf_to_rdf = new NetCDFToRDF(netcdf_basename + ".ttl" , Dataset.ISIMIP, true);
		netcdf_to_rdf.init();
		netcdf_to_rdf.add( netcdf_basename + ".nc", "epic_hadgem2-es_rcp4p5_ssp2_co2_firr_yield_bar_annual_2005_2099" );
		netcdf_to_rdf.close();
		
	}

}
