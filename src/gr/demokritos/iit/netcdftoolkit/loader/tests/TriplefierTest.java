/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader.tests;

import static org.junit.Assert.assertTrue;
import gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.Model;
import org.openrdf.model.util.ModelUtil;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.Rio;
import org.openrdf.rio.UnsupportedRDFormatException;

/**
 * @author Giannis Mouchakis
 *
 */
public class TriplefierTest {
	
	private Path slow_path;
	private Path fast_path;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		String netcdf_file = "resources/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.nc";
		String ttl_fast = "resources/fast.ttl";
		String ttl_slow = "resources/slow.ttl";
		File ff = new File(ttl_fast);
		File sf = new File(ttl_slow);
		fast_path = Paths.get(ff.toURI());
		slow_path = Paths.get(sf.toURI());
		NetCDFToRDF fast_n_to_rdf = new NetCDFToRDF( ttl_fast , null, true);
		fast_n_to_rdf.init();
		fast_n_to_rdf.add( netcdf_file, "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t" );
		fast_n_to_rdf.close();
		NetCDFToRDF slow_n_to_rdf = new NetCDFToRDF( ttl_slow , null);
		slow_n_to_rdf.init();
		slow_n_to_rdf.add( netcdf_file, "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t" );
		slow_n_to_rdf.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		Files.delete(fast_path);
		Files.delete(slow_path);
	}

	@Test
	public void test() throws RDFParseException, UnsupportedRDFormatException, IOException  {
        InputStream f_is = new FileInputStream("resources/fast.ttl");
        RDFFormat f_f = Rio.getParserFormatForFileName("resources/fast.ttl");
        Model f_model = Rio.parse(f_is, "", f_f);
        f_is.close();
        InputStream s_is = new FileInputStream("resources/slow.ttl");
        RDFFormat s_f = Rio.getParserFormatForFileName("resources/slow.ttl");
        Model s_model = Rio.parse(s_is, "", s_f);
        s_is.close();
        assertTrue(ModelUtil.equals(f_model, s_model));
        
	}


}
