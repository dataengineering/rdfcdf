/***************
<p>Title: NetCDFToRDF</p>

<p>Description:
Converter from NetCDF to RDF.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Sotiris Konstantinidis (SemaGrow 2013-2014)
@author Georgios Kalampokinis (SemaGrow 2014)
@author Giannis Mouchakis (SemaGrow 2014)

***************/


package gr.demokritos.iit.netcdftoolkit.loader;

import gr.demokritos.iit.netcdftoolkit.commons.NetCDFUtils;
import gr.demokritos.iit.netcdftoolkit.commons.NetCDFVocab;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;


public class NetCDFToRDF
{
	static Logger logger = LoggerFactory.getLogger(NetCDFToRDF.class);

	private final BufferedWriter out;
	private final IOException ex;
	private Dataset dataset;
	private boolean inited = false;
	private boolean writtenLat = false;
	private boolean writtenLon = false;
	private boolean fastTriplefication = false;
	private boolean getObservations = true;


	public NetCDFToRDF( String rdfttl_filename, Dataset dataset )
	{
		this.dataset = dataset;
		BufferedWriter temp_out;
		IOException temp_ex;
		try {
			temp_out = new BufferedWriter( new FileWriter(rdfttl_filename) );
			temp_ex = null;
		}
		catch( IOException ex ) {
			temp_out = null;
			temp_ex = ex;
		}
		this.out = temp_out;
		this.ex = temp_ex;
	}
	
	public NetCDFToRDF( String rdfttl_filename, Dataset dataset, boolean fastTriplefication ) {
		this.dataset = dataset;
		this.fastTriplefication = fastTriplefication;
		BufferedWriter temp_out;
		IOException temp_ex;
		try {
			temp_out = new BufferedWriter( new FileWriter(rdfttl_filename) );
			temp_ex = null;
		}
		catch( IOException ex ) {
			temp_out = null;
			temp_ex = ex;
		}
		this.out = temp_out;
		this.ex = temp_ex;
	}
	
	public NetCDFToRDF( String rdfttl_filename, Dataset dataset, boolean fastTriplefication, boolean getObservations ) {
		this.dataset = dataset;
		this.fastTriplefication = fastTriplefication;
		this.getObservations = getObservations;
		BufferedWriter temp_out;
		IOException temp_ex;
		try {
			temp_out = new BufferedWriter( new FileWriter(rdfttl_filename) );
			temp_ex = null;
		}
		catch( IOException ex ) {
			temp_out = null;
			temp_ex = ex;
		}
		this.out = temp_out;
		this.ex = temp_ex;
	}


	public void init()
	throws IOException
	{
		if( this.out == null ) { throw this.ex; }

		for( String pref : NetCDFVocab.prefixMapping.getNsPrefixMap().keySet() ) {
			String exp = NetCDFVocab.prefixMapping.getNsPrefixURI( pref );
			this.out.write( "@prefix " + pref + ": <" + exp + "> .\n" );
		}
		
		this.writtenLat = false;
		this.writtenLon = false;
		
		this.inited = true;
	}


	public void close()
	throws IOException
	{
		
		this.writtenLat = false;
		this.writtenLon = false;
		
		if( this.out != null ) { this.out.close(); }
	}

	
	public void add( String netcdf_filename, String uribase )
	throws IOException, InvalidRangeException
	{
		if( ! this.inited ) { this.init(); }
				
		String dataset_name = NetCDFVocab.NS_DATA + uribase;
		String struct_name = NetCDFVocab.NS_STRUCT + uribase + "_struct";
		
		NetcdfFile ncfile = NetcdfFile.open( netcdf_filename );

		List<Variable> var_list = ncfile.getVariables();
		List<Dimension> dim_list = ncfile.getDimensions();

		List<Variable> dimension_variables = new ArrayList<Variable>();
		List<Variable> measurement_variables = new ArrayList<Variable>();
		List<Array> dimension_data_list = new ArrayList<Array>();

		// read data for each dimension variable and add to the list.
		for (Variable variable : var_list) {
			if (isDimensionVar(variable, dim_list)) {
				// for dimension variables, record their existence
				dimension_variables.add(variable);
				// .. and the array of possible values
				// These values are the allowed indices for each dimension when reading measurements
				dimension_data_list.add( variable.read() );
			}
			else {
				// for measurement variables, only record their existence
				measurement_variables.add(variable);
			}
		}
		
		// writes the main dataset instance and its global attributes
		addMetadata( ncfile, dataset_name, struct_name, var_list, dimension_variables, dim_list, dimension_data_list, uribase);
		
		/*if (measurement_variables.size()>1) {
			logger.error("this does not work for netCDF files with more than one measurement variables");
			System.exit(1);
		}*/
		
		for (Variable measurement_variable : measurement_variables) {
			
			//add dimension dataset and struct 
			String var_dataset = dataset_name + "_" + measurement_variable.getShortName();
			String var_dataset_struct = var_dataset + "_struct";
			out.write("\n" + var_dataset + " a " + NetCDFVocab.strDataset + " ."); 
			out.write("\n" + var_dataset + " " + NetCDFVocab.strDatasetStructure + " " + var_dataset_struct + " .");
			out.write("\n" + var_dataset_struct + " a " + NetCDFVocab.strStructure + " .");
			String var_name = getVarStructName(measurement_variable, struct_name);
			out.write("\n" + var_dataset_struct + " " + NetCDFVocab.strStructureVariable + " " + var_name + " .");
						
			//order dimensions according to measurement variable's dimensions
			List<Variable> dim_var_ordered = new ArrayList<Variable>();
			// These values are the allowed indices for each dimension when reading measurements
			List<Array> dim_data_odered = new ArrayList<Array>();
				
			List<Dimension> dimensions = measurement_variable.getDimensions();
			for (Dimension dimension : dimensions) {
				for (Variable dim_var : dimension_variables) {
					if (dimension.getShortName().equals(dim_var.getShortName())) {
						dim_var_ordered.add(dim_var);
						dim_data_odered.add(dim_var.read());
						//add variable structs as components
						String var_dim_name = getVarStructName(dim_var, struct_name);
						out.write("\n" + var_dataset_struct + " " + NetCDFVocab.strStructureVariable + " " + var_dim_name + " .");
					}
				}
			}
			
			String m_prop_name = getPropName(measurement_variable);
			String data_type = NetCDFUtils.getXsdType(measurement_variable.getDataType());
			
			if (this.getObservations) {
				if (fastTriplefication) {//load values in memory and start writing.
					Array results = measurement_variable.read();
					Long count = 0l;//start from the end to match slow writing counter
					IndexIterator it = results.getIndexIterator();
					while(it.hasNext()) {
						Object result = it.next();
						int[] indices = it.getCurrentCounter();
						logger.debug(Arrays.toString(indices));
						String obs = var_dataset + "_obs_" + count.toString();
						writeVariableValues(indices, dim_var_ordered, 
								measurement_variable, var_dataset, obs, m_prop_name,
								data_type, struct_name, result);
						count++;
					}
				} else {//read values one by one and write.
				
					int n = dim_data_odered.size();
					int[] indices = new int[n];
					long[] ranges = new long[n];
					int i = 0;
					for (Array array : dim_data_odered) {
						ranges[i] = array.getSize();
						i++;
					}
							
					// writes observations
					// Start off at indices = [0,...,0]
					Integer count = 0;
					do {
						logger.debug( Arrays.toString(indices) );
						String obs = var_dataset + "_obs_" + count.toString();
						writeVariableValues(indices, dim_var_ordered, 
								measurement_variable, var_dataset, obs, m_prop_name,
								data_type, struct_name);
						advanceIndices(n, indices, ranges);
						++count;
					}
					while( ! allMaxed(n, indices, ranges) );
						//do last one
						String obs = var_dataset + "_obs_" + count.toString();
						logger.debug( Arrays.toString(indices) );
						writeVariableValues(indices, dim_var_ordered, 
								measurement_variable, var_dataset, obs, m_prop_name, data_type, struct_name);
					}
				 
			}
		}
		
		ncfile.close();
		
		this.out.flush();
	}
	
	private void writeVariableValues( int[] indices,
			List<Variable> sensor_variables, Variable measurement_variable, 
			String dataset_name, String observation_name, 
			String m_prop_name, String data_type, String struct_name, Object result )
	throws IOException, InvalidRangeException {
		int x = 0;
		for (Variable variable : sensor_variables) {
			String p = getPropName(variable);
			String var_name = getVarStructName(variable, struct_name);
			String pos = getPos(var_name, indices[x]);
			out.write("\n" + observation_name + " " + p + " " + pos + " . ");		
			x++;
		}
		//write measurement
		out.write("\n" + observation_name + " " + m_prop_name + " ");
		out.write("\"" + result.toString() + "\"");
		out.write(data_type + " .");
		out.write("\n" + observation_name + " " + NetCDFVocab.strObservationDataset + " " + dataset_name + " ." );
	}

	private void writeVariableValues( int[] indices,
			List<Variable> sensor_variables, Variable measurement_variable, 
			String dataset_name, String observation_name, 
			String m_prop_name, String data_type, String struct_name )
	throws IOException, InvalidRangeException
	{
		
		int x = 0;
		for (Variable variable : sensor_variables) {
			String p = getPropName(variable);
			/*out.write("\n" + observation_name + " " + p + " ");
			out.write("\"" + sensor_variables_data.get(x).getObject(indices[x]).toString() + "\"");
			out.write(NetCDFUtils.getXsdType(variable.getDataType()));
			out.write(" .");*/
			String var_name = getVarStructName(variable, struct_name);
			String pos = getPos(var_name, indices[x]);
			out.write("\n" + observation_name + " " + p + " " + pos + " . ");		
			x++;
		}
		
		String sectionSpec = "";//used to read values in the grid
		for (int i = 0; i < indices.length - 1; i++) {
			sectionSpec += indices[i] + ":" + indices[i] + ", ";
		}
		sectionSpec += indices[indices.length - 1] + ":" + indices[indices.length - 1];
		
		//write measurement
		Array result = measurement_variable.read(sectionSpec);
		out.write("\n" + observation_name + " " + m_prop_name + " ");
		out.write("\"" + result.getObject(0).toString() + "\"");
		out.write(data_type + " .");

		out.write("\n" + observation_name + " " + NetCDFVocab.strObservationDataset + " " + dataset_name + " ." );
	}
	
	
	// Advances 'indices' to the next in sequence.
    private void advanceIndices(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i]+1 == ranges[i]) {
                indices[i] = 0;
            }
            else {
                indices[i] += 1;
                break;
            }
        }

    }

    // Tests if indices are in final position.
    private boolean allMaxed(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i] != ranges[i]-1) {
                return false;
            }
        }
        return true;

    }

 

    /**
     * Private helper for instantiating a dataset and its structure
     * and writing global attributes.
     * 
     * @param ncfile The input NetCDF file handle
     * @param dataset_name
     * @param struct_name
     * @param var_list
     * @param dimension_variables
     * @param dim_list
     * @param dimension_data_list
     * @throws IOException
     */

	private void addMetadata(NetcdfFile ncfile,
			String dataset_name, String struct_name, List<Variable> var_list,
			List<Variable> dimension_variables, List<Dimension> dim_list, List<Array> dimension_data_list, String uribase)
			throws IOException {
		
		//write file info
		out.write("\n" + NetCDFVocab.strFile + " " + NetCDFVocab.strRdfValue+ " \"" + uribase + "\"^^xsd:string .");
		out.write("\n" + dataset_name + " " + NetCDFVocab.strFromFile + " \"" + uribase + "\"^^xsd:string .");
		//write dataset and struct
		out.write("\n" + dataset_name + " a " + NetCDFVocab.strDataset + " ."); 
		out.write("\n" + dataset_name + " " + NetCDFVocab.strDatasetStructure + " " + struct_name + " .");
		out.write("\n" + struct_name + " a " + NetCDFVocab.strStructure + " .");
		
		// Write variables
		int x = 0;
		for (Variable variable : var_list) {
			
			String prop = getPropName(variable);

			if (prop.equals("cf:longitude")) {
				//String var_name = NetCDFVocab.NS_STRUCT + this.dataset.toString() + "_longitude";
				String var_name = getVarStructName(variable, struct_name);
				out.write("\n" + struct_name + " " + NetCDFVocab.strStructureVariable + " " + var_name + " .");
				if (!writtenLon) {
					writeVariable(var_name, prop, struct_name, 0, variable, dimension_variables, dim_list, dimension_data_list);
					writtenLon = true;
				}
			} else if (prop.equals("cf:latitude")) {
				//String var_name = NetCDFVocab.NS_STRUCT + this.dataset.toString() + "_latitude";
				String var_name = getVarStructName(variable, struct_name);
				out.write("\n" + struct_name + " " + NetCDFVocab.strStructureVariable + " " + var_name + " .");
				if (!writtenLat) {
					writeVariable(var_name, prop, struct_name, 1, variable, dimension_variables, dim_list, dimension_data_list);
					writtenLat = true;
				}
			} else {
				//String var_name = struct_name + "_" + variable.getShortName();
				String var_name = getVarStructName(variable, struct_name);
				out.write("\n" + struct_name + " " + NetCDFVocab.strStructureVariable + " " + var_name + " .");
				writeVariable(var_name, prop, struct_name, x, variable, dimension_variables, dim_list, dimension_data_list);
			}
			
			x++;
		}
		
		writeGlobalAttributes(ncfile, dataset_name, struct_name);
		
    }
	
 
    /**
     * Private helper checking if a variable is a dimension.
     * 
     * @param variable
     * @param dim_list
     */
	private boolean isDimensionVar(Variable variable, List<Dimension> dim_list)
	{
		for( Dimension dimension : dim_list ) {
			String dimName = dimension.getShortName();
			if( (dimName != null) && dimName.equals(variable.getShortName()) )
			{ return true; }
		}
		return false;
	}
	
	/**
	 * 
	 * method to get the property name for a variable. 
	 * 
	 * @param variable
	 * @return the property name. if variable has standard name then 
	 * name=cf:&lt;stantard_name&gt; else name=sgdata:&lt;short_name&gt;
	 */
	private String getPropName(Variable variable) {
		String prop = null;
		Attribute name_attr = variable.findAttribute("standard_name");
		if (name_attr == null) {
			prop = NetCDFVocab.NS_DATA + variable.getShortName();
		} else {
			prop = NetCDFVocab.CF + name_attr.getStringValue();
		}
		return prop;
	}
	
	
	private void writeVariable(String var_name, String prop, String struct_name, int order, Variable variable, List<Variable> dimension_variables, List<Dimension> dim_list, List<Array> dimension_data_list) throws IOException {
				
		//out.write("\n" + struct_name + " " + NetCDFVocab.strStructureVariable + " " + var_name + " .");
		
		out.write("\n" + var_name + " a " + NetCDFVocab.strVariable + " .");
		out.write("\n" + var_name + " " + NetCDFVocab.strVariableOrder + " \"" + order + "\"^^xsd:int .");
		out.write("\n" + var_name + " " + NetCDFVocab.strVariableProperty + " " + prop + " .");
		if (isDimensionVar(variable, dim_list)) {
			out.write("\n" + prop + " a " + NetCDFVocab.strDimension + " . ");
		} else {
			out.write("\n" + prop + " a " + NetCDFVocab.strMeasure + " . ");
		}
		out.write("\n" + var_name + " " + NetCDFVocab.strVariableRequired + " \"1\"^^xsd:boolean .");
		out.write("\n" + var_name + " " + NetCDFVocab.strVariableAttachment + " " + NetCDFVocab.strDataset + " ." );
		out.write("\n" + var_name + " " + NetCDFVocab.strShortName + " \"" + variable.getShortName() + "\"^^xsd:string ." );
		
		//datatype
		String type = NetCDFUtils.getDataTypeToString(variable.getDataType());
		String typePrefix = type + ":";
		out.write("\n" + var_name + " " + NetCDFVocab.strDataType + " <" + NetCDFVocab.prefixMapping.expandPrefix(typePrefix)  + "> ." );
		
		// Codelist
		int index = dimension_variables.indexOf(variable);
		if (index != -1) {//if variable is dimension
			
			// make up codelist instance name
			String cl = var_name + "_codelist";
			out.write("\n" + var_name + " " + NetCDFVocab.strVariableCodelist + " " + cl + " ." );
			
			String clr = "\n" + cl + " " + NetCDFVocab.strCodelistRoot + " ";
			
			Array var_values = dimension_data_list.get(index);
			Long size = var_values.getSize();
			for (int i = 0; i < size; i++) {
				String value = var_values.getObject(i).toString();
				String pos = getPos(var_name, i);
				//out.write(clr + "<" + NetCDFVocab.prefixMapping.expandPrefix(typePrefix + value)  + "> ." );
				out.write(clr + pos + " . ");
				out.write("\n" + pos + " " + NetCDFVocab.strRdfValue + " \"" + value + "\"" + NetCDFUtils.getXsdType(variable.getDataType()) + " . ");
				out.write("\n" + pos + " " + NetCDFVocab.strIndex + " \"" + i + "\"^^xsd:long . ");
			}
			out.write("\n" + var_name + " " + NetCDFVocab.strDimensionSize + " \"" + size.toString() + "\"^^xsd:long . " );
				
		}
		
		writeVariableAttributes(variable, var_name);
		
	}
	
	private void writeGlobalAttributes(NetcdfFile ncfile, String dataset_name, String struct_name) throws IOException {
		
		List<Attribute> attribute_list = ncfile.getGlobalAttributes();
		
		for (Attribute attribute : attribute_list) {

			String attr_name = attribute.getShortName();
			Array attribute_values = attribute.getValues();
			String attr_resource_str;
			com.hp.hpl.jena.rdf.model.Resource r;
			r = NetCDFVocab.KnownNetCDFProperties.get(attr_name);
			if (r == null) {
				attr_resource_str = NetCDFVocab.NS_DATA + attr_name;
			} else {
				attr_resource_str = "<" + r.getURI() + ">";
			}
			String spec = struct_name + "_" + attr_name + "_spec";
			
			out.write("\n" + struct_name + " " + NetCDFVocab.strStructureVariable + " " + spec + " .");
			out.write("\n" + spec + " a " + NetCDFVocab.strVariable + " .");
			out.write("\n" + spec + " " + NetCDFVocab.strVariableProperty + " " + attr_resource_str + " .");
			out.write("\n" + spec + " " + NetCDFVocab.strShortName + " \"" + attribute.getShortName() + "\"^^xsd:string ." );
			out.write("\n" + attr_resource_str + " a " + NetCDFVocab.strAttribute + " .");
			
			// write values
			for (int i = 0; i < attribute_values.getSize(); i++) {
				//write two triples for values for now and decide later...
				out.write("\n" + spec + " " + NetCDFVocab.strAttrValue + " ");
				out.write("\"" + StringEscapeUtils.escapeHtml4(attribute_values.getObject(i).toString()) + "\"");
				out.write(NetCDFUtils.getXsdType(attribute.getDataType()));
				out.write(" .");
				
				out.write("\n" + dataset_name + " " + attr_resource_str + " ");
				out.write("\"" + StringEscapeUtils.escapeHtml4(attribute_values.getObject(i).toString()) + "\"");
				out.write(NetCDFUtils.getXsdType(attribute.getDataType()));
				out.write(" .");
			}
			
		}
		
	}
	
	
	private void writeVariableAttributes(Variable variable, String var_name) throws IOException {
		
		List<Attribute> attribute_list = variable.getAttributes();
		
		for (Attribute attribute : attribute_list) {
			
			String attr_name = attribute.getShortName();
			Array attribute_values = attribute.getValues();
			String attr_resource_str = NetCDFVocab.NS_DATA + attr_name;
			String spec = var_name + "_" + attr_name + "_spec";
			
			out.write("\n" + var_name + " " + NetCDFVocab.strStructureVariable + " " + spec + " .");
			out.write("\n" + spec + " a " + NetCDFVocab.strVariable + " .");
			out.write("\n" + spec + " " + NetCDFVocab.strVariableProperty + " " + attr_resource_str + " .");
			out.write("\n" + spec + " " + NetCDFVocab.strShortName + " \"" + attribute.getShortName() + "\"^^xsd:string ." );
			out.write("\n" + attr_resource_str + " a " + NetCDFVocab.strAttribute + " .");
			
			// write values
			for (int i = 0; i < attribute_values.getSize(); i++) {
				//write two triples for values for now and decide later...
				out.write("\n" + spec + " " + NetCDFVocab.strAttrValue + " ");
				out.write("\"" + StringEscapeUtils.escapeHtml4(attribute_values.getObject(i).toString()) + "\"");
				out.write(NetCDFUtils.getXsdType(attribute.getDataType()));
				out.write(" .");
				
				out.write("\n" + var_name + " " + attr_resource_str + " ");
				out.write("\"" + StringEscapeUtils.escapeHtml4(attribute_values.getObject(i).toString()) + "\"");
				out.write(NetCDFUtils.getXsdType(attribute.getDataType()));
				out.write(" .");
			}
			
		}
	}
	
	private String getVarStructName(Variable variable, String struct_name) {
		String var_name = null;
		/*String prop = getPropName(variable);
		if (prop.equals("cf:longitude")) {
			var_name = NetCDFVocab.NS_STRUCT + this.dataset.toString() + "_longitude";
		} else if (prop.equals("cf:latitude")) {
			var_name = NetCDFVocab.NS_STRUCT + this.dataset.toString() + "_latitude";

		} else {*/
			var_name = struct_name + "_" + variable.getShortName();
		//}
		return var_name;
	}
	
	private String getPos(String var_name, int i) {
		return var_name + "_pos_" + i;
	}

}
