/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.loader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

/**
 * @author Giannis Mouchakis
 *
 */
public class DatesTriples {
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		Writer writer = new BufferedWriter( new FileWriter(args[1]) );
		writer.write("@prefix sgstruct: <http://semagrow.eu/rdf/struct/> .\n" );
		writer.write("@prefix sgdata: <http://semagrow.eu/rdf/data/> .\n" );
		writer.write("@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n" );
		writer.write("\n");
		
		File to_triplefy = new File(args[0]);
		int i = 0;
		Collection<File> files = FileUtils.listFiles(to_triplefy, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : files) {
			if (file.getName().endsWith(".nc")) {
				String basename = file.getName().replaceAll(".nc", "");
				String dataset_struct = "sgstruct:"+basename+"_struct";
				NetcdfFile ncfile = NetcdfFile.open(file.getAbsolutePath());
				List<Variable> variables = ncfile.getVariables();
				for (Variable variable : variables) {
					if (variable.getShortName().equals("time")) {
						String[] minMax = NetCDJSONFHeader.transformTime(variable);
						writer.write(dataset_struct+" sgdata:minTimeValue \""+minMax[0]+"\"^^xsd:dateTime . \n");
						writer.write(dataset_struct+" sgdata:maxTimeValue \""+minMax[1]+"\"^^xsd:dateTime . \n");
						break;
					}
				}
				ncfile.close();
				i++;
				System.out.println(i);
			}
		}
		writer.close();
	}

	
}
