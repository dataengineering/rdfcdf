package gr.demokritos.iit.netcdftoolkit.search.tests;

import gr.demokritos.iit.agmip.AgmipMetadataSearch;
import gr.demokritos.iit.netcdftoolkit.commons.SearchUtils;
import gr.demokritos.iit.netcdftoolkit.search.IsimipMetadataSearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.wur.alterra.semagrow.file.SemagrowQueryBlock;

public class MetadataSearchTest {

	public static void main(String[] args) throws InterruptedException {
		
		Date started = new Date();
		
		String endpoint = "http://143.233.226.44:8080/SemaGrow/sparql";
		//String[] terms = {"wheat(aaa,dddd,cccc)", "epic(snow)", "CDI"};
		String[] terms = {"temperature"};
		
		String[] src = {"isimip", "agmip"};
		
		float west = -179f;
		float east = 179f;
		float south = -89f;
		float north = 89f;
		SemagrowQueryBlock semagrowQueryBlock = new SemagrowQueryBlock();
		semagrowQueryBlock.setWest(west);//lon
		semagrowQueryBlock.setEast(east);//lon
		semagrowQueryBlock.setSouth(south);//lat
		semagrowQueryBlock.setNorth(north);//lat
		semagrowQueryBlock.setFrom(new Date(-30609792000000l));
		semagrowQueryBlock.setTo(new Date(32503766399999l));
		/*semagrowQueryBlock.setFrom(new Date(1262296800000l));//2010
		semagrowQueryBlock.setTo(new Date(1420063200000l));//2015
*/		//semagrowQueryBlock.setFrom(new Date(0l));	
		//semagrowQueryBlock.setTo(new Date(142006320000000l));//2015
		
		int limit = 0;
		
		boolean and = true;
		
		final List<String> keywords_no_agrovoc = new ArrayList<>(terms.length);
		final Map<String, Set<String>> agrovoc_map = SearchUtils.findAgroVocWithOneHop(terms, keywords_no_agrovoc);	
		
		Thread isimip_thread = null;
		Thread agmip_thread = null;
		
		List<String> isimip_results;
		List<String> agmip_results;
		if (limit > 0) {
			isimip_results =  new ArrayList<String>(limit * 2);
			agmip_results =  new ArrayList<String>(limit * 2);
		} else {
			isimip_results =  new ArrayList<String>();
			agmip_results =  new ArrayList<String>();
		}
		
		for (int i = 0; i < src.length; i++) {
			if (src[i].equals("isimip")) {
				//IsimipMetadataSearch searcher = new IsimipMetadataSearch("http://143.233.226.52:8891/sparql", keywords_no_agrovoc, agrovoc_map, and, limit, semagrowQueryBlock, results);
				IsimipMetadataSearch searcher = new IsimipMetadataSearch(endpoint, keywords_no_agrovoc, agrovoc_map, and, limit, semagrowQueryBlock, isimip_results);
				isimip_thread = new Thread(searcher);
				isimip_thread.start();
			} else if (src[i].equals("agmip")) {
				//AgmipMetadataSearch searcher = new AgmipMetadataSearch("http://143.233.226.52:8080/strabon-endpoint-3.3.2-SNAPSHOT/Query", keywords_no_agrovoc, agrovoc_map, and, limit, semagrowQueryBlock, results);
				AgmipMetadataSearch searcher = new AgmipMetadataSearch(endpoint, keywords_no_agrovoc, agrovoc_map, and, limit, semagrowQueryBlock, agmip_results);
				agmip_thread = new Thread(searcher);
				agmip_thread.start();
			}
		}
		
		if (isimip_thread != null)
			isimip_thread.join();
		
		if (agmip_thread != null)
			agmip_thread.join();
		
		StringBuilder builder = new StringBuilder("[");
		
		for (int i = 0; i < isimip_results.size()-1; i++) {	
			builder.append(isimip_results.get(i));
			builder.append(", ");
		}
		if (isimip_results.size()>0) {
			builder.append(isimip_results.get(isimip_results.size() - 1));
			if (agmip_results.size() > 0 ) 
				builder.append(", ");
			
		}
		
		for (int i = 0; i < agmip_results.size()-1; i++) {	
			builder.append(agmip_results.get(i));
			builder.append(", ");
		}
		if (agmip_results.size()>0) {
			builder.append(agmip_results.get(agmip_results.size() - 1));
		}
		
		builder.append("]");
		
		System.out.println(builder.toString());
		System.out.println(started);
		System.out.println(new Date());
	}

}
