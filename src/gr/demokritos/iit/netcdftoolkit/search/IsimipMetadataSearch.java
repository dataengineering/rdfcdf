/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.search;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import nl.wur.alterra.semagrow.file.SemagrowQueryBlock;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

import gr.demokritos.iit.netcdftoolkit.commons.SearchUtils;

/**
 * @author Giannis Mouchakis
 *
 */
public class IsimipMetadataSearch  implements Runnable{
	
	static Logger logger = LoggerFactory.getLogger(IsimipMetadataSearch.class);
	
	final private String endpoint;
	final private List<String> keywords_no_agrovoc;
	final private Map<String, Set<String>> agrovoc_map;
	final private boolean and;
	final private int limit;
	final private SemagrowQueryBlock semagrowQueryBlock;
	private List<String> results;
	

	/**
	 * @param endpoint
	 * @param keywords_no_agrovoc
	 * @param agrovoc_map
	 * @param and
	 * @param limit
	 * @param semagrowQueryBlock
	 * @param results
	 */
	public IsimipMetadataSearch(final String endpoint, final List<String> keywords_no_agrovoc, 
			final Map<String, Set<String>> agrovoc_map, final boolean and, final int limit, 
			final SemagrowQueryBlock semagrowQueryBlock, List<String> results) {
		this.endpoint = endpoint;
		this.keywords_no_agrovoc = keywords_no_agrovoc;
		this.agrovoc_map = agrovoc_map;
		this.and = and;
		this.limit = limit;
		this.semagrowQueryBlock = semagrowQueryBlock;
		this.results = results;
	}

	@Override
	public void run() {
			
		query();
	}
	
	public void query() {
		
		Float south = semagrowQueryBlock.getSouth();
		Float north = semagrowQueryBlock.getNorth();
		if (checkLat(south, north)) {
			return;
		}
		
		Float west = semagrowQueryBlock.getWest();
		Float east = semagrowQueryBlock.getEast();
		if (checkLon(west, east)) {
			return;
		}
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String from_date = df.format(semagrowQueryBlock.getFrom());
		String to_date = df.format(semagrowQueryBlock.getTo());
		
		
		String query_str = "PREFIX qb: <http://purl.org/linked-data/cube#> "
				+ "PREFIX sgstruct: <http://semagrow.eu/rdf/struct/> "
				+ "PREFIX nc: <http://rdf.iit.demokritos.gr/2014/netcdf#> "
				+ "PREFIX sgdata: <http://semagrow.eu/rdf/data/> "
				+ "PREFIX  xsd: <http://www.w3.org/2001/XMLSchema#> "
				+ "SELECT DISTINCT ?json "
				+ "{ ";
		
		if (and) { //AND Operator
			
			query_str += "?dataset qb:structure ?dataset_struct . "
					//+ "?dataset_struct sgdata:header ?header . "
					+ "?dataset_struct sgdata:jsonHeader ?json . "
					+ "?dataset_struct sgdata:minTimeValue ?min_value . "
					+ "?dataset_struct sgdata:maxTimeValue ?max_value . "
					+ SearchUtils.createANDFilter(agrovoc_map, keywords_no_agrovoc, " ?dataset_struct sgdata:header ?header . ")
					+ "FILTER ((xsd:dateTime(?min_value) >= \""+from_date+"\"^^xsd:dateTime &&"
							+ "xsd:dateTime(?max_value) <= \""+to_date+"\"^^xsd:dateTime) "
							+ "||"
							+ "(xsd:dateTime(?min_value) >= \""+from_date+"\"^^xsd:dateTime &&"
							+ "xsd:dateTime(?min_value) <= \""+to_date+"\"^^xsd:dateTime)"
							+ "||"
							+ "(xsd:dateTime(?max_value) >= \""+from_date+"\"^^xsd:dateTime &&"
							+ "xsd:dateTime(?max_value) <= \""+to_date+"\"^^xsd:dateTime) "
							+ "||"
							+ "(xsd:dateTime(?min_value) <= \""+from_date+"\"^^xsd:dateTime &&"
							+ "xsd:dateTime(?max_value) >= \""+to_date+"\"^^xsd:dateTime)"
							+ ") . ";
			
		} else { //OR Operator
			
			boolean union = false;
			if ( ! agrovoc_map.isEmpty() &&  ! keywords_no_agrovoc.isEmpty()) {
				union = true;
			}
			
			if (union) query_str += " { ";
			
			if ( ! agrovoc_map.isEmpty() ) {
				
				query_str += "?dataset qb:structure ?dataset_struct . "
						//+ "?dataset_struct sgdata:header ?header . "
						+ "?dataset_struct sgdata:jsonHeader ?json . "
						+ "?dataset_struct sgdata:minTimeValue ?min_value . "
						+ "?dataset_struct sgdata:maxTimeValue ?max_value . "
						+ SearchUtils.createAgroVocFilter(agrovoc_map)
						+ "FILTER ((xsd:dateTime(?min_value) >= \""+from_date+"\"^^xsd:dateTime &&"
								+ "xsd:dateTime(?max_value) <= \""+to_date+"\"^^xsd:dateTime) "
								+ "||"
								+ "(xsd:dateTime(?min_value) >= \""+from_date+"\"^^xsd:dateTime &&"
								+ "xsd:dateTime(?min_value) <= \""+to_date+"\"^^xsd:dateTime)"
								+ "||"
								+ "(xsd:dateTime(?max_value) >= \""+from_date+"\"^^xsd:dateTime &&"
								+ "xsd:dateTime(?max_value) <= \""+to_date+"\"^^xsd:dateTime) "
								+ "||"
								+ "(xsd:dateTime(?min_value) <= \""+from_date+"\"^^xsd:dateTime &&"
								+ "xsd:dateTime(?max_value) >= \""+to_date+"\"^^xsd:dateTime)"
								+ ") . ";
				
				
			}
			
			if (union) query_str += " } UNION { ";
			
			
			if ( ! keywords_no_agrovoc.isEmpty() ) {
				
				query_str += "?dataset qb:structure ?dataset_struct . "
						//+ "?dataset_struct sgdata:header ?header . "
						+ "?dataset_struct sgdata:jsonHeader ?json . "
						+ "?dataset_struct sgdata:minTimeValue ?min_value . "
						+ "?dataset_struct sgdata:maxTimeValue ?max_value . "
						+ SearchUtils.createKeywordFilter(keywords_no_agrovoc, " ?dataset_struct sgdata:header ?header . ")
						+ "FILTER ((xsd:dateTime(?min_value) >= \""+from_date+"\"^^xsd:dateTime &&"
								+ "xsd:dateTime(?max_value) <= \""+to_date+"\"^^xsd:dateTime) "
								+ "||"
								+ "(xsd:dateTime(?min_value) >= \""+from_date+"\"^^xsd:dateTime &&"
								+ "xsd:dateTime(?min_value) <= \""+to_date+"\"^^xsd:dateTime)"
								+ "||"
								+ "(xsd:dateTime(?max_value) >= \""+from_date+"\"^^xsd:dateTime &&"
								+ "xsd:dateTime(?max_value) <= \""+to_date+"\"^^xsd:dateTime) "
								+ "||"
								+ "(xsd:dateTime(?min_value) <= \""+from_date+"\"^^xsd:dateTime &&"
								+ "xsd:dateTime(?max_value) >= \""+to_date+"\"^^xsd:dateTime)"
								+ ") . ";
				
				
				
			}
			
			if (union) query_str += " } ";
			
			
		}
	
		query_str += " } ";
		
		if (limit > 0) {
			query_str += "LIMIT "+ limit;
		}
		
		Query query = QueryFactory.create(query_str);
		ARQ.getContext().setTrue(ARQ.useSAX);
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);			
		ResultSet resultSet = qexec.execSelect();				
		while (resultSet.hasNext()) {
			QuerySolution soln = resultSet.next();			
			RDFNode json_n = soln.get("json");
			String json = "";
			if (json_n.isLiteral()) {
				json = StringEscapeUtils.unescapeXml(json_n.asLiteral().getString());
			} else if (json_n.isResource()) {
				json = StringEscapeUtils.unescapeXml(json_n.asResource().toString());
			} else {
				logger.error("Could not handle node"+json_n.toString());
			}
			results.add(json);
		}	
		qexec.close();
		
	}

	private boolean checkLon(float west, float east) {
		boolean no_values;
		if (west < east) {
			if (east < -179.75f || west > 179.75f ) {
				no_values = true;
			} else {
				no_values = false;
			}
		} else if (west > east) {
			if (west > 179.75f && east < -179.75f ) {
				no_values = true;
			} else {
				no_values = false;
			}
		} else {
			if ( (-179.75f <= east && east <= 179.75f)) {
				no_values = false;
			} else {
				no_values = true;
			}
		}
		return no_values;		
	}
		
	
	private boolean checkLat(float south, float north) {
		boolean no_values;
		if (south < north) {
			if (north < -89.75f || south > 89.75f ) {
				no_values = true;
			} else {
				no_values = false;
			}
		} else if (south > north) {
			if (south > 89.75f && north < -89.75f ) {
				no_values = true;
			} else {
				no_values = false;
			}
		} else {
			if ( (-89.75f <= north && north <= 89.75f)) {
				no_values = false;
			} else {
				no_values = true;
			}
		}
		return no_values;		
	}
	
	
}
