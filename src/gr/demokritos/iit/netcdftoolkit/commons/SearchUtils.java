/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.commons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Giannis Mouchakis
 *
 */
public class SearchUtils {
	
	static Logger logger = LoggerFactory.getLogger(SearchUtils.class);
	
	final static String agrovoc_endpoint = "http://202.45.139.84:10035/catalogs/fao/repositories/agrovoc";
	
	
	/**
	 * 
	 * Queries the official AgroVoc repository to find the AgroVoc URI matching to this term in English
	 * 
	 * @param term term to search
	 * @return List of matching URIs
	 */
	public static List<String> findAgroVoc(String term)  {
		return findAgroVoc(term, "en");
	}
	
	/**
	 * 
	 * Queries the official AgroVoc repository to find the AgroVoc URI matching to this term
	 * 
	 * @param term term to search
	 * @param lang the language to search as used in AgroVoc, e.g. 'en'
	 * @return List of matching URIs
	 */
	public static List<String> findAgroVoc(String term, String lang)  {
		
		List<String> agrovoc_uris = new ArrayList<String>(1);
		String query_str = "SELECT ?s { ?s ?p \"" + term + "\"@" + lang + " }";
		
		try {
			Repository repository = new SPARQLRepository(agrovoc_endpoint);
			repository.initialize();
			RepositoryConnection connection = repository.getConnection();
			
			TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);
			TupleQueryResult result = query.evaluate();
			
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				String agrovoc_uri = bindingSet.getValue("s").stringValue();
				agrovoc_uris.add(agrovoc_uri);
			}
			
			result.close();
			connection.close();
			repository.shutDown();
			
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			logger.error("could not retrieve AgroVoc URIs", e);
		}
		
		return agrovoc_uris;
		
	}
	
	
	
	public static Map<String, Set<String>> findAgroVocWithOneHop(String[] terms, List<String> not_found)  {
		return findAgroVocWithOneHop(terms, "en", not_found);
	}
	
	
	public static Map<String, Set<String>> findAgroVocWithOneHop(String terms[], String lang, List<String> not_found)  {
		
		Map<String, Set<String>> map = new HashMap<>();
		
		for (String term : terms) {
			
			String t_term = null;
			if (lang.equals("en")) {//agrovoc contains lower case literals for english
				t_term = term.toLowerCase();
			} else {//check other languages in the future
				t_term = term;
			}
		
			String query_str = "SELECT DISTINCT ?this ?boarder ?narrower { ?this ?p \"" + t_term + "\"@" + lang + " . "
					+ " ?this <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept> . "
					+ " OPTIONAL { ?this <http://www.w3.org/2004/02/skos/core#broader> ?boarder } "
					+ " OPTIONAL { ?this <http://www.w3.org/2004/02/skos/core#narrower> ?narrower  } . "
					+ " } ";
			
			/*String query_str = "SELECT DISTINCT ?this ?t ?rel ?relative ?rellabel ?intermediate ?interlabel { "
					+ " ?this ?p \"" + term.toLowerCase() + "\"@" + lang + " . ?this rdf:type <http://www.w3.org/2004/02/skos/core#Concept> "
					+ " { ?this <http://www.w3.org/2004/02/skos/core#broader> ?relative . "
					+ " ?relative skos:prefLabel ?rellabel. FILTER (lang(?rellabel) = 'en') . BIND (\"parent\" AS ?rel) } "
					+ " UNION "
					+ " { ?this <http://www.w3.org/2004/02/skos/core#broader> ?intermediate . "
					+ " ?relative <http://www.w3.org/2004/02/skos/core#broader> ?intermediate . "
					+ " ?relative skos:prefLabel ?interlabel. FILTER (lang(?interlabel) = 'en') . BIND (\"sibling\" AS ?rel)  } "
					+ " UNION "
					+ " { ?relative <http://www.w3.org/2004/02/skos/core#broader> ?this . "
					+ " ?relative skos:prefLabel ?rellabel. FILTER (lang(?rellabel) = 'en') . BIND (\"child\" AS ?rel)  } "
					+ "}";*/
			
			try {
				Repository repository = new SPARQLRepository(agrovoc_endpoint);
				repository.initialize();
				RepositoryConnection connection = repository.getConnection();
				
				TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);
				TupleQueryResult result = query.evaluate();
				
				if ( ! result.hasNext() ) {
					logger.debug("did not find AgroVoc concept for term " + term);
					not_found.add(term);
				}
				
				while (result.hasNext()) {
					
					BindingSet bindingSet = result.next();
					String agrovoc_uri = bindingSet.getValue("this").stringValue();
					Value boarder = bindingSet.getValue("boarder");
					Value narrower = bindingSet.getValue("narrower");
					
					Set<String> related = map.get(agrovoc_uri);
					
					if (related != null) {
						if (boarder != null && boarder.stringValue().startsWith("http://aims.fao.org/aos/agrovoc/c_")) 
							related.add(boarder.stringValue());
						if (narrower != null && narrower.stringValue().startsWith("http://aims.fao.org/aos/agrovoc/c_")) 
							related.add(narrower.stringValue());
					} else {
						if (agrovoc_uri.startsWith("http://aims.fao.org/aos/agrovoc/c_")) {
							related = new HashSet<String>();
							if (boarder != null && boarder.stringValue().startsWith("http://aims.fao.org/aos/agrovoc/c_")) 
								related.add(boarder.stringValue());
							if (narrower != null && narrower.stringValue().startsWith("http://aims.fao.org/aos/agrovoc/c_")) 
								related.add(narrower.stringValue());
							map.put(agrovoc_uri, related);
						}
					}
					
				}
				
				result.close();
				connection.close();
				repository.shutDown();
				
			} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
				logger.error("could not retrieve AgroVoc URIs", e);
			}
		}
		
		return map;
		
	}
	
	
	public static String createANDFilter(Map<String, Set<String>> agrovoc_map, List<String> text_search, String search_pattern) {
		
		String query_substr = "";
		
			
		int i = 0;
		for (Entry<String, Set<String>> entry : agrovoc_map.entrySet()) {
				
			i++;
				
			query_substr += " ?dataset  <http://purl.org/dc/terms/subject> ?agrovoc" + i + " . ";
			query_substr += "VALUES (?agrovoc"+ i + ") { ";
						
			String agrovoc = entry.getKey();
			query_substr += " ( <" + agrovoc + "> ) ";
			Set<String> relateds = entry.getValue();
			if (relateds != null) {
				for (String related : relateds) {
					query_substr += " ( <" + related + "> ) ";
				}
			}
				
			query_substr += " } . ";
				
		}
			
		if ( ! text_search.isEmpty()) {			
			query_substr += search_pattern;
			for (String keyword : text_search) {
				query_substr += "FILTER (regex(str(?header), \"" + keyword + "\", \"i\" ) ) . ";
			}	
		}
			
		return query_substr;
	}
	
	
	public static String createAgroVocFilter(Map<String, Set<String>> agrovoc_map) {
		
		String query_substr = "";	
		
		if ( ! agrovoc_map.isEmpty() ) {
			
			query_substr += " ?dataset  <http://purl.org/dc/terms/subject> ?agrovoc . ";
			query_substr += "VALUES (?agrovoc) { ";
			for (Entry<String, Set<String>> entry : agrovoc_map.entrySet()) {
				String agrovoc = entry.getKey();
				query_substr += " ( <" + agrovoc + "> ) ";
				Set<String> relateds = entry.getValue();
				if (relateds != null) {
					for (String related : relateds) {
						query_substr += " ( <" + related + "> ) ";
					}
				}
			}
			query_substr += " } . ";
			
		}
		
		
		return query_substr;
	}
	
	public static String createKeywordFilter(List<String> keywords, String search_pattern) {
		
		String query_substr = "";
		
		if ( ! keywords.isEmpty() ) {
			
			query_substr += search_pattern;
			query_substr += "FILTER (";	
			for (int i = 0; i < keywords.size() - 1; i++) {	
				query_substr += "regex(str(?header), \"" + keywords.get(i) + "\", \"i\" ) || ";		
			}
			query_substr += "regex(str(?header), \"" + keywords.get(keywords.size() - 1) + "\", \"i\" ) ) . ";	
			
		}
		
		return query_substr;
	}
		
	

}
