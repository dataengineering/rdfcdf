/***************
<p>Title: NetCDF Utilities</p>

<p>Description:
NetCDF Utilities
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Giannis Mouchakis (SemaGrow 2014)

***************/


package gr.demokritos.iit.netcdftoolkit.commons;

import java.nio.ByteBuffer;
import java.util.Arrays;

import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.XMLSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.rdf.model.RDFNode;

import ucar.ma2.DataType;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;


public class NetCDFUtils
{
	
	static Logger logger = LoggerFactory.getLogger( NetCDFUtils.class );
	
	/**
     * Converter from NetCDF datatypes to XSD datatypes.
     * 
     * @param netcdfDatatype
     * @return String representation of XSD equivalent datatype.
     */
    static public String getXsdType( DataType netcdfDatatype )
    {
    	return "^^xsd:" + getDataTypeToString(netcdfDatatype);
    }
    
    /**
     * Converter from NetCDF datatypes to string literals datatypes.
     * 
     * @param netcdfDatatype
     * @return String representation of equivalent NetCDF datatype
     * or null if no equivalent datatype exists.
     */
    static public String getDataTypeToString( DataType netcdfDatatype )
    {
    	String type = null;
		if (netcdfDatatype == DataType.FLOAT) {
			type = "float";
		} else if (netcdfDatatype == DataType.DOUBLE) {
			type = "double";
		} else if ((netcdfDatatype == DataType.SHORT) || (netcdfDatatype == DataType.ENUM2)) {
			type = "short";
		} else if ((netcdfDatatype == DataType.INT) || (netcdfDatatype == DataType.ENUM4)){
			type = "int";
		} else if ((netcdfDatatype == DataType.BYTE) || (netcdfDatatype == DataType.ENUM1)) {
			type = "byte";
		} else if (netcdfDatatype == DataType.CHAR) {
			type = "char";
		} else if (netcdfDatatype == DataType.BOOLEAN) {
			logger.warn("value is a boolean, skip value");
		} else if (netcdfDatatype == DataType.LONG) {
			type = "long";
		} else if (netcdfDatatype == DataType.STRING) {
			type = "string";
		} else if (netcdfDatatype == DataType.STRUCTURE) {
			logger.warn("value is a STRUCTURE, skip value");
		} else if (netcdfDatatype == DataType.SEQUENCE) {
			logger.warn("value is a SEQUENCE, skip value");
		} else if (netcdfDatatype == DataType.OPAQUE) {
			logger.warn("value is a OPAQUE, skip value");
		} else {
			logger.warn("value is of unknown type, skip value");
		}
		return type;
    }
    
    /**
     * Converter from NetCDF datatypes to org.openrdf.model.URI datatypes.
     * 
     * @param netcdfDatatype
     * @return org.openrdf.model.URI datatype
     * or null no equivalent datatype exists.
     */
    static public org.openrdf.model.URI getDataTypeToXMLSchema( DataType netcdfDatatype )
    {
    	org.openrdf.model.URI type = null;
		if (netcdfDatatype == DataType.FLOAT) {
			type = XMLSchema.FLOAT;
		} else if (netcdfDatatype == DataType.DOUBLE) {
			type = XMLSchema.DOUBLE;
		} else if ((netcdfDatatype == DataType.SHORT) || (netcdfDatatype == DataType.ENUM2)) {
			type = XMLSchema.SHORT;
		} else if ((netcdfDatatype == DataType.INT) || (netcdfDatatype == DataType.ENUM4)){
			type = XMLSchema.INT;
		} else if ((netcdfDatatype == DataType.BYTE) || (netcdfDatatype == DataType.ENUM1)) {
			type = XMLSchema.BYTE;
		} else if (netcdfDatatype == DataType.CHAR) {
			type = XMLSchema.STRING;
			logger.warn("value is a char, we convert it to string");
		} else if (netcdfDatatype == DataType.BOOLEAN) {
			logger.warn("value is a boolean, skip value");
		} else if (netcdfDatatype == DataType.LONG) {
			type = XMLSchema.LONG;
		} else if (netcdfDatatype == DataType.STRING) {
			type = XMLSchema.STRING;
		} else if (netcdfDatatype == DataType.STRUCTURE) {
			logger.warn("value is a STRUCTURE, skip value");
		} else if (netcdfDatatype == DataType.SEQUENCE) {
			logger.warn("value is a SEQUENCE, skip value");
		} else if (netcdfDatatype == DataType.OPAQUE) {
			logger.warn("value is a OPAQUE, skip value");
		} else {
			logger.warn("value is of unknown type, skip value");
		}
		return type;
    }
    
    
    public static Object getValue(String uri_value) {
    	
    	if (uri_value.startsWith(NetCDFVocab.prefixMapping.getNsPrefixURI("float"))) {
    		return new Float(uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("float").length()));
    	} else if (uri_value.startsWith(NetCDFVocab.prefixMapping.getNsPrefixURI("double"))) {
    		return new Double(uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("double").length()));
    	} else if (uri_value.startsWith(NetCDFVocab.prefixMapping.getNsPrefixURI("short"))) {
    		return new Short(uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("short").length()));
    	} else if (uri_value.startsWith(NetCDFVocab.prefixMapping.getNsPrefixURI("int"))) {
    		return new Integer(uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("int").length()));
    	} else if (uri_value.startsWith(NetCDFVocab.prefixMapping.getNsPrefixURI("byte"))) {
    		return new Byte(uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("byte").length()));
    	} else if (uri_value.startsWith(NetCDFVocab.prefixMapping.getNsPrefixURI("long"))) {
    		return new Long(uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("long").length()));
    	} else if (uri_value.startsWith(NetCDFVocab.prefixMapping.getNsPrefixURI("string"))) {
    		return new String(uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("string").length()));
    	} else if (uri_value.startsWith(NetCDFVocab.prefixMapping.getNsPrefixURI("char"))) {
    		if ((uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("char").length())).toCharArray().length>1) return null;
    		return new Character((uri_value.substring(NetCDFVocab.prefixMapping.getNsPrefixURI("char").length())).toCharArray()[0]);
    	} else {
    		return null;
    	}
    	
    }
    

    public static String getXsdType(Class<?> c) {
      if ((c == float.class) || (c == Float.class)) return "^^xsd:float";
      if ((c == double.class) || (c == Double.class)) return "^^xsd:double";
      if ((c == short.class) || (c == Short.class)) return "^^xsd:short";
      if ((c == int.class) || (c == Integer.class)) return "^^xsd:int";
      if ((c == byte.class) || (c == Byte.class)) return "^^xsd:byte";
      if ((c == char.class) || (c == Character.class)) return "^^xsd:string";
      if ((c == long.class) || (c == Long.class)) return "^^xsd:long";
      if (c == String.class) return "^^xsd:string";
      //else
      logger.error("unknown datatype");
      return null;
    }
    
    public static String[] compareCodeListValues(String val1, String val2) {
    	String[] comparison = new String[2];
    	Object value1 = NetCDFUtils.getValue(val1);
    	Object value2 = NetCDFUtils.getValue(val2);
    	if ( ! value1.getClass().equals(value2.getClass()) ) {
    		logger.error("objects must be of the same class");
    	} else if (value1 instanceof Long) {
    		long a = (Long) value1;
    		long b = (Long) value2;
    		if (a<b) {
    			comparison[0] = val1;
    			comparison[1] = val2;
    		} else {
    			comparison[0] = val2;
    			comparison[1] = val1;
    		}
    	} else if (value1 instanceof Double) {
    		double a = (Double) value1;
    		double b = (Double) value2;
    		if (a<b) {
    			comparison[0] = val1;
    			comparison[1] = val2;
    		} else {
    			comparison[0] = val2;
    			comparison[1] = val1;
    		}
    	} else if (value1 instanceof Integer) {
    		int a = (Integer) value1;
    		int b = (Integer) value2;
    		if (a<b) {
    			comparison[0] = val1;
    			comparison[1] = val2;
    		} else {
    			comparison[0] = val2;
    			comparison[1] = val1;
    		}
    	} else if (value1 instanceof Float) {
    		float a = (Float) value1;
    		float b = (Float) value2;
    		if (a<b) {
    			comparison[0] = val1;
    			comparison[1] = val2;
    		} else {
    			comparison[0] = val2;
    			comparison[1] = val1;
    		}
    	} else if (value1 instanceof Short) {
    		short a = (Short) value1;
    		short b = (Short) value2;
    		if (a<b) {
    			comparison[0] = val1;
    			comparison[1] = val2;
    		} else {
    			comparison[0] = val2;
    			comparison[1] = val1;
    		}
    	} else if (value1 instanceof Byte) {
    		byte a = (Byte) value1;
    		byte b = (Byte) value2;
    		if (a<b) {
    			comparison[0] = val1;
    			comparison[1] = val2;
    		} else {
    			comparison[0] = val2;
    			comparison[1] = val1;
    		}
    	} else if (value1 instanceof String) {
    		comparison[0] = val1;
			comparison[1] = val2;
			Arrays.sort(comparison);
    	} else {
    		logger.error("unknown type to compare");
    	}
		return comparison;
    }

    
    /**
     * Find the DataType that matches this uri.
     *
     * uri representing data type
     * @return DataType or null if no match.
     */
    public static DataType getDataTypeFromURI(String uri) {
    	if (uri.equals(NetCDFVocab.prefixMapping.expandPrefix("float:"))) {
    		return DataType.FLOAT;
    	} else if (uri.equals(NetCDFVocab.prefixMapping.expandPrefix("double:"))) {
    		return DataType.DOUBLE;
    	} else if (uri.equals(NetCDFVocab.prefixMapping.expandPrefix("short:"))) {
    		return DataType.SHORT;
    	} else if (uri.equals(NetCDFVocab.prefixMapping.expandPrefix("int:"))) {
    		return DataType.INT;
    	} else if (uri.equals(NetCDFVocab.prefixMapping.expandPrefix("byte:"))) {
    		return DataType.BYTE;
    	} else if (uri.equals(NetCDFVocab.prefixMapping.expandPrefix("long:"))) {
    		return DataType.LONG;
    	} else if (uri.equals(NetCDFVocab.prefixMapping.expandPrefix("string:"))) {
    		return DataType.STRING;
    	}  else if (uri.equals(NetCDFVocab.prefixMapping.expandPrefix("char:"))) {
    		return DataType.CHAR;
    	}else {
    		logger.error("unknown data type");
    		return null;
    	}
    }
    
	/**
	 * at the moment some values are stored as strings so
	 * node.asLiteral().getValue() always returns String Object. some class
	 * types like boolean or Structure Data are not used in the transformation
	 * and a warning message is produced
	 **/
	public static Object getCorrectType(Class<?> c, RDFNode node) {
		Object obj = null;
		if ((c == float.class) || (c == Float.class)) {
			obj = new Float(node.asLiteral().getValue().toString());
		} else if ((c == double.class) || (c == Double.class)) {
	    	obj = new Double(node.asLiteral().getValue().toString());
	    } else if ((c == short.class) || (c == Short.class)) {
	    	obj = new Short(node.asLiteral().getValue().toString());
	    } else if ((c == int.class) || (c == Integer.class))  {
	    	obj = new Integer(node.asLiteral().getValue().toString());
	    } else if ((c == byte.class) || (c == Byte.class)) {
	    	obj = new Byte(node.asLiteral().getValue().toString());
	    } else if ((c == char.class) || (c == Character.class)) {
	    	obj = new Character(node.asLiteral().getValue().toString().charAt(0));
	    } else if ((c == boolean.class) || (c == Boolean.class)) {
	    	logger.warn("found variable boolean. do nothing");
	    } else if ((c == long.class) || (c == Long.class)) {
	    	obj = new Long(node.asLiteral().getValue().toString());
	    } else if (c == String.class) {
	    	obj = node.asLiteral().getValue().toString();
	    } else if (c == StructureData.class) {
	    	logger.warn("found variable StructureData. do nothing");
	    } else if (c == StructureDataIterator.class) {
	    	logger.warn("found variable StructureDataIterator. do nothing");
	    } else if (c == ByteBuffer.class) {
	    	logger.warn("found variable ByteBuffer. do nothing");
	    }
		
		if (obj==null) {
			throw new IllegalArgumentException(
					"Triplestore returned null value or value could not be converted to java type.");
		}
		
		return obj;
	}
    
	/**
	 * 
	 * @param literal
	 * @return returns the Java Object for this literal determined by the literal's datatype. Float, double, int, 
	 * short, string, byte, char supported. Null if not supported.
	 */
	public static Object getObjectFromLiteral(Literal literal) {
		URI datatype = literal.getDatatype();
		if (datatype == null) {
			logger.error("not defined datatype for literal "+ literal.toString());
			return null;
		}
		if (datatype.equals(XMLSchema.FLOAT)) {
			return literal.floatValue();
		} 
		if (datatype.equals(XMLSchema.DOUBLE)) {
			return literal.doubleValue();
		}
		if (datatype.equals(XMLSchema.INTEGER)) {
			return literal.integerValue();
		}
		if (datatype.equals(XMLSchema.SHORT)) {
			return literal.shortValue();
		}
		if (datatype.equals(XMLSchema.STRING)) {
			return literal.stringValue();
		}
		if (datatype.equals(XMLSchema.BYTE)) {
			return literal.byteValue();
		}
		if (datatype.equals(XMLSchema.INT)) {
			return literal.intValue();
		}
		if (datatype.equals(new URIImpl("http://www.w3.org/2001/XMLSchema#char"))) {		 
			return literal.stringValue().charAt(0);
		}
		logger.error("unknown datatype "+datatype.stringValue());
		return null;
	}
	
}
