/***************
<p>Title: NetCDF Vocabularies</p>

<p>Description:
NetCDF Vocabularies
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Giannis Mouchakis (SemaGrow 2014)
@author Stasinos Konstantopoulos (SemaGrow 2014)

***************/


package gr.demokritos.iit.netcdftoolkit.commons;

import java.util.HashMap;
import java.util.Map;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.shared.PrefixMapping;


public class NetCDFVocab
{
	// These prefixes are used to dynamically create URIs from NetCDF datasets
	// All other prefixes are used for static schema URIs
	static private final String NS_STRUCT_INTERN = "sgstruct";
	static private final String NS_DATA_INTERNAL = "sgdata";
	static private final String CF_INTERNAL = "cf";

	static public final String NS_STRUCT = NS_STRUCT_INTERN + ":";
	static public final String NS_DATA   = NS_DATA_INTERNAL + ":";
	static public final String CF   = CF_INTERNAL + ":";
	
	static public final String datasets_g_attr = "_from_datasets_";
	
	static public final PrefixMapping prefixMapping;
	static {
		prefixMapping = PrefixMapping.Factory.create();
		prefixMapping.setNsPrefix( "xsd", "http://www.w3.org/2001/XMLSchema#" );
		prefixMapping.setNsPrefix( "qb", "http://purl.org/linked-data/cube#" );
		prefixMapping.setNsPrefix( "dct", "http://purl.org/dc/terms/" );
		prefixMapping.setNsPrefix( "nc", "http://rdf.iit.demokritos.gr/2014/netcdf#" );
		prefixMapping.setNsPrefix( CF_INTERNAL, "http://rdf.iit.demokritos.gr/2014/cfconventions#" );
		prefixMapping.setNsPrefix( "float", "http://rdf.iit.demokritos.gr/2014/float#" );
		prefixMapping.setNsPrefix( "int", "http://rdf.iit.demokritos.gr/2014/int#" );
		prefixMapping.setNsPrefix( "long", "http://rdf.iit.demokritos.gr/2014/long#" );
		prefixMapping.setNsPrefix( "short", "http://rdf.iit.demokritos.gr/2014/short#" );
		prefixMapping.setNsPrefix( "double", "http://rdf.iit.demokritos.gr/2014/double#" );
		prefixMapping.setNsPrefix( "byte", "http://rdf.iit.demokritos.gr/2014/byte#" );
		prefixMapping.setNsPrefix( "string", "http://rdf.iit.demokritos.gr/2014/string#" );
		prefixMapping.setNsPrefix( "char", "http://rdf.iit.demokritos.gr/2014/char#" );
		prefixMapping.setNsPrefix( NS_STRUCT_INTERN, "http://semagrow.eu/rdf/struct/" );
		prefixMapping.setNsPrefix( NS_DATA_INTERNAL, "http://semagrow.eu/rdf/data/" );
		prefixMapping.setNsPrefix( "rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
		prefixMapping.lock();
	}		

	static public final String strDataset = "qb:DataSet";
	static public final String strStructure = "qb:DataStructureDefinition";
	static public final String strDatasetStructure = "qb:structure";
	static public final String strVariable = "qb:ComponentSpecification";
	static public final String strStructureVariable = "qb:component";
	static public final String strVariableOrder = "qb:order";
	static public final String strVariableProperty = "qb:componentProperty";
	static public final String strVariableRequired = "qb:componentRequired";
	static public final String strVariableAttachment = "qb:componentAttachment";
	// Attachment = {qb:DataSet, qb:Slice, qb:Observation, or a qb:MeasureProperty}
	static public final String strVariableCodelist = "qb:codeList";
	static public final String strCodelistRoot = "qb:hierarchyRoot";
	static public final String strDimension = "qb:DimensionProperty";
	static public final String strMeasure = "qb:MeasureProperty";
	static public final String strAttribute = "qb:AttributeProperty";
	static public final String strObservation = "qb:Observation";
	static public final String strObservationDataset = "qb:dataSet";
	static public final String strSubset = "qb:Slice";
	static public final String strDatasetSubset = "qb:slice";
	static public final String strSubsetObservation = "qb:observation";
	static public final String strSubstructure = "qb:SliceKey";
	static public final String strSubstructureProperty = "qb:componentProperty";
	static public final String strSliceSubstructure = "qb:sliceStructure";
	
	static public final String strDimensionSize = NS_STRUCT + "dimensionSize";
	static public final String strShortName = NS_STRUCT + "shortName";
	static public final String strDataType = NS_STRUCT + "dataType";
	static public final String strIndex= NS_STRUCT + "index";
	
	static public final String strRdfType = "rdf:type";
	static public final String strRdfValue = "rdf:value";
	
	static public final String strFile = "nc:File";
	static public final String strFromFile = "nc:fromFile";
	
	static public final String strAttrValue = "sgstruct:attrValue";

	static public final Resource Dataset;
	static public final Resource Structure;
	static public final Resource DatasetStructure;
	static public final Resource Variable;
	static public final Resource StructureVariable;
	static public final Resource VariableOrder;
	static public final Resource VariableProperty;
	static public final Resource VariableRequired;
	static public final Resource VariableAttachment;
	static public final Resource VariableCodelist;
	static public final Resource CodelistRoot;
	static public final Resource Dimension;
	static public final Resource Measure;
	static public final Resource Attribute;
	static public final Resource Observation;
	static public final Resource ObservationDataset;
	static public final Resource Subset;
	static public final Resource DatasetSubset;
	static public final Resource SubsetObservation;
	static public final Resource Substructure;
	static public final Resource SubstructureProperty;
	static public final Resource SliceSubstructure;
	static public final Resource DimensionSize;
	static public final Resource ShortName;
	static public final Resource DataType;
	static public final Resource Index;
	static public final Resource RDFType;
	static public final Resource RDFValue;
	static public final Resource File;
	static public final Resource FromFile;
	static public final Resource AttrValue;
	static {
		Dataset = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strDataset) );
		Structure = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strStructure) );
		DatasetStructure = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strDatasetStructure) );
		Variable = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strVariable) );
		StructureVariable = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strStructureVariable) );
		VariableOrder = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strVariableOrder) );
		VariableProperty = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strVariableProperty) );
		VariableRequired = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strVariableRequired) );
		VariableAttachment = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strVariableAttachment) );
		VariableCodelist = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strVariableCodelist) );
		CodelistRoot = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strCodelistRoot) );
		Dimension = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strDimension) );
		Measure = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strMeasure) );
		Attribute = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strAttribute) );
		Observation = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strObservation) );
		ObservationDataset = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strObservationDataset) );
		Subset = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strSubset) );
		DatasetSubset = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strDatasetSubset) );
		SubsetObservation = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strSubsetObservation) );
		Substructure = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strSubstructure) );
		SubstructureProperty = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strSubstructureProperty) );
		SliceSubstructure = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strSliceSubstructure) );
		DimensionSize = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strDimensionSize) );
		ShortName = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strShortName) );
		DataType = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strDataType) );
		Index = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strIndex) );
		RDFType = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strRdfType) );
		RDFValue = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strRdfValue) );
		File = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strFile) );
		FromFile = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strFromFile) );
		AttrValue = ResourceFactory.createResource(
				prefixMapping.expandPrefix(strAttrValue) );
	}

	
	/*
	 * Well-known NetCDF properties
	 */

	static public final Map<String,Resource> KnownNetCDFProperties = new HashMap<String,Resource>();
	static {
		Resource r = null;
		
		// Global metadata
		r = ResourceFactory.createResource( prefixMapping.expandPrefix("dct:creator") );
		KnownNetCDFProperties.put( "author", r );
		r = ResourceFactory.createResource( prefixMapping.expandPrefix("dct:title") );
		KnownNetCDFProperties.put( "title", r );
		
		// Standard dimension names
		r = ResourceFactory.createResource( prefixMapping.expandPrefix("cf:latitude") );
		KnownNetCDFProperties.put( "latitude", r );
		r = ResourceFactory.createResource( prefixMapping.expandPrefix("cf:longitude") );
		KnownNetCDFProperties.put( "longitude", r );
		r = ResourceFactory.createResource( prefixMapping.expandPrefix("cf:time") );
		KnownNetCDFProperties.put( "time", r );
		
	}
    
    
}
