package gr.demokritos.iit.netcdftoolkit;

import gr.demokritos.iit.netcdftoolkit.creator.MeasurementVariable;
import gr.demokritos.iit.netcdftoolkit.creator.NetCDFCreator;
import gr.demokritos.iit.netcdftoolkit.creator.DimensionSpecification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.wur.alterra.semagrow.file.SemagrowQueryBlock;

import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.Variable;

public class CreatorExample {

	static Logger logger = LoggerFactory.getLogger(CreatorExample.class);
	
	public static void main(String[] args) throws IOException, InvalidRangeException {
		//your endpoint here
		String endpoint = null;
		//where to store the file
		String output_file = System.getProperty("user.home") + "/created.nc";
		//parameters
		SemagrowQueryBlock semagrowQueryBlock = new SemagrowQueryBlock();
		semagrowQueryBlock.setWest(0.75f);//from lon
		semagrowQueryBlock.setEast(4.25f);//to lon
		semagrowQueryBlock.setSouth(50.75f);//from lat
		semagrowQueryBlock.setNorth(54.25f);//to lat
		semagrowQueryBlock.setFrom(new Date(1925078400000l));//from date
		semagrowQueryBlock.setTo(new Date(1925337600000l));//to date
		//init creator
		NetCDFCreator rdf_to_netcdf = new NetCDFCreator( endpoint, true );
		//Datasets to be queried. 
		//template http://semagrow.eu/rdf/data/{dataset name}.
		List<String> datasets = new ArrayList<String>();
		datasets.add("http://semagrow.eu/rdf/data/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t");
		//measurement variables to insert to the created NetCDF file
		//template http://semagrow.eu/rdf/data/{dataset name}_{variable short name}
		String meas_var_dataset = "http://semagrow.eu/rdf/data/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t_uasAdjust";	
		//find global variables for the dataset
		Set<Attribute> global_attributes = rdf_to_netcdf.findGlobalAttributes(datasets);
		//find dimension specifications for the dataset
		Map<String, List<DimensionSpecification>> meas_var_dim_metadata = new HashMap<String, List<DimensionSpecification>>();
		List<DimensionSpecification> dim_metadata =	rdf_to_netcdf.findDimensions(meas_var_dataset);
		//create geo box if needed
		rdf_to_netcdf.createBox(dim_metadata, semagrowQueryBlock);
		//store metadata
		meas_var_dim_metadata.put(meas_var_dataset, dim_metadata);
		//create writer object
		NetcdfFileWriter writer = NetcdfFileWriter.createNew(NetcdfFileWriter.Version.netcdf3, output_file);
		//store dimension values in the map
		Map<Variable, Object[]> dim_values_map = new HashMap<Variable, Object[]>();
		//store measurement variables
		List<MeasurementVariable> measurement_variables = new ArrayList<MeasurementVariable>();
		//create NetCDF structure
		rdf_to_netcdf.createNetCDFStructure(writer, global_attributes, meas_var_dim_metadata, dim_values_map, measurement_variables);
		//write
		//write dimension values
		rdf_to_netcdf.writeDimensionValues(writer, dim_values_map);
		//write measurement values
		try {
			rdf_to_netcdf.writeMeasurementValues(writer, measurement_variables);
		} catch (RepositoryException | MalformedQueryException
				| QueryEvaluationException e) {
			e.printStackTrace();
		}
		writer.close();
	}

}
