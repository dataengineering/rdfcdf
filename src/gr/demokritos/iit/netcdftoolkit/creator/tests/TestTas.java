package gr.demokritos.iit.netcdftoolkit.creator.tests;

import gr.demokritos.iit.netcdftoolkit.creator.MeasurementVariable;
import gr.demokritos.iit.netcdftoolkit.creator.NetCDFCreator;
import gr.demokritos.iit.netcdftoolkit.creator.DimensionSpecification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.wur.alterra.semagrow.file.SemagrowQueryBlock;

import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.Variable;

public class TestTas {

	static Logger logger = LoggerFactory.getLogger(TestTas.class);
	static String endpoint;
	static String output_file;
	static float west;
	static float east; 
	static float south; 
	static float north;
	
	public static void main(String[] args) throws IOException, InvalidRangeException {
		
		if (args.length > 0) {
			endpoint = args[0];
			output_file = args[1];
			west = new Float(args[2]);
			east = new Float(args[3]);
			south = new Float(args[4]);
			north = new Float(args[5]);
		} else {
			endpoint = "http://143.233.226.44:8080/SemaGrow/sparql";
			//endpoint = "http://83.212.119.138:8090/sparql/";
			//endpoint = "http://10.0.100.24:8890/sparql";
			output_file = System.getProperty("user.home") + "/test_tasmax.nc";
			west = -76.5f;
			east = -76.5f;
			south = 3.08f;
			north = 3.08f;
		}

		runTest(west, east, south, north);
		
	}
	
	public static void runTest(Float west, Float east, Float south, Float north) throws IOException, InvalidRangeException {
		
		Date start = new Date();
		logger.info("started at {}", start.toString());
		
		SemagrowQueryBlock semagrowQueryBlock = new SemagrowQueryBlock();
		semagrowQueryBlock.setWest(west);//lon
		semagrowQueryBlock.setEast(east);//lon
		semagrowQueryBlock.setSouth(south);//lat
		semagrowQueryBlock.setNorth(north);//lat
		semagrowQueryBlock.setFrom(new Date(-2208988800000l));
		semagrowQueryBlock.setTo(new Date(-599702400000l));
		
		logger.info("doing west={}, east={}, south={}, north={}",
				west.toString(), east.toString(), south.toString(),
				north.toString());

		NetCDFCreator rdf_to_netcdf = new NetCDFCreator( endpoint, true );
		
		List<String> datasets = new ArrayList<String>();
		datasets.add("http://semagrow.eu/rdf/data/tasmax_bced_1960_1999_hadgem2-es_rcp2p6_2011-2020");
		
		String meas_var_dataset = "http://semagrow.eu/rdf/data/tasmax_bced_1960_1999_hadgem2-es_rcp2p6_2011-2020_tasmaxAdjust";	
		
		Set<Attribute> global_attributes = rdf_to_netcdf.findGlobalAttributes(datasets);
				
		Map<String, List<DimensionSpecification>> meas_var_dim_metadata = 
				new HashMap<String, List<DimensionSpecification>>();
		List<DimensionSpecification> dim_metadata =
				rdf_to_netcdf.findDimensions(meas_var_dataset);
		
		//create geo box if needed
		rdf_to_netcdf.createBox(dim_metadata, semagrowQueryBlock);
		//store metadata
		meas_var_dim_metadata.put(meas_var_dataset, dim_metadata);
		//create writer object
		NetcdfFileWriter writer = NetcdfFileWriter.createNew(NetcdfFileWriter.Version.netcdf3, output_file);
		//store dimension values in the map
		Map<Variable, Object[]> dim_values_map = new HashMap<Variable, Object[]>();
		//store measurement variables
		List<MeasurementVariable> measurement_variables = new ArrayList<MeasurementVariable>();
		//create netcdf structure
		rdf_to_netcdf.createNetCDFStructure(writer, global_attributes, meas_var_dim_metadata, dim_values_map, measurement_variables);
		//write
		//write dimension values
		rdf_to_netcdf.writeDimensionValues(writer, dim_values_map);
		//write measurement values
		try {
			rdf_to_netcdf.writeMeasurementValues(writer, measurement_variables);
		} catch (RepositoryException | MalformedQueryException
				| QueryEvaluationException e) {
			e.printStackTrace();
		}
		writer.close();
		
		Date end = new Date();
		logger.info("finished at {}", end.toString());
		
	}

}
