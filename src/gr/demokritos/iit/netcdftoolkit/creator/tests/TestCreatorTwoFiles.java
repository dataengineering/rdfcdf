package gr.demokritos.iit.netcdftoolkit.creator.tests;

import gr.demokritos.iit.netcdftoolkit.creator.MeasurementVariable;
import gr.demokritos.iit.netcdftoolkit.creator.NetCDFCreator;
import gr.demokritos.iit.netcdftoolkit.creator.DimensionSpecification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.wur.alterra.semagrow.file.SemagrowQueryBlock;

import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.Variable;

public class TestCreatorTwoFiles {

	public static void main(String[] args) throws RepositoryException, RDFParseException, IOException, InvalidRangeException {
		
		SemagrowQueryBlock semagrowQueryBlock = new SemagrowQueryBlock();
		semagrowQueryBlock.setWest(1.25f);//lon
		semagrowQueryBlock.setEast(-15.25f);//lon
		semagrowQueryBlock.setSouth(40.25f);//lat
		semagrowQueryBlock.setNorth(65.25f);//lat
		/*semagrowQueryBlock.setEast(-15.75f);//lon
		semagrowQueryBlock.setWest(-5.25f);//lon
		semagrowQueryBlock.setSouth(40.25f);//lat
		semagrowQueryBlock.setNorth(64.25f);//lat*/
		
		String endpoint = "http://83.212.119.138:8081/openrdf-workbench/repositories/test_two_files/query";

		NetCDFCreator rdf_to_netcdf = new NetCDFCreator( endpoint, true );
				
		List<String> datasets = new ArrayList<String>();
		datasets.add("http://semagrow.eu/rdf/data/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_aet_whe_annual_2005_2099_t");
		datasets.add("http://semagrow.eu/rdf/data/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t");
		
		String meas_var_dataset_aet = "http://semagrow.eu/rdf/data/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_aet_whe_annual_2005_2099_t_aet";
		String meas_var_dataset_biom = "http://semagrow.eu/rdf/data/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t_biom";
		
		Set<Attribute> global_attributes = rdf_to_netcdf.findGlobalAttributes(datasets);
				
		Map<String, List<DimensionSpecification>> meas_var_dim_metadata = 
				new HashMap<String, List<DimensionSpecification>>();
		List<DimensionSpecification> dim_metadata_aet = rdf_to_netcdf.findDimensions(meas_var_dataset_aet);
		//create geo box if needed
		rdf_to_netcdf.createBox(dim_metadata_aet, semagrowQueryBlock);
		meas_var_dim_metadata.put(meas_var_dataset_aet, dim_metadata_aet);
		List<DimensionSpecification> dim_metadata_biom = rdf_to_netcdf.findDimensions(meas_var_dataset_biom);
		rdf_to_netcdf.createBox(dim_metadata_biom, semagrowQueryBlock);
		meas_var_dim_metadata.put(meas_var_dataset_biom, dim_metadata_biom);

		//create writer object
		NetcdfFileWriter writer = NetcdfFileWriter.createNew(
				NetcdfFileWriter.Version.netcdf3,
				System.getProperty("user.home") + "/test_two_files.nc");
		//store dimension values in the map
		Map<Variable, Object[]> dim_values_map = new HashMap<Variable, Object[]>();
		//store measurement variables
		List<MeasurementVariable> measurement_variables = new ArrayList<MeasurementVariable>();
		//create netcdf structure
		rdf_to_netcdf.createNetCDFStructure(writer, global_attributes, meas_var_dim_metadata, dim_values_map, measurement_variables);
		//write
		//write dimension values
		rdf_to_netcdf.writeDimensionValues(writer, dim_values_map);
		//write measurement values
		try {
			rdf_to_netcdf.writeMeasurementValues(writer, measurement_variables);
		} catch (MalformedQueryException | QueryEvaluationException e) {
			e.printStackTrace();
		}
		writer.close();		
		
	}

}
