package gr.demokritos.iit.netcdftoolkit.creator.tests;

import gr.demokritos.iit.netcdftoolkit.creator.MeasurementVariable;
import gr.demokritos.iit.netcdftoolkit.creator.NetCDFCreator;
import gr.demokritos.iit.netcdftoolkit.creator.DimensionSpecification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.wur.alterra.semagrow.file.SemagrowQueryBlock;

import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.Variable;

public class TestCreator {

	public static void main(String[] args) throws RepositoryException, RDFParseException, IOException, InvalidRangeException {
		
		/*String netcdf_basename = System.getProperty( "user.home" ) + File.separator + "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t";

		NetCDFToRDF netcdf_to_rdf = new NetCDFToRDF( netcdf_basename + ".ttl" , Dataset.ISIMIP);
		netcdf_to_rdf.init();
		netcdf_to_rdf.add( System.getProperty( "user.home" ) + File.separator + "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.nc", "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t" );
		netcdf_to_rdf.close();
		
		File file = new File(
				System.getProperty( "user.home" ) + File.separator + "uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t.ttl");
		String baseURI = "";
		Repository repo = new HTTPRepository("http://localhost:8080/openrdf-sesame", "test_creator");
		repo.initialize();
		RepositoryConnection con = repo.getConnection();
		//con.add(file, baseURI, RDFFormat.TURTLE);
		con.close();
		repo.shutDown();*/
		
		SemagrowQueryBlock semagrowQueryBlock = new SemagrowQueryBlock();
		semagrowQueryBlock.setEast(-6f);//lon
		semagrowQueryBlock.setWest(6.0f);//lon
		semagrowQueryBlock.setSouth(48.0f);//lat
		semagrowQueryBlock.setNorth(56.0f);//lat
		
		String endpoint = "http://143.233.226.44:8080/SemaGrow/sparql";

		NetCDFCreator rdf_to_netcdf = new NetCDFCreator( endpoint, true );
				
		List<String> datasets = new ArrayList<String>();
		datasets.add("http://semagrow.eu/rdf/data/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t");
		
		String meas_var_dataset = "http://semagrow.eu/rdf/data/uas_bced_1960_1999_hadgem2-es_rcp4p5_2031-2040_t_uasAdjust";	
		
		Set<Attribute> global_attributes = rdf_to_netcdf.findGlobalAttributes(datasets);
				
		Map<String, List<DimensionSpecification>> meas_var_dim_metadata = 
				new HashMap<String, List<DimensionSpecification>>();
		List<DimensionSpecification> dim_metadata =
				rdf_to_netcdf.findDimensions(meas_var_dataset);
		
		//create geo box if needed
		rdf_to_netcdf.createBox(dim_metadata, semagrowQueryBlock);
		//store metadata
		meas_var_dim_metadata.put(meas_var_dataset, dim_metadata);
		//create writer object
		NetcdfFileWriter writer = NetcdfFileWriter.createNew(NetcdfFileWriter.Version.netcdf3, "/home/giannis/test.nc");
		//store dimension values in the map
		Map<Variable, Object[]> dim_values_map = new HashMap<Variable, Object[]>();
		//store measurement variables
		List<MeasurementVariable> measurement_variables = new ArrayList<MeasurementVariable>();
		//create netcdf structure
		rdf_to_netcdf.createNetCDFStructure(writer, global_attributes, meas_var_dim_metadata, dim_values_map, measurement_variables);
		//write
		//write dimension values
		rdf_to_netcdf.writeDimensionValues(writer, dim_values_map);
		//write measurement values
		try {
			rdf_to_netcdf.writeMeasurementValues(writer, measurement_variables);
		} catch (MalformedQueryException | QueryEvaluationException e) {
			e.printStackTrace();
		}
		writer.close();
		
	}

}
