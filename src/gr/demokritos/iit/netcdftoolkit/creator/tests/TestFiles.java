/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.creator.tests;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ucar.ma2.Array;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

/**
 * @author Giannis Mouchakis
 *
 */
public class TestFiles {
	
	static Logger logger = LoggerFactory.getLogger(TestFiles.class);
	
	public static void main(String[] args) throws IOException {
		
		NetcdfFile aet_file = NetcdfFile.open( System.getProperty("user.home") + "/small_netcdfs/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_aet_whe_annual_2005_2099_t.nc" );
		//NetcdfFile aet_file = NetcdfFile.open( System.getProperty("user.home") + "/small_netcdfs/epic_hadgem2-es_rcp4p5_ssp2_co2_firr_biom_bar_annual_2005_2099_t.nc" );

		String var_name = "aet";
		NetcdfFile created_file = NetcdfFile.open( System.getProperty("user.home") + "/test_two_files.nc" );
		
		Variable o_aet = aet_file.findVariable(var_name);
		Variable c_aet = created_file.findVariable(var_name);
		
		Array aet_v_o = o_aet.read();
		Array aet_v_c = c_aet.read();
		
		int c = 0;
		Set<Float> set = new HashSet<>();
		for (int i = 0; i < aet_v_o.getSize(); i++) {
			float o_v = aet_v_o.getFloat(i);
			boolean found = false;
			for (int j = 0; j < aet_v_c.getSize(); j++) {
				float c_v = aet_v_c.getFloat(j);
				if (o_v == c_v) {
					found = true;
					break;
				}
			}
			if ( ! found ) {
				set.add(o_v);
				//System.out.println(o_v);
				c++;
			}
		}
		logger.info("set={}",set);
		logger.info("set size={}",set.size());
		logger.info("not_found={}",c);
		
		aet_file.close();
		created_file.close();
		
	}

}
