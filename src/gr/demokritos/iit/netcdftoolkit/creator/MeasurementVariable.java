/**
 * 
 */
package gr.demokritos.iit.netcdftoolkit.creator;

import java.util.List;

import ucar.nc2.Variable;

/**
 * @author Giannis Mouchakis
 *
 */
public class MeasurementVariable {
	
	private VariableMetadata variableMetadata;
	private List<DimensionSpecification> dimensions;
	private String dataset;
	private Variable variable;
	
	public MeasurementVariable() {
		super();
	}

	/**
	 * @return the variableMetadata
	 */
	public VariableMetadata getVariableMetadata() {
		return variableMetadata;
	}

	/**
	 * @param variableMetadata the variableMetadata to set
	 */
	public void setVariableMetadata(VariableMetadata variableMetadata) {
		this.variableMetadata = variableMetadata;
	}

	/**
	 * @return the dimensions
	 */
	public List<DimensionSpecification> getDimensions() {
		return dimensions;
	}

	/**
	 * @param dimensions the dimensions to set
	 */
	public void setDimensions(List<DimensionSpecification> dimensions) {
		this.dimensions = dimensions;
	}

	/**
	 * @return the dataset
	 */
	public String getDataset() {
		return dataset;
	}

	/**
	 * @param dataset the dataset to set
	 */
	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	/**
	 * @return the variable
	 */
	public Variable getVariable() {
		return variable;
	}

	/**
	 * @param variable the variable to set
	 */
	public void setVariable(Variable variable) {
		this.variable = variable;
	}

}