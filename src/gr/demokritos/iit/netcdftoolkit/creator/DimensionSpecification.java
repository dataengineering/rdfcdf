/***************
<p>Title: Dimension Specification</p>

<p>Description:
Dimension Specification
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Giannis Mouchakis (NCSR "D", Semagrow, 2014)

***************/


package gr.demokritos.iit.netcdftoolkit.creator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ucar.nc2.Attribute;

public class DimensionSpecification
{
	
	private String dataset;//TODO:maybe remove
	private String dim_name;
	private String dim_short_name;
	private Long dimension_size;
	private String datatype;
	private Map<String, Object> values;
	private Object from;
	private Object to;
	private Object step;
	private List<Attribute> attribute_list;
	
	/**
	 * @param size the number of dimensions
	 */
	public DimensionSpecification(String dataset) {
		super();
		this.dataset = dataset;
		attribute_list = new ArrayList<Attribute>();
		values = new HashMap<String, Object>();
	}

	/**
	 * @return the dataset
	 */
	public String getDataset() {
		return dataset;
	}

	/**
	 * @param dataset the dataset to set
	 */
	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	/**
	 * @return the dim_name
	 */
	public String getDim_name() {
		return dim_name;
	}

	/**
	 * @param dim_name the dim_name to set
	 */
	public void setDim_name(String dim_name) {
		this.dim_name = dim_name;
	}

	/**
	 * @return the dim_short_name
	 */
	public String getDim_short_name() {
		return dim_short_name;
	}

	/**
	 * @param dim_short_name the dim_short_name to set
	 */
	public void setDim_short_name(String dim_short_name) {
		this.dim_short_name = dim_short_name;
	}

	/**
	 * @return the dimension_size
	 */
	public Long getDimension_size() {
		return dimension_size;
	}

	/**
	 * @param dimension_size the dimension_size to set
	 */
	public void setDimension_size(Long dimension_size) {
		this.dimension_size = dimension_size;
	}

	/**
	 * @return the values
	 */
	public Map<String, Object> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

	/**
	 * @return the from
	 */
	public Object getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Object from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Object getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Object to) {
		this.to = to;
	}

	/**
	 * @return the attribute_list
	 */
	public List<Attribute> getAttribute_list() {
		return attribute_list;
	}

	/**
	 * @param attribute_list the attribute_list to set
	 */
	public void setAttribute_list(List<Attribute> attribute_list) {
		this.attribute_list = attribute_list;
	}

	/**
	 * @return the datatype
	 */
	public String getDatatype() {
		return datatype;
	}

	/**
	 * @param datatype the datatype to set
	 */
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	/**
	 * @return the step of the values
	 */
	public Object getStep() {
		return step;
	}

	/**
	 * @param step the step of the values to set
	 */
	public void setStep(Object step) {
		this.step = step;
	}
	
	
	
}