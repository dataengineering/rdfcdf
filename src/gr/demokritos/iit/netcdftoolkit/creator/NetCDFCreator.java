/***************
<p>Title: NetCDFCreator</p>

<p>Description:
Converter from SPARQL query results to NetCDF and supporting features.
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Georgios Kalampokinis (UAH, Semagrow, 2014)
@author Giannis Mouchakis (NCSR "D", Semagrow, 2014)

***************/


package gr.demokritos.iit.netcdftoolkit.creator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openrdf.model.Literal;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

import gr.demokritos.iit.netcdftoolkit.commons.NetCDFUtils;
import gr.demokritos.iit.netcdftoolkit.commons.NetCDFVocab;
import nl.wur.alterra.semagrow.file.SemagrowQueryBlock;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.Variable;
import ucar.nc2.time.CalendarDate;
import ucar.nc2.units.DateUnit;


public class NetCDFCreator
{
	
	static Logger logger = LoggerFactory.getLogger(NetCDFCreator.class);
	
	private List<Variable> agmip_variables = new ArrayList<>();
	
	String endpoint;
	/**
	 * @param endpoint
	 */ 
	public NetCDFCreator(String endpoint, boolean sorted) {
		super();
		this.endpoint = endpoint;
	}

	public Set<Attribute> findGlobalAttributes(List<String> datasets) {
		
		Set<Attribute> attributes= new HashSet<Attribute>();
		String datasets_str = "";
		for (String dataset : datasets) {
			
			datasets_str += dataset + " ";
			
			String dataset_struct = getDatasetStruct(dataset);
			Set<String> attr_specs = getAttrSpec(dataset_struct);
			
			for (String attr_spec : attr_specs) {
			
				//String p = getAttributeProperty(attr_spec);
				
				String query_str = " PREFIX qb: <http://purl.org/linked-data/cube#> "
						+ " PREFIX sgstruct: <http://semagrow.eu/rdf/struct/> "
						+ "SELECT DISTINCT ?attr_name ?attr_val "
						+ "{ "
						+ "<" + attr_spec + "> qb:componentProperty ?p . "
						+ "?p a qb:AttributeProperty . "
						+ "<" + dataset + "> ?p ?attr_val . "
						+ "<" + attr_spec + "> sgstruct:shortName ?attr_name . "
						+ "}";
				
				Query query = QueryFactory.create(query_str);
				ARQ.getContext().setTrue(ARQ.useSAX);
				QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);

				ResultSet resultSet = qexec.execSelect();
					
				while (resultSet.hasNext()) {
					QuerySolution soln = resultSet.next();
					
					RDFNode attr_name = soln.get("?attr_name");
					String name = attr_name.asLiteral().getString();
					
					RDFNode attr_val = soln.get("?attr_val");
					Object val = attr_val.asLiteral().getValue();
					
					//if attribute name is datasets global attribute skip it and it will be written later.
					if ( ! name.equals(NetCDFVocab.datasets_g_attr)) {
						//constructor does not accept Object but accepts singleton list of Objects
						Attribute attribute = new Attribute(name, Collections.singletonList(val));
						attributes.add(attribute);
					}
					
				}
				
				qexec.close();
				
			}
			
		}	
		
		//add datasets attribute
		datasets_str = datasets_str.trim();
		Attribute attribute = new Attribute(NetCDFVocab.datasets_g_attr , datasets_str);
		attributes.add(attribute);
		
		return attributes;
	}
	
	public List<DimensionSpecification> findDimensions(String dataset ) {
		
		List<DimensionSpecification> dim_spec_list = new ArrayList<DimensionSpecification>();
			
		String query_str = " PREFIX qb: <http://purl.org/linked-data/cube#> "
				+ "SELECT DISTINCT ?property ?codeList ?var_struct ?shortName ?dimensionSize ?dataType "
				+ "{"
				+ "<" + dataset + "> qb:structure ?dstruct . "
				+ "?dstruct qb:component ?var_struct . " 
				+ "?var_struct <" + NetCDFVocab.ShortName.getURI() + "> ?shortName . "
				+ "?var_struct <" + NetCDFVocab.DimensionSize.getURI() + "> ?dimensionSize . "
				+ "?var_struct <" + NetCDFVocab.DataType.getURI() + "> ?dataType . "
				+ "?var_struct qb:componentProperty ?property . "
				+ "?var_struct qb:codeList ?codeList . "
				+ "?property a qb:DimensionProperty . "
				+ "}";
		
		Query query = QueryFactory.create(query_str);
		ARQ.getContext().setTrue(ARQ.useSAX);
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
		ResultSet resultSet = qexec.execSelect();
		
		List<String> codelist_list = new ArrayList<String>();
		List<String> property_list = new ArrayList<String>();
		List<String> var_struct_list = new ArrayList<String>();
		List<String> short_name_list = new ArrayList<String>();
		List<Long> dimension_size_list = new ArrayList<Long>();
		List<String> data_type_list = new ArrayList<String>();
			
		while (resultSet.hasNext()) {
			
			QuerySolution soln = resultSet.next();
			
			RDFNode codelist = soln.get("codeList");
			codelist_list.add(codelist.asResource().getURI());
			
			RDFNode property = soln.get("property");
			property_list.add(property.asResource().getURI());
				
			RDFNode var_struct = soln.get("var_struct");
			var_struct_list.add(var_struct.asResource().getURI());
				
			RDFNode shortName = soln.get("shortName");
			short_name_list.add(shortName.asLiteral().getString());
				
			RDFNode dimensionSize = soln.get("dimensionSize");
			dimension_size_list.add(dimensionSize.asLiteral().getLong());
			
			RDFNode dataType = soln.get("dataType");
			data_type_list.add(dataType.asResource().getURI());
				
		}
			
		qexec.close();
		
		for (int i = 0; i < property_list.size(); i++) {
			DimensionSpecification dim_spec = new DimensionSpecification(dataset);
			dim_spec.setDim_name(property_list.get(i));
			dim_spec.setDim_short_name(short_name_list.get(i));
			dim_spec.setDimension_size(dimension_size_list.get(i));
			dim_spec.setDatatype(data_type_list.get(i));
			dim_spec_list.add(dim_spec);
		}
		
		//URI codeList; // for each property, find value range
		int i = 0;
		for (String codeList : codelist_list) {

			Map<String, Object> values = new LinkedHashMap<String, Object>();
								
			query_str = " PREFIX qb: <http://purl.org/linked-data/cube#> "
					+ " PREFIX sgstruct: <http://semagrow.eu/rdf/struct/> "
					+ " PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> "
					+ "SELECT DISTINCT ?pos ?value ?index "
					+ "{"
					+ "<" + codeList + "> qb:hierarchyRoot ?pos . "
					+ "?pos <http://www.w3.org/1999/02/22-rdf-syntax-ns#value> ?value . "
					+ "?pos sgstruct:index ?index . "
					//+ "} ORDER BY xsd:long(?index) ";
					+ "} ORDER BY ?index ";
				
			query = QueryFactory.create(query_str);
			ARQ.getContext().setTrue(ARQ.useSAX);
			qexec = QueryExecutionFactory.sparqlService(endpoint, query);
			resultSet = qexec.execSelect();
			
			while (resultSet.hasNext()) {
				QuerySolution soln = resultSet.next();
				RDFNode node_pos = soln.get("pos");
				String pos = node_pos.asResource().getURI();
				RDFNode node_value = soln.get("value");
				Object value = node_value.asLiteral().getValue();
				Object prev = values.put(pos, value);
				if (prev != null) {
					logger.error("found same position that has value in the grid. "
							+ "pos=" + pos
							+ " current value="+value.toString()
							+ " previous value="+prev.toString());
				}
			}
				
			qexec.close();
				
			if (values.isEmpty()) {
				logger.error("No results for property {}", property_list.get(i));
			} else {
				dim_spec_list.get(i).setValues(values);
			}
				
			i++;
								
		}
			
		int x = 0;
		for (String var_struct : var_struct_list) {
			List<Attribute> attribute_list = new ArrayList<Attribute>();
			// for each property, find attributes
			query_str = " PREFIX qb: <http://purl.org/linked-data/cube#> "
					+ " PREFIX sgstruct: <http://semagrow.eu/rdf/struct/> "
					+ "SELECT DISTINCT ?short_name ?attr "
					+ "{"
					+ "<" + var_struct + "> qb:component ?var_attribute_struct . "
					+ "?var_attribute_struct qb:componentProperty ?var_attr_name . "
					+ "?var_attribute_struct sgstruct:shortName ?short_name . "
					//+ "<" + var_struct + "> ?var_attr_name ?attr "sgstruct:attrValue
					+ "?var_attribute_struct sgstruct:attrValue ?attr "
					+ "}";
				
			query = QueryFactory.create(query_str);
			ARQ.getContext().setTrue(ARQ.useSAX);
			qexec = QueryExecutionFactory.sparqlService(endpoint, query);
			resultSet = qexec.execSelect();
			
			while (resultSet.hasNext()) {
				QuerySolution soln = resultSet.next();
				RDFNode short_name= soln.get("short_name");
				RDFNode node_attr = soln.get("attr");
					
				String name = short_name.asLiteral().getString();				
				Object attr = node_attr.asLiteral().getValue();
				//constructor does not accept Object but seems to be OK with List<Object>
				Attribute attribute = new Attribute(name, Collections.singletonList(attr));
				attribute_list.add(attribute);
			}
				
			qexec.close();
			
			dim_spec_list.get(x).setAttribute_list(attribute_list);
			
			x++;
				
		}
		
		return dim_spec_list;
				
	}
	
	//TODO: check for bug regarding _ChuncSize attribute.
	private List<VariableMetadata> findMeasurementMetadata(String dataset) {
			
		List<VariableMetadata> var_metadatas = new ArrayList<VariableMetadata>();
		List<String> var_structs = getVarStructMeasurement(dataset);
		for (String var_struct : var_structs) {
			
			VariableMetadata var_metadata = new VariableMetadata(new ArrayList<Attribute>(), null, null, null, null, null);

			String query_str = " PREFIX qb: <http://purl.org/linked-data/cube#> "
					+ " PREFIX sgstruct: <http://semagrow.eu/rdf/struct/> "
					+ " SELECT DISTINCT ?var_s_name ?data_type ?property "
					+ "{ "
					+ "<" + var_struct + "> sgstruct:shortName ?var_s_name . "
					+ "<" + var_struct + "> sgstruct:dataType ?data_type . "
					+ "<" + var_struct + ">  qb:componentProperty ?property . "
					+ "}";
						
			Query query = QueryFactory.create(query_str);
			ARQ.getContext().setTrue(ARQ.useSAX);
			QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
			ResultSet resultSet = qexec.execSelect();
						
			while (resultSet.hasNext()) {
							
				QuerySolution soln = resultSet.next();
								
				RDFNode var_s_name = soln.get("?var_s_name");
				String var_name = var_s_name.asLiteral().getString();
								
				RDFNode data_type_node = soln.get("?data_type");
				String data_type = data_type_node.asResource().getURI();
				
				RDFNode property_node = soln.get("?property");
				String property = property_node.asResource().getURI();
				
				var_metadata.sparqlName = property;
				var_metadata.dataset = dataset;
				var_metadata.shortName = var_name;
				var_metadata.dataType = NetCDFUtils.getDataTypeFromURI(data_type);
							
			}
			qexec.close();
							
			String query_str_attr = " PREFIX qb: <http://purl.org/linked-data/cube#> "
					+ " PREFIX sgstruct: <http://semagrow.eu/rdf/struct/> "
					+ " SELECT DISTINCT ?attr_short_name ?attr_val "
					+ "{ "
					+ "<" + var_struct + "> qb:component ?var_attribute_struct . "
					+ "?var_attribute_struct qb:componentProperty ?attr_name . "
					+ "?var_attribute_struct sgstruct:shortName ?attr_short_name . "
					//+ "<" + var_struct + "> ?attr_name ?attr_val . "
					+ "?var_attribute_struct sgstruct:attrValue ?attr_val "
					+ "}";
							
			Query query_attr = QueryFactory.create(query_str_attr);
			ARQ.getContext().setTrue(ARQ.useSAX);
			QueryExecution qexec_attr = QueryExecutionFactory.sparqlService(endpoint, query_attr);
			ResultSet resultSet_attr = qexec_attr.execSelect();
							
			while (resultSet_attr.hasNext()) {
								
				QuerySolution soln_attr = resultSet_attr.next();
								
				RDFNode attr_short_name = soln_attr.get("?attr_short_name");
				String attr_name = attr_short_name.asLiteral().getString();
								
				RDFNode attr_val = soln_attr.get("?attr_val");
				Object val = attr_val.asLiteral().getValue();
								
				Attribute attribute = new Attribute(attr_name, Collections.singletonList(val));
				var_metadata.attribute.add(attribute);
			}		
			qexec_attr.close();
			var_metadatas.add(var_metadata);
		}

		return var_metadatas;
			
	}
	
	private List<String> getVarStructMeasurement(String dataset) {
		List<String> var_structs = new ArrayList<String>();
		String query_str = " PREFIX qb: <http://purl.org/linked-data/cube#> "
				+ "SELECT DISTINCT ?var_struct "
				+ "{"
				+ "<" + dataset + "> a  qb:DataSet . "
				+ "<" + dataset + "> qb:structure ?dataset_struct . "
				+ "?dataset_struct a qb:DataStructureDefinition . "
				+ "?dataset_struct qb:component ?var_struct . " 
				+ "?var_struct qb:componentProperty ?property . "
				+ "?property a qb:MeasureProperty . "
				+ "}";

		Query query = QueryFactory.create(query_str);
		ARQ.getContext().setTrue(ARQ.useSAX);
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
		ResultSet resultSet = qexec.execSelect();
		while (resultSet.hasNext()) {
			QuerySolution soln = resultSet.next();
			RDFNode var_struct_n = soln.get("?var_struct");
			var_structs.add(var_struct_n.asResource().getURI());
		}
		qexec.close();	
		return var_structs;
	}
	
	public void createNetCDFStructure(NetcdfFileWriter writer, Set<Attribute> g_attributes, 
			Map<String, List<DimensionSpecification>> meas_var_dim_metadata, 
			Map<Variable, Object[]> dim_values_map, Collection<MeasurementVariable> measurement_variables) 
					throws IOException {
		createNetCDFStructure(writer, g_attributes, meas_var_dim_metadata, dim_values_map, measurement_variables,
				Collections.<String>emptyList());
	}
	
	public void createNetCDFStructure(NetcdfFileWriter writer, Set<Attribute> g_attributes, 
			Map<String, List<DimensionSpecification>> meas_var_dim_metadata, 
			Map<Variable, Object[]> dim_values_map, Collection<MeasurementVariable> measurement_variables, 
			Collection<String> agmip_vars) 
					throws IOException {
		
		//write global attributes
		for (Attribute attribute : g_attributes) {
			writer.addGroupAttribute(null, attribute);
		}
		
		//write dim variables
		List<String> written_dim = new ArrayList<String>();
		for (Entry<String, List<DimensionSpecification>> entry : meas_var_dim_metadata.entrySet()) {
			
			MeasurementVariable meas_var = new MeasurementVariable();
			String dataset = entry.getKey();
			meas_var.setDataset(dataset);
			VariableMetadata meas_var_metadata = findMeasurementMetadata(dataset).get(0);//we expect one result here
			meas_var.setVariableMetadata(meas_var_metadata);
			
			String dim_str = "";
			List<DimensionSpecification> dim_spec_list = entry.getValue();
			meas_var.setDimensions(dim_spec_list);
			
			for (DimensionSpecification dim_spec : dim_spec_list) {
				
				String dim_short_name = dim_spec.getDim_short_name();
				dim_str += dim_short_name + " ";
				
				//if not already written
				if ( ! written_dim.contains(dim_short_name)) {
					//write dimensions
					writer.addDimension(null, dim_short_name, dim_spec.getValues().size());
					//add to written 
					DataType data_type = NetCDFUtils.getDataTypeFromURI(dim_spec.getDatatype());
					Variable var = writer.addVariable(null, dim_short_name, data_type, dim_short_name);
					
					for (Attribute att : dim_spec.getAttribute_list()) {
						var.addAttribute(att);
					}
					
					dim_values_map.put(var, fillDimensionValues(dim_spec.getValues().values()));
					written_dim.add(dim_short_name);
				}
			}
			dim_str = dim_str.trim();
			
			Variable var = writer.addVariable(meas_var_metadata.group,
					meas_var_metadata.shortName, meas_var_metadata.dataType,
					dim_str);
			for (Attribute att : meas_var_metadata.attribute) {
				var.addAttribute(att);
			}
			meas_var.setVariable(var);
			//add to measurement variables
			measurement_variables.add(meas_var);
			
		}
		
		//add AgMIP variables
		for(String agmip_var : agmip_vars) {
			Variable agmip = writeAgmipVars(writer, agmip_var);
			agmip_variables.add(agmip);
		}
		
		writer.setFill(true);//fill file with _FillValues.
		writer.setLargeFile(true);
		writer.create();//after this cannot add dims or attrs
	}
	
	
	
	
	
	public void writeDimensionValues(NetcdfFileWriter writer, Map<Variable, Object[]> dim_values_map) throws IOException, InvalidRangeException {
		//write dimension values
		for (Entry<Variable, Object[]> entry : dim_values_map.entrySet()) {
			Variable variable = entry.getKey();
			Object[] values = entry.getValue();
			if (values instanceof Float[]) {
				writer.write(variable, Array.factory( ArrayUtils.toPrimitive( (Float[]) values)) );
			} else if (values instanceof Double[]) {
				writer.write(variable, Array.factory( ArrayUtils.toPrimitive( (Double[]) values)) );
			} else if (values instanceof Short[]) {
				writer.write(variable, Array.factory( ArrayUtils.toPrimitive( (Short[]) values)) );
			} else if (values instanceof Integer[]) {
				writer.write(variable, Array.factory( ArrayUtils.toPrimitive( (Integer[]) values)) );
			} else if (values instanceof Byte[]) {
				writer.write(variable, Array.factory( ArrayUtils.toPrimitive( (Byte[]) values)) );
			} else if (values instanceof Character[]) {
				writer.write(variable, Array.factory( ArrayUtils.toPrimitive( (Character[]) values)) );
			} else if (values instanceof Long[]) {
				writer.write(variable, Array.factory( ArrayUtils.toPrimitive( (Long[]) values)) );
			} else {
				writer.write(variable, Array.factory( values ) );
			}
		}
	}
	
	public void writeMeasurementValues(NetcdfFileWriter writer, List<MeasurementVariable> measurement_variables)
			throws IOException, InvalidRangeException, RepositoryException, MalformedQueryException, QueryEvaluationException {
		
		Repository repository = new SPARQLRepository(endpoint);
		repository.initialize();
		RepositoryConnection connection = repository.getConnection();
		
		for (MeasurementVariable var : measurement_variables) {
			String query_str_beggining = "SELECT ?meas_val ";
			String dataset = var.getDataset();
			if (dataset.contains(("epic_hadgem2-es_rcp2p6_ssp2_co2_firr_yield_whe_annual_2005_2099"))) {
				query_str_beggining += " FROM NAMED <http://143.233.226.36:8891/sparql> ";
				query_str_beggining += " FROM NAMED <http://4store.ipb.ac.rs:8003/sparql/> ";
			} else if (dataset.contains("tasmax_bced_1960_1999_hadgem2-es_rcp2p6_2011-2020")) {
				query_str_beggining += " FROM NAMED <http://143.233.226.36:8891/sparql> ";
				query_str_beggining += " FROM NAMED <http://4store.ipb.ac.rs:8001/sparql/> ";
			} else if (dataset.contains("cgms-wofost_agmerra_hist_default_firr_biom_soy_annual_1980_2010")) {
				query_str_beggining += " FROM NAMED <http://143.233.226.36:8891/sparql> ";
				query_str_beggining += " FROM NAMED <http://143.233.226.52:8892/sparql> ";
			} else if (dataset.contains("cgms-wofost_agmerra_hist_default_firr_yield_soy_annual_1980_2010")) {
				query_str_beggining += " FROM NAMED <http://143.233.226.36:8891/sparql> ";
				query_str_beggining += " FROM NAMED <http://143.233.226.52:8893/sparql> ";
			}
			query_str_beggining += " { ";
			
			List<DimensionSpecification> dim_specs = var.getDimensions();
			int dim_size = dim_specs.size();
			List<String> dim_names = new ArrayList<>(dim_size);
			List<String> dim_properties = new ArrayList<>(dim_size);
			List<String[]> dim_pos_uris = new ArrayList<>(dim_size);
			for (DimensionSpecification dim_spec : dim_specs) {
				dim_names.add(dim_spec.getDim_short_name());
				dim_properties.add(dim_spec.getDim_name());
				dim_pos_uris.add(dim_spec.getValues().keySet().toArray(new String[dim_spec.getValues().keySet().size()]));
			}
			
			//int n = dim_specs.size();
			int[] indices = new int[dim_size];
			long[] ranges = new long[dim_size];
			int i = 0;
			for (String[] uris : dim_pos_uris) {
				ranges[i] = uris.length;
				i++;
			}

			int[] shape = new int[dim_size];
			for (int y = 0; y < shape.length; y++) {
				shape[y] = 1;
			}
			
			String measurement_prop_name = var.getVariableMetadata().sparqlName;
			// Start off at indices = [0,...,0]
			do {
				boolean do_query = true;
				String obs_patterns = "";
				int x = 0;
				str_build:
				for (String property : dim_properties) {
					String uri = dim_pos_uris.get(x)[indices[x]];
					if ( ! uri.startsWith("http")) {
						do_query = false;
						break str_build;
					}
					obs_patterns += "?observation <"+property+"> <"+uri+"> . ";
					x++;
				}
				
				if (do_query) {
					obs_patterns += "?observation <"+measurement_prop_name+"> ?meas_val . }";
					String query_str = query_str_beggining + obs_patterns;
					
					TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);
					TupleQueryResult result = query.evaluate();
					while (result.hasNext()) {
						BindingSet bindingSet = result.next();
						Literal meas_val_literal = (Literal) bindingSet.getValue("meas_val");
						Object obj = NetCDFUtils.getObjectFromLiteral(meas_val_literal);
						Array result_array = Array.factory(var.getVariableMetadata().dataType, shape);
						result_array.setObject(0, obj);
						writer.write(var.getVariable(), indices, result_array);
					}
					result.close();
				}
				
				advanceIndices(dim_size, indices, ranges);
			}
			while( ! allMaxed(dim_size, indices, ranges) );
			
			//do last one
			boolean do_query = true;
			String obs_patterns = "";
			int x = 0;
			str_build:
			for (String property : dim_properties) {
				String uri = dim_pos_uris.get(x)[indices[x]];
				if ( ! uri.startsWith("http")) {
					do_query = false;
					break str_build;
				}
				obs_patterns += "?observation <"+property+"> <"+uri+"> . ";
				x++;
			}
			
			if (do_query) {
				obs_patterns += "?observation <"+measurement_prop_name+"> ?meas_val . }";
				String query_str = query_str_beggining + obs_patterns;
				
				TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);
				TupleQueryResult result = query.evaluate();
				while (result.hasNext()) {
					BindingSet bindingSet = result.next();
					Literal meas_val_literal = (Literal) bindingSet.getValue("meas_val");
					Object obj = NetCDFUtils.getObjectFromLiteral(meas_val_literal);
					Array result_array = Array.factory(var.getVariableMetadata().dataType, shape);
					result_array.setObject(0, obj);
					writer.write(var.getVariable(), indices, result_array);
				}
				result.close();
			}
			
		
		}
		connection.close();
		repository.shutDown();
	}

	private String getDatasetStruct(String dataset) {
		String dataset_struct = null;
		String query_str = " PREFIX qb: <http://purl.org/linked-data/cube#> "
				+ " SELECT DISTINCT ?dataset_struct  "
				+ "{ "
				+ "<" + dataset + "> a  qb:DataSet . "
				+ "<" + dataset + "> qb:structure ?dataset_struct . "
				+ "?dataset_struct a qb:DataStructureDefinition . "
				+ "}";
		Query query = QueryFactory.create(query_str);
		ARQ.getContext().setTrue(ARQ.useSAX);
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
		ResultSet resultSet = qexec.execSelect();
		int i = 0;
		while (resultSet.hasNext()) {
			 if (i > 1){
				logger.error("Found dataset with more than one dataset structs");
				qexec.close();
				return null;
			} 
			QuerySolution soln = resultSet.next();
			RDFNode dataset_struct_n = soln.get("?dataset_struct");
			dataset_struct = dataset_struct_n.asResource().getURI();
			i++;
		}
		qexec.close();	
		if (i == 0) {
			logger.error("Found dataset with no dataset structs");
			return null;
		} else return dataset_struct;
	}
	
	private Set<String> getAttrSpec(String dataset_struct) {
		Set<String> attr_spec = new HashSet<String>();
		String query_str = " PREFIX qb: <http://purl.org/linked-data/cube#> "
				+ "SELECT DISTINCT ?attr_spec "
				+ "{ "
				+ "<" + dataset_struct + "> qb:component ?attr_spec . " 
				+ "?attr_spec a qb:ComponentSpecification . "
				+ "?attr_spec qb:componentProperty ?p . "
				+ "?p a qb:AttributeProperty . "
				+ "}";
		Query query = QueryFactory.create(query_str);
		ARQ.getContext().setTrue(ARQ.useSAX);
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
		ResultSet resultSet = qexec.execSelect();
		while (resultSet.hasNext()) {
			QuerySolution soln = resultSet.next();
			RDFNode attr_spec_n = soln.get("?attr_spec");
			attr_spec.add(attr_spec_n.asResource().getURI());
		}
		qexec.close();	
		return attr_spec;
	}
	
	private Object[] fillDimensionValues(Collection<Object> values) {
		for (Object obj : values) {// one iteration just to get the class type. maybe this can be better implemented...
			if (obj instanceof Float) {
				Float[] arr = new Float[values.size()];
				int a = 0;
				for (Object v : values) {
					arr[a] = (Float) v;
					a++;
				}
				return arr;
			} else if (obj instanceof Double) {
				Double[] arr = new Double[values.size()];
				int a = 0;
				for (Object v : values) {
					arr[a] = (Double) v;
					a++;
				}
				return arr;
			} else if (obj instanceof Long) {
				Long[] arr = new Long[values.size()];
				int a = 0;
				for (Object v : values) {
					arr[a] = (Long) v;
					a++;
				}
				return arr;
			} else if (obj instanceof Short) {
				Short[] arr = new Short[values.size()];
				int a = 0;
				for (Object v : values) {
					arr[a] = (Short) v;
					a++;
				}
				return arr;
			} else if (obj instanceof Integer) {
				Integer[] arr = new Integer[values.size()];
				int a = 0;
				for (Object v : values) {
					arr[a] = (Integer) v;
					a++;
				}
				return arr;
			} else if (obj instanceof Byte) {
				Byte[] arr = new Byte[values.size()];
				int a = 0;
				for (Object v : values) {
					arr[a] = (Byte) v;
					a++;
				}
				return arr;
			} else if (obj instanceof Character) {
				Character[] arr = new Character[values.size()];
				int a = 0;
				for (Object v : values) {
					arr[a] = (Character) v;
					a++;
				}
				return arr;
			} else if (obj instanceof String) {
				String[] arr = new String[values.size()];
				int a = 0;
				for (Object v : values) {
					arr[a] = (String) v;
					a++;
				}
				return arr;
			} else {				
				logger.warn("value is not Number, Charater, String");
				return values.toArray (new Object[values.size()]);
			}
		}
		return null;
	}
		
	public Map<String, Boolean> createBox(List<DimensionSpecification> dim_metadata,
			SemagrowQueryBlock semagrowQueryBlock) {
		Map<String, Boolean> no_values_map = new HashMap<String, Boolean>();
		for (DimensionSpecification dim_spec : dim_metadata) {
			String name = dim_spec.getDim_short_name();
			if (name.equals("lon")) {
				Float small = semagrowQueryBlock.getWest();
				Float big = semagrowQueryBlock.getEast();
				boolean no_values = latLonBox(dim_spec, small, big);
				no_values_map.put(name, no_values);
			} else if (name.equals("lat")) {
				Float small = semagrowQueryBlock.getSouth();
				Float big = semagrowQueryBlock.getNorth();
				boolean no_values = latLonBox(dim_spec, small, big);
				no_values_map.put(name, no_values);
			} else if (name.equals("time")) {
				Date from_d = semagrowQueryBlock.getFrom();
				Date to_d = semagrowQueryBlock.getTo();
				boolean no_values = timeBox(dim_spec, from_d, to_d);
				no_values_map.put(name, no_values);
			}
		}
		return no_values_map;
	}

	
	private boolean latLonBox(DimensionSpecification dim_spec, float small, float big) {//small is south or west, big is north or east
		
		boolean no_values = true;
		
		//calculate step
		Iterator<Entry<String, Object>> it = dim_spec.getValues().entrySet().iterator();
		float first = 0;
		float second = 0;
		if (it.hasNext()) {
			first = Float.parseFloat(it.next().getValue().toString());
		} else {
			logger.error("Cannot create geo box without dimension values", new UnsupportedOperationException());
		}
		if (it.hasNext()) {
			second = Float.parseFloat(it.next().getValue().toString());
		}
		float step = Math.abs(second - first);
		
		//find values
		LinkedList<String> pos = new LinkedList<String>();
		LinkedList<Object> vals = new LinkedList<Object>();
		if (big >= small) {
			for (Entry<String, Object> entry : dim_spec.getValues().entrySet()) {
				float value = Float.parseFloat(entry.getValue().toString());
				if (value > small - step && value < big + step)  {
					String key = entry.getKey();
					pos.add(key);
					vals.add(value);
				}
			}
		} else {
			for (Entry<String, Object> entry : dim_spec.getValues().entrySet()) {
				float value = Float.parseFloat(entry.getValue().toString());
				if ( ! (value < small - step && value > big + step ) )  {
					String key = entry.getKey();
					pos.add(key);
					vals.add(value);
				}
			}
		}
		
		no_values = vals.isEmpty();
		
		//build back
		int i=0;
		Map<String, Object> new_map = new LinkedHashMap<>();
		for (String str : pos) {
			Object test = new_map.put(str, vals.get(i));
			if (test != null) {
				logger.error("error building back the grid");
				throw new RuntimeException();
			}
			i++;
		}
		dim_spec.setFrom(small);
		dim_spec.setTo(big);
		dim_spec.setDimension_size((long) new_map.size());
		dim_spec.setValues(new_map);
		dim_spec.setStep(step);
		
		return no_values;	
	}
	
	private boolean timeBox(DimensionSpecification dim_spec, Date from_d, Date to_d) {
		boolean no_values = true;
		for (Attribute attribute: dim_spec.getAttribute_list()) {
			if (attribute.getShortName().equals("units")) {
				String units = attribute.getStringValue();
				if (units != null) {
					CalendarDate from =  CalendarDate.of(from_d);
					CalendarDate to =  CalendarDate.of(to_d);
					Iterator<Entry<String, Object>> it = dim_spec.getValues().entrySet().iterator();
					double first = (double) it.next().getValue();
					double second = (double) it.next().getValue();
					double step = Math.abs(second - first);
					LinkedList<String> pos = new LinkedList<String>();
					LinkedList<Double> vals = new LinkedList<Double>();
					for (Entry<String, Object> entry : dim_spec.getValues().entrySet()) {
						Double value = (Double) entry.getValue();
						String formated_value = value.toString() + " " + units;
						CalendarDate c_date = CalendarDate.parseUdunits(null, formated_value);
						if ( (c_date.compareTo(from) == 0 || c_date.compareTo(to) == 0) ||
								(c_date.isAfter(from) && c_date.isBefore(to)) ) {
							String key = entry.getKey();
							pos.add(key);
							vals.add(value);
						}
					}
					no_values = vals.isEmpty();
					if (no_values) {
						DateUnit du = null;
						try {
							du = new DateUnit(units);
						} catch (Exception e) {
							logger.error("Error while making new date units", e);
							throw new RuntimeException(e);
						}
						vals.addFirst(du.makeValue(from_d));
						vals.addLast(du.makeValue(to_d));
					}
					//add filters
					dim_spec.setFrom(vals.getFirst());
					dim_spec.setTo(vals.getLast());
					
					int i=0;
					Map<String, Object> new_map = new LinkedHashMap<>();
					for (String str : pos) {
						Object test = new_map.put(str, vals.get(i));
						if (test != null) {
							logger.error("error building back the grid");
							throw new RuntimeException();
						}
						i++;
					}
					dim_spec.setDimension_size((long) new_map.size());
					dim_spec.setValues(new_map);
					dim_spec.setStep(step);
				}
				break;
			}
		}
		return no_values;
	}
	
	// Advances 'indices' to the next in sequence.
    private void advanceIndices(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i]+1 == ranges[i]) {
                indices[i] = 0;
            }
            else {
                indices[i] += 1;
                break;
            }
        }

    }

    // Tests if indices are in final position.
    private boolean allMaxed(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i] != ranges[i]-1) {
                return false;
            }
        }
        return true;

    }
    
    
    private Variable writeAgmipVars(NetcdfFileWriter writer, String dataset_var) {
    	
    	Variable variable = null;
    	
    	String[] split = dataset_var.split("_", 2);//0=dataset, 1=variable
    	String exp_id = split[0];
		String var = split[1];
		
		String exp_name = getExpName(exp_id);
		String var_name = exp_name + "_" + var;
    	
    	if (var.equals("rain")) {
    		
    		variable = writer.addVariable(null, var_name, DataType.FLOAT, "time lat lon");
    		variable.addAttribute(new Attribute("long_name", "Rainfall, including moisture in snow, in one day"));
    		variable.addAttribute(new Attribute("units", "mm/d"));
    		variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	} else if (var.equals("tmin")) {
    		
    		variable = writer.addVariable(null, var_name, DataType.FLOAT, "time lat lon");
    		variable.addAttribute(new Attribute("long_name", "Temperature of air, minimum"));
    		variable.addAttribute(new Attribute("units", "degrees C"));
    		variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	} else if (var.equals("tmax")) {
    		
    		variable = writer.addVariable(null, var_name, DataType.FLOAT, "time lat lon");
    		variable.addAttribute(new Attribute("long_name", "Temperature of air, maximum"));
    		variable.addAttribute(new Attribute("units", "degrees C"));
    		variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	} else if (var.equals("srad")) {
    		
    		variable = writer.addVariable(null, var_name, DataType.FLOAT, "time lat lon");
    		variable.addAttribute(new Attribute("long_name", "Solar radiation"));
    		variable.addAttribute(new Attribute("units", "MJ/m2.d"));
    		variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	} else if (var.equals("wind")) {
    		
    		variable = writer.addVariable(null, var_name, DataType.FLOAT, "time lat lon");
    		variable.addAttribute(new Attribute("long_name", "Wind speed, daily"));
    		variable.addAttribute(new Attribute("units", "km/d"));
    		variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	} else if (var.equals("dewp")) {
    		
    		variable = writer.addVariable(null, var_name, DataType.FLOAT, "time lat lon");
    		variable.addAttribute(new Attribute("long_name", "Temperature, dewpoint, daily average"));
    		variable.addAttribute(new Attribute("units", "degrees C"));
    		variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	} else if (var.equals("vprsd")) {
		
			variable = writer.addVariable(null, var_name, DataType.FLOAT, "time lat lon");
			variable.addAttribute(new Attribute("long_name", "Vapor pressure"));
			variable.addAttribute(new Attribute("units", "kPa"));
			variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	} else if (var.equals("rhumd")) {
    		
    		variable = writer.addVariable(null, var_name, DataType.FLOAT, "time lat lon");
			variable.addAttribute(new Attribute("long_name", "Relative humidity at TMIN (or max rel. hum)"));
			variable.addAttribute(new Attribute("units", "%"));
			variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	} else {
    		
    		variable = writer.addVariable(null, var_name, DataType.STRING, "time lat lon");
    		variable.addAttribute(new Attribute("source", "AgMIP"));
    		
    	}
    	
    	String minMax[] = getMinMaxAgmipValues(split[0], var);
		if (minMax[0] != null && minMax[1] != null) {
			variable.addAttribute(new Attribute("min_value", minMax[0]));
    		variable.addAttribute(new Attribute("max_value", minMax[1]));
		}
		
    	return variable;
    	
    }
    
    
    private String getExpName(String exp_id) {
    	
    	String exp_name = null;
    	
    	String query_str = "SELECT ?exp_name { "
    			+ "<http://www.semagrow.eu/agmip/experiment/" + exp_id + "> <http://semagrow.eu/agmip/experiment/exname> ?exp_name "
    			+ "} LIMIT 1";
		
		try {
			Repository repository = new SPARQLRepository(endpoint);
			repository.initialize();
			RepositoryConnection connection = repository.getConnection();
			
			TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);
			TupleQueryResult result = query.evaluate();
			
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				exp_name = bindingSet.getValue("exp_name").stringValue();
			}
			
			result.close();
			connection.close();
			repository.shutDown();
			
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			logger.error("could not query AgMIP repository", e);
		}
		
		return exp_name;
    	
    }
    
    
    private DataType getDataTypeFromAgmipVar(String var) {	
    	if (var.equals("rain") || var.equals("tmin") || var.equals("tmax") || var.equals("srad") || 
    			var.equals("wind") || var.equals("dewp") || var.equals("vprsd") || var.equals("rhumd") ) {
    		return DataType.FLOAT;
    	} else {
    		return DataType.STRING;
    	}
    }
    
    
    public void writeAgmipValues(List<String> agmip_vars, List<DimensionSpecification> dim_specs, NetcdfFileWriter writer) throws IOException, InvalidRangeException {

    	for (String dataset_var : agmip_vars) {
    		
			Float lat = null;
			Float lon = null;
			String exname = null;
			
			String[] split = dataset_var.split("_", 2);//0=dataset, 1=variable
			String exp_url = "<http://www.semagrow.eu/agmip/experiment/" + split[0] + ">";
			String var = split[1];
			
			String query_str = "SELECT ?exname ?fl_lat ?fl_long { \n"
					+ exp_url + " <http://semagrow.eu/agmip/experiment/exname> ?exname . \n" 
					+ exp_url + " <http://semagrow.eu/agmip/experiment/fl_lat> ?fl_lat . \n" 
					+ exp_url + " <http://semagrow.eu/agmip/experiment/fl_long> ?fl_long . \n"  
					+ " } ";
			
			try {
				Repository repository = new SPARQLRepository(endpoint);
				repository.initialize();
				RepositoryConnection connection = repository.getConnection();
				
				TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);
				TupleQueryResult result = query.evaluate();
				
				while (result.hasNext()) {
					BindingSet bindingSet = result.next();
					exname = bindingSet.getValue("exname").stringValue();
					lat = new Float(bindingSet.getValue("fl_lat").stringValue());
					lon = new Float(bindingSet.getValue("fl_long").stringValue());
				}
				
				result.close();
				connection.close();
				repository.shutDown();
				
			} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
				logger.error("could not retrieve AgMIP lat lon for dataset " + split[0], e);
			}
			
			
			int lon_pos = 0;
			int lat_pos = 0;
			Map<Integer, Object> tim_pos_results = new HashMap<>();
			
			for (DimensionSpecification dim : dim_specs) {
				
				if (dim.getDim_short_name().equals("lon")) {
					
					Float[] isimip_values = dim.getValues().values().toArray(new Float[dim.getValues().values().size()]);
					lon_pos = findClosestLatLon(lon, isimip_values);
					
				} else if (dim.getDim_short_name().equals("lat")) {
					
					Float[] isimip_values = dim.getValues().values().toArray(new Float[dim.getValues().values().size()]);
					lat_pos = findClosestLatLon(lat, isimip_values);
					
				} else if (dim.getDim_short_name().equals("time")) {
					
					attr:
					for(Attribute attr : dim.getAttribute_list()) {
						
						if (attr.getShortName().equals("units")) {//find units
							
							String units = attr.getStringValue();		
							if (units == null) {
								logger.error("Did not find units attribute. Cannot continue");
								return;							
							}
												
							Map<Long, Object> time_marked_results;
							if (units.startsWith("years")) {		
								
								String aggr = null;
								if (var.equals("rain")) {
									aggr = "sum";
								} else {
									aggr = "avg";
								}
								
								time_marked_results = getAgmipYearMergedResults(exp_url, var, aggr);
									
							} else  {
								time_marked_results = getAgmipResults(exp_url, var);
							}
							
							for (Entry<Long, Object> e : time_marked_results.entrySet()) {
								int tim_pos = findClosestTime(dim, e.getKey(), units);
								tim_pos_results.put(tim_pos, e.getValue());
							}
							
							break attr;
						}
					}
				}
			}	
			
			int[] shape = {1, 1, 1};
			int[] indices = new int[3];
			
			//find netcdf variable for this var
			Variable netcdf_var = null;
			String var_name = exname + "_" + var;
			for (Variable ag_netcdf_var  : agmip_variables) {
				if (ag_netcdf_var.getShortName().equals(var_name)) {
					netcdf_var = ag_netcdf_var;
				}
			}
			
			DataType data_type = getDataTypeFromAgmipVar(var);
			Array result_array = Array.factory(data_type, shape);
			for (Entry<Integer, Object> e : tim_pos_results.entrySet()) {
				
				if (data_type.equals(DataType.FLOAT)) {
					result_array.setFloat(0, new Float(e.getValue().toString()));
				} else {
					result_array.setObject(0, e.getValue().toString());
				}
				
				indices[0] = e.getKey();//time
				indices[1] = lat_pos;//lat
				indices[2] = lon_pos;//lon

				writer.write(netcdf_var, indices, result_array);
			}
		}
    }
	
    
    public boolean isAgmipDataset(String dataset) {
    	
    	boolean isAgmip = false;
    	
    	String query_str = "SELECT * { <http://www.semagrow.eu/agmip/experiment/" + dataset + "> ?p ?o } LIMIT 1";
		
		try {
			Repository repository = new SPARQLRepository(endpoint);
			repository.initialize();
			RepositoryConnection connection = repository.getConnection();
			
			TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);
			TupleQueryResult result = query.evaluate();
			
			if (result.hasNext()) isAgmip = true;
			
			result.close();
			connection.close();
			repository.shutDown();
			
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			logger.error("could not query AgMIP repository", e);
		}
    	return isAgmip;
    }
    
    private int findClosestLatLon(Float agmip_value, Float[] isimip_values) {
		int idx = 0;
		float distance = Math.abs(isimip_values[0] - agmip_value);
		for(int i = 1; i < isimip_values.length; i++){
		    float cdistance = Math.abs(isimip_values[i] - agmip_value);
		    if(cdistance < distance){
		    	idx = i;
		        distance = cdistance;
		    }
		}
		return idx;
	}
	
	private int findClosestTime(DimensionSpecification  dim, Long millis, String units) {
		
		int time_pos = 0;
		CalendarDate c_agmip =  CalendarDate.of(millis);
		
		Double[] isimip_values = dim.getValues().values().toArray(new Double[dim.getValues().values().size()]);
		String formated_value = isimip_values[0].toString() + " " + units;
		CalendarDate c_date = CalendarDate.parseUdunits(null, formated_value);
									
		float distance = Math.abs(c_date.getDifferenceInMsecs(c_agmip));
		for(int i = 1; i < isimip_values.length; i++) {
			formated_value = isimip_values[i].toString() + " " + units;
			 c_date = CalendarDate.parseUdunits(null, formated_value);
		    float c_distance = Math.abs(c_date.getDifferenceInMsecs(c_agmip));
		    if(c_distance < distance){
		    	time_pos = i;
		        distance = c_distance;
		    }
		}
		return time_pos;
	}
	
	
	private Map<Long, Object> getAgmipYearMergedResults(String exp_url, String var, String aggr) {
		
		Map<Long, Object> results = new HashMap<>();
		
		String var_url = "<http://semagrow.eu/agmip/record/" + var + ">";
		
		String query_str = "PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#> \n" + 
				"\n" + 
				"select ?station ?time (" + aggr + "(xsd:double(?var)) as ?res) \n" + 
				"where { \n" + 
				"select  ?station ?var (year(?date) as ?time)\n" + 
				"where { \n" + 
				exp_url + " <http://semagrow.eu/agmip/experiment/hasWeatherStation> ?station . \n" + 
				"?station <http://semagrow.eu/agmip/hasRecord> ?record . \n" + 
				"?record <http://semagrow.eu/agmip/record_date> ?date . \n" + 
				"?record " + var_url + " ?var \n" + 
				" }\n" + 
				"} group by  ?station ?time";
		

			
			Query query = QueryFactory.create(query_str);
			ARQ.getContext().setTrue(ARQ.useSAX);
			QueryExecution qexec = QueryExecutionFactory.sparqlService("http://143.233.226.52:8080/strabon-endpoint-3.3.2-SNAPSHOT/Query", query);
			ResultSet resultSet = qexec.execSelect();
			
			while (resultSet.hasNext()) {
				
				QuerySolution soln = resultSet.next();
				Integer time = (Integer) soln.get("time").asLiteral().getValue();
				Object res = soln.get("res").asLiteral().getValue();
				
				DateTime dt = new DateTime();
				DateTime t = dt.withYear(time);
				long millis = t.getMillis();

				if (results.put(millis, res) != null) {//put results in map
					logger.error("found previous results in for date " + time);//this should not happen, maybe throw exception
				}
			}
			
			qexec.close();
			

		return results;
		
	}
	
	private Map<Long, Object> getAgmipResults(String exp_url, String var) {
		
		Map<Long, Object> results = new HashMap<>();
		
		String var_url = "<http://semagrow.eu/agmip/record/" + var + ">";
		
		String query_str = "select DISTINCT ?time ?res \n" + 
				"where {\n" + 
				exp_url +" <http://semagrow.eu/agmip/experiment/hasWeatherStation> ?station . \n" + 
				"?station <http://semagrow.eu/agmip/hasRecord> ?record. \n" + 
				"?record <http://semagrow.eu/agmip/record_date> ?time . \n" + 
				"?record " + var_url + " ?res \n" +  
				"}";
		
		Query query = QueryFactory.create(query_str);
		ARQ.getContext().setTrue(ARQ.useSAX);
		QueryExecution qexec = QueryExecutionFactory.sparqlService("http://143.233.226.52:8080/strabon-endpoint-3.3.2-SNAPSHOT/Query", query);
		ResultSet resultSet = qexec.execSelect();
		
		while (resultSet.hasNext()) {
			QuerySolution soln = resultSet.next();
			
				String time = soln.get("time").asLiteral().getValue().toString();
				Object res = soln.get("res").asLiteral().getValue();
				
				DateTimeFormatter xsdDateFormat = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'").withZoneUTC();
				DateTime t = xsdDateFormat.parseDateTime(time);
				long millis = t.getMillis();
				
				if (results.put(millis, res) != null) {//put results in map
					logger.error("found previous results in for date " + time);//this should not happen, maybe throw exception
				}
		}
			
		qexec.close();
		
		return results;
		
	}
	
	
	private String[] getMinMaxAgmipValues(String dataset, String var) {
		
		String exp_url = "<http://www.semagrow.eu/agmip/experiment/" + dataset + ">";
		String var_url = "<http://semagrow.eu/agmip/record/" + var + ">";
		
		String query_str = "SELECT DISTINCT ?result \n" + 
				"where {\n" + 
				exp_url +" <http://semagrow.eu/agmip/experiment/hasWeatherStation> ?station . \n" + 
				"?station <http://semagrow.eu/agmip/hasRecord> ?record. \n" + 
				"?record " + var_url + " ?result \n" +  
				"}";
		
		List<Object> results = new ArrayList<Object>();
		boolean isFloat = false;
		if (var.equals("rain") || var.equals("tmin") || var.equals("tmax") || var.equals("srad") || 
    			var.equals("wind") || var.equals("dewp") || var.equals("vprsd") || var.equals("rhumd") ) {
			isFloat = true;
    	}
		
		try {
			Repository repository = new SPARQLRepository(endpoint);
			repository.initialize();
			RepositoryConnection connection = repository.getConnection();
			
			TupleQuery query = connection.prepareTupleQuery(QueryLanguage.SPARQL, query_str);
			TupleQueryResult result = query.evaluate();
			
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				if (isFloat) {
					results.add(new Float(bindingSet.getValue("result").stringValue()));
				} else {
					results.add(bindingSet.getValue("result").stringValue());
				}
			}
			
			result.close();
			connection.close();
			repository.shutDown();
			
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			logger.error("could not query AgMIP repository", e);
		}
		
		Object[] arr = results.toArray();
		Arrays.sort(arr);
		String min = null;
		String max = null;
		if (arr.length > 0) {
			min = arr[0].toString();
			max = arr[arr.length - 1].toString();
		}
		
		String[] minMax = {min, max}; 
		return minMax; 
		
	}
}
