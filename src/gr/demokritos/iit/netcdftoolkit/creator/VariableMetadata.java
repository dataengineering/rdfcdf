/***************
<p>Title: NetCDF Variable Metadata</p>

<p>Description:
NetCDF Variable Metadata
</p>

<p>
This file is part of the Semagrow NetCDF Toolkit.
<br> Copyright (c) 2013-2014 University of Alcala de Henares
<br> Copyright (c) 2014 National Centre for Scientific Research "Demokritos"
</p>

<p>
The Semagrow NetCDF Toolkit is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
</p>

<p>
The Semagrow NetCDF Toolkit is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
</p>

<p>
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
</p>

@author Sotiris Konstantinidis (SemaGrow 2013-2014)
@author Georgios Kalampokinis (SemaGrow 2014)
@author Giannis Mouchakis (SemaGrow 2014)

***************/


package gr.demokritos.iit.netcdftoolkit.creator;

import java.util.List;

import ucar.ma2.DataType;
import ucar.nc2.Attribute;
import ucar.nc2.Group;


public class VariableMetadata
{
	
	List<Attribute> attribute;
	DataType dataType;
	//List<String> dimension;
	Group group;
	String shortName;
	String sparqlName;
	String dataset;
	

	public VariableMetadata(List<Attribute> attribute, DataType dataType,
			/*List<String> dimension,*/ Group group, String shortName,
			String sparqlName, String dataset) {
		super();
		this.attribute = attribute;
		this.dataType = dataType;
		//this.dimension = dimension;
		this.group = group;
		this.shortName = shortName;
		this.sparqlName = sparqlName;
		this.dataset = dataset;
	}

	
	
	

}
