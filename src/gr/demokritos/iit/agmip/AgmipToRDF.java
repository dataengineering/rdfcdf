/**
 * 
 */
package gr.demokritos.iit.agmip;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import org.agmip.ace.AceDataset;
import org.agmip.ace.AceEvent;
import org.agmip.ace.AceEventType;
import org.agmip.ace.AceExperiment;
import org.agmip.ace.AceRecord;
import org.agmip.ace.AceRecordCollection;
import org.agmip.ace.AceSoil;
import org.agmip.ace.AceWeather;
import org.agmip.ace.io.AceParser;
import org.agmip.ace.lookup.LookupCodes;
import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gr.demokritos.iit.netcdftoolkit.commons.SearchUtils;

/**
 * @author Giannis Mouchakis
 *
 */
public class AgmipToRDF {
	
	static Logger logger = LoggerFactory.getLogger(AgmipToRDF.class);
	
	final private DateTimeFormatter agmipDateFormat = DateTimeFormat.forPattern("yyyyMMdd");
	final private DateTimeFormatter xsdDateFormat = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
	final private AceDataset dataset;
	
	/**
	 * @param aceb file
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public AgmipToRDF(File aceb) throws FileNotFoundException, IOException {
		super();
		this.dataset = AceParser.parseACEB(new FileInputStream(aceb));
	}

	public void triplify(final Writer writer) throws IOException {
		
		//experiments
		List<AceExperiment> experiments = dataset.getExperiments();
		for (AceExperiment experiment : experiments) {
			
			String eid = experiment.getValue("eid");
			String ex_uri = "<http://www.semagrow.eu/agmip/experiment/" + eid + ">";
						
			String fl_lat = null;
			String fl_long = null;
			Set<String> keys = experiment.keySet();
			for (String key : keys) {
				String value = experiment.getValue(key);
				
				if (key.equals("fl_lat")) {//latitude
					fl_lat = value;
				} else if (key.equals("fl_long")) {//longitude
					fl_long = value;
				} else if (key.equals("sdat")) {//start date
					String sdat = "\"" + xsdDateFormat.print(agmipDateFormat.parseDateTime(value)) + 
							"\"^^<http://www.w3.org/2001/XMLSchema#dateTime>";
					writer.write(ex_uri + " <http://semagrow.eu/agmip/experiment/starts> " + sdat + " . \n");
				} else if (key.equals("endat")) {//end date
					String endat = "\"" + xsdDateFormat.print(agmipDateFormat.parseDateTime(value)) + 
							"\"^^<http://www.w3.org/2001/XMLSchema#dateTime>";
					writer.write(ex_uri + " <http://semagrow.eu/agmip/experiment/ends> " + endat + " . \n");
				} else if (key.equals("sid")) {//soil
					writer.write(ex_uri + " <http://semagrow.eu/agmip/experiment/hasSoil> <http://semagrow.eu/agmip/soil/" + value + "> . \n");
				} else if (key.equals("wid")) {//weather station
					writer.write(ex_uri + " <http://semagrow.eu/agmip/experiment/hasWeatherStation> <http://semagrow.eu/agmip/weather_station/" + value + "> . \n");
				} 
				//finally write everything to also keep the original values
				writer.write(ex_uri + " <http://semagrow.eu/agmip/experiment/" + URLEncoder.encode(key, "UTF-8") + "> \"" + escapeLiteral(value) + "\" . \n");

			}
			
			//write strabon POINT
			writer.write(ex_uri	+ " <http://www.w3.org/2003/01/geo/wgs84_pos#geometry> "
					+ "\"POINT(" + fl_long + " " + fl_lat + ")\"^^<http://strdf.di.uoa.gr/ontology#WKT> . \n");
						
			//write events
			List<AceEvent> events = experiment.getEvents().asList();
			for (AceEvent event : events) {
				
				String event_uri = "<http://semagrow.eu/agmip/event/" + eid + "_" + event.getEventType().name() + ">";
				writer.write(ex_uri + " <http://semagrow.eu/agmip/experiment/hasEvent> " + event_uri + " . \n");
				Set<String> event_keys = event.keySet();
				for (String event_key : event_keys) {
					String event_value = event.getValue(event_key);
					if (event_key.equals("date")) {
						String event_date = "\"" + xsdDateFormat.print(agmipDateFormat.parseDateTime(event_value)) + 
								"\"^^<http://www.w3.org/2001/XMLSchema#dateTime>";
						writer.write(event_uri + " <http://semagrow.eu/agmip/event/event_date> " + event_date + " . \n");
					}
					writer.write(event_uri + " <http://semagrow.eu/agmip/event/" + URLEncoder.encode(event_key, "UTF-8") + "> \"" + escapeLiteral(event_value) + "\" . \n");
				}
			}
			
			//write start and end date
			if ( ! events.isEmpty() ) {
				//write first
				AceEvent start_event = events.get(0);
				String start_date = start_event.getValue("date");
				if (start_date != null) {
					String start_date_form = "\"" + xsdDateFormat.print(agmipDateFormat.parseDateTime(start_date)) + 
							"\"^^<http://www.w3.org/2001/XMLSchema#dateTime>";
					writer.write(ex_uri + " <http://semagrow.eu/agmip/experiment/minTimeValue> " + start_date_form + " . \n");
				}
				//write last
				AceEvent end_event = events.get(events.size() - 1);
				String end_date = end_event.getValue("date");
				if (end_date != null) {
					String end_date_form = "\"" + xsdDateFormat.print(agmipDateFormat.parseDateTime(end_date)) + 
							"\"^^<http://www.w3.org/2001/XMLSchema#dateTime>";
					writer.write(ex_uri + " <http://semagrow.eu/agmip/experiment/maxTimeValue> " + end_date_form + " . \n");
				}
			}
			
		}
		
		//soil
		List<AceSoil> soils = dataset.getSoils();
		for (AceSoil soil : soils) {
			String sid = soil.getId();
			String soil_uri = "<http://semagrow.eu/agmip/soil/" + sid + ">";
			Set<String> soil_keys = soil.keySet();
			for (String soil_key : soil_keys) {
				String soil_value = soil.getValue(soil_key);
				writer.write(soil_uri + " <http://semagrow.eu/agmip/soil/" + URLEncoder.encode(soil_key, "UTF-8") + "> \"" + escapeLiteral(soil_value) + "\" . \n");
			}
		}
		
		//weather
		List<AceWeather> stations = dataset.getWeathers();
		for (AceWeather station : stations) {
			String wid = station.getId();
			
			String station_uri = "<http://semagrow.eu/agmip/weather_station/" + wid + ">";
			Set<String> station_keys = station.keySet();
			for (String station_key : station_keys) {
				String station_value = station.getValue(station_key);
				writer.write(station_uri + " <http://semagrow.eu/agmip/weather_station/" + URLEncoder.encode(station_key, "UTF-8") + "> \"" + escapeLiteral(station_value) + "\" . \n");
			}
			
			//recorded years
			List<String> rec_years = station.getRecordYears();
			for (String rec_year : rec_years) {
				String year = "\"" + rec_year + "-01-01T00:00:00Z\"^^<http://www.w3.org/2001/XMLSchema#dateTime>";
				writer.write(station_uri + " <http://semagrow.eu/agmip/weather_station/recorder_year> " + year + " . \n");
			}
			
			//missing dates
			/*List<String> missing_dates = station.getMissingDates();
			for (String missing_date : missing_dates) {
				String date = "\"" + xsdDateFormat.print(agmipDateFormat.parseDateTime(missing_date)) + 
						"\"^^<http://www.w3.org/2001/XMLSchema#dateTime>";
				writer.write(station_uri + " <http://semagrow.eu/agmip/weather_station/recorder_year> " + date + " . \n");
			}*/// aceb lib uses joda time and it throws exception when trying to transform date
			
			//daily weather
			AceRecordCollection daily_weather_collection = station.getDailyWeather();
			for (AceRecord daily_weather : daily_weather_collection) {
				Set<String> daily_weather_keys = daily_weather.keySet();
				String uuid = UUID.randomUUID().toString().replaceAll("-", "");
				String daily_uri = "<http://semagrow.eu/agmip/daily_record/" + uuid + ">";
				writer.write(station_uri + " <http://semagrow.eu/agmip/hasRecord> " + daily_uri + " . \n");
				for (String key : daily_weather_keys) {
					String value = daily_weather.getValue(key);
					if (key.equals("w_date")) {
						String date = "\"" + value.substring(0, 4) + "-" + value.substring(4, 6) + "-" 
								+ value.substring(6, 8) + "T00:00:00Z\"^^<http://www.w3.org/2001/XMLSchema#dateTime>";
						writer.write(daily_uri + " <http://semagrow.eu/agmip/record_date> " + date + " . \n");
					}
					writer.write(daily_uri + " <http://semagrow.eu/agmip/record/" + URLEncoder.encode(key, "UTF-8") + "> \"" + escapeLiteral(value) + "\" . \n");
				}
			}
			
		}
		
	}
	
	
	/**
	 * 
	 * Creates a JSON summary of the dataset and exports triples 
	 * <http://www.semagrow.eu/agmip/experiment/experiment_id>" <http://semagrow.eu/agmip/experiment/json> "json"^^<http://www.w3.org/2001/XMLSchema#string>
	 * 
	 * @throws IOException
	 */
	public void exportJSON(final Writer writer) throws IOException {
		
		Set<String> triples = new HashSet<>();
		
		List<AceExperiment> experiments = dataset.getExperiments();
		for (AceExperiment exp : experiments) {
			
			JsonObjectBuilder json = Json.createObjectBuilder();
			
			//add vars
			JsonArrayBuilder vars = Json.createArrayBuilder();
					
			json.add("src", "agmip");
			json.add("src_id", UUID.randomUUID().toString());
			json.add("downloadable", "true");
			json.add("link", "https://data.agmip.org/cropsitedb");
			json.add("eid", exp.getId());
			json.add("exname", exp.getValueOr("exname", ""));
			json.add("institution", exp.getValueOr("institution", ""));
			json.add("fl_lat", exp.getValueOr("fl_lat", ""));
			json.add("fl_long", exp.getValueOr("fl_long", ""));
			json.add("site_name", exp.getValueOr("site_name", ""));
			json.add("title", exp.getValueOr("exname", ""));
			
			//description
			String local_id = "local_id:" + exp.getValueOr("local_id", "");
			String local_name = "local_name:" + exp.getValueOr("local_name", "");
			String objectives = "objectives:" + exp.getValueOr("objectives", "");
			String main_factor = "main_factor:" + exp.getValueOr("main_factor", "");
			String factors = "factors:" + exp.getValueOr("factors", "");
			json.add("description", local_id + ", " + local_name + ", " + objectives  + ", " + 
					main_factor + ", " +  factors );
			
			List<AceEvent> events = exp.getEvents().asList();
			for (AceEvent event : events) {
				if (event.getEventType().equals(AceEventType.ACE_PLANTING_EVENT)) {
					String crid = event.getValue("crid");
					if (crid != null) {
						json.add("crid", crid);
						//find human name
						String crid_text = LookupCodes.lookupCode("crid", crid, "crid");
						if (crid_text == null) crid_text = "";
						json.add("crid_text", crid_text);
					} else {
						json.add("crid", "");
						json.add("crid_text", "");
					}
					//cultivar
					String cul_name = event.getValueOr("cul_name", "");
					json.add("cul_name", cul_name);
					//planting date
					String date = event.getValueOr("date", "");
					json.add("date", date);
					
					break;
				}
			}
			
			if ( ! events.isEmpty()) {
				
				String start = events.get(0).getValueOr("date", "");
				String end = events.get(events.size() - 1).getValueOr("date", "");
				
				json.add("exp_starts", start);
				json.add("exp_ends", end);
				
				JsonObjectBuilder jtime = Json.createObjectBuilder();
				jtime.add("short_name", "time");
				jtime.add("long_name", "time");
				jtime.add("units", "datetime");
				jtime.add("from", start);
				jtime.add("to", end);
				vars.add(jtime);
				
			} else {
				json.add("exp_starts", "");
				json.add("exp_ends", "");
			}
			
			
			JsonObjectBuilder jlon = Json.createObjectBuilder();
			jlon.add("short_name", "fl_long");
			jlon.add("long_name", "Field longitude, E positive,W negative");
			jlon.add("units", "degree angle");
			jlon.add("from", exp.getValueOr("fl_long", ""));
			jlon.add("to", exp.getValueOr("fl_long", ""));
			vars.add(jlon);
			
			JsonObjectBuilder jlat = Json.createObjectBuilder();
			jlat.add("short_name", "fl_lat");
			jlat.add("long_name", "Field latitude");
			jlat.add("units", "degree angle");
			jlat.add("from", exp.getValueOr("fl_lat", ""));
			jlat.add("to", exp.getValueOr("fl_lat", ""));
			vars.add(jlat);
			
			List<Double> temps_min = new ArrayList<Double>();
			List<Double> temps_max = new ArrayList<Double>();
			List<Double> rains = new ArrayList<Double>();
			List<Double> srads = new ArrayList<Double>();
			List<Double> winds = new ArrayList<Double>();
			List<Double> dewps = new ArrayList<Double>();
			List<Double> vprsds = new ArrayList<Double>();
			List<Double> rhumds = new ArrayList<Double>();
			
			List<AceWeather> stations = dataset.getWeathers();
			for (AceWeather station : stations) {
				//daily weather
				AceRecordCollection daily_weather_collection = station.getDailyWeather();
				for (AceRecord daily_weather : daily_weather_collection) {
					
					String min = daily_weather.getValue("tmin");
					if (min != null) temps_min.add(new Double(min));
					
					String max = daily_weather.getValue("tmax");
					if (max != null) temps_max.add(new Double(max));
					
					String rain = daily_weather.getValue("rain");
					if (rain != null) rains.add(new Double(rain));
					
					String srad = daily_weather.getValue("srad");
					if (srad != null) srads.add(new Double(rain));
					
					String wind = daily_weather.getValue("wind");
					if (wind != null) winds.add(new Double(srad));
					
					String dewp = daily_weather.getValue("dewp");
					if (dewp != null) dewps.add(new Double(dewp));				
					
					String vprsd = daily_weather.getValue("vprsd");
					if (vprsd != null) vprsds.add(new Double(vprsd));
					
					String rhumd = daily_weather.getValue("rhumd");
					if (rhumd != null) rhumds.add(new Double(rhumd));
					
					
					
				}
			}
			
			if ( ! rains.isEmpty() ) {
				
				Object[] arr = rains.toArray();
				Arrays.sort(arr);
				
				JsonObjectBuilder j = Json.createObjectBuilder();
				j.add("short_name", "rain");
				j.add("long_name", "Rainfall, including moisture in snow, in one day");
				j.add("units", "mm/d");
				j.add("from", arr[0].toString());
				j.add("to", arr[arr.length - 1].toString());
				vars.add(j);
			}
			
			if ( ! temps_min.isEmpty() ) {
				
				Object[] arr = temps_min.toArray();
				Arrays.sort(arr);
				
				JsonObjectBuilder j = Json.createObjectBuilder();
				j.add("short_name", "tmin");
				j.add("long_name", "Temperature of air, minimum");
				j.add("units", "degrees C");
				j.add("from", arr[0].toString());
				j.add("to", arr[arr.length - 1].toString());
				vars.add(j);
				
			}
			
			if ( ! temps_max.isEmpty() ) {
				
				Object[] arr = temps_max.toArray();
				Arrays.sort(arr);
				
				JsonObjectBuilder j = Json.createObjectBuilder();
				j.add("short_name", "tmax");
				j.add("long_name", "Temperature of air, maximum");
				j.add("units", "degrees C");
				j.add("from", arr[0].toString());
				j.add("to", arr[arr.length - 1].toString());
				vars.add(j);
				
			}
			
			if ( ! srads.isEmpty() ) {
				
				Object[] arr = srads.toArray();
				Arrays.sort(arr);
				
				JsonObjectBuilder j = Json.createObjectBuilder();
				j.add("short_name", "srad");
				j.add("long_name", "Solar radiation");
				j.add("units", "MJ/m2.d");
				j.add("from", arr[0].toString());
				j.add("to", arr[arr.length - 1].toString());
				vars.add(j);
				
			}

			if ( ! winds.isEmpty() ) {
				
				Object[] arr = winds.toArray();
				Arrays.sort(arr);
				
				JsonObjectBuilder j = Json.createObjectBuilder();
				j.add("short_name", "wind");
				j.add("long_name", "Wind speed, daily");
				j.add("units", "km/d");
				j.add("from", arr[0].toString());
				j.add("to", arr[arr.length - 1].toString());
				vars.add(j);
				
			}

			if ( ! dewps.isEmpty() ) {
				
				Object[] arr = dewps.toArray();
				Arrays.sort(arr);
				
				JsonObjectBuilder j = Json.createObjectBuilder();
				j.add("short_name", "dewp");
				j.add("long_name", "Temperature, dewpoint, daily average");
				j.add("units", "degrees C");
				j.add("from", arr[0].toString());
				j.add("to", arr[arr.length - 1].toString());
				vars.add(j);
				
			}
			
			if ( ! vprsds.isEmpty() ) {
				
				Object[] arr = vprsds.toArray();
				Arrays.sort(arr);
				
				JsonObjectBuilder j = Json.createObjectBuilder();
				j.add("short_name", "vprsd");
				j.add("long_name", "Vapor pressure");
				j.add("units", "kPa");
				j.add("from", arr[0].toString());
				j.add("to", arr[arr.length - 1].toString());
				vars.add(j);
				
			}
			
			if ( ! rhumds.isEmpty() ) {
				
				Object[] arr = rhumds.toArray();
				Arrays.sort(arr);
				
				JsonObjectBuilder j = Json.createObjectBuilder();
				j.add("short_name", "rhumd");
				j.add("long_name", "Relative humidity at TMIN (or max rel. hum)");
				j.add("units", "%");
				j.add("from", arr[0].toString());
				j.add("to", arr[arr.length - 1].toString());
				vars.add(j);
				
			}
			
				
			json.add("vars", vars);
			triples.add("<http://www.semagrow.eu/agmip/experiment/" + exp.getId() + "> "
					+ "<http://semagrow.eu/agmip/experiment/json> "
					+ "\"" + StringEscapeUtils.escapeXml10(json.build().toString()) + 
					"\"^^<http://www.w3.org/2001/XMLSchema#string> . \n");
		}
		
		for (String triple : triples) {
			writer.write(triple);
		}
	
	}
	
	/**
	 * 
	 * creates links from AgroVoc to this dataset using variable names and crops contained 
	 * and exports triples <http://www.semagrow.eu/agmip/experiment/experiment_id> <http://purl.org/dc/terms/subject> <agrovoc_URI>
	 * 
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public void exportAgroVoc(final Writer writer) throws IOException, URISyntaxException {
		
		Set<String> triples = new HashSet<>(); 
		
		List<AceExperiment> experiments = dataset.getExperiments();
		for (AceExperiment exp : experiments) {
			
			String eid = exp.getId();
			String ex_uri = "<http://www.semagrow.eu/agmip/experiment/" + eid + ">";
			
			URL resourceUrl = AgmipToRDF.class.getResource("/crop_codes_to_agrovoc.txt");
			Path path = Paths.get(resourceUrl.toURI());
			
			List<String> lines = Files.readAllLines(path, Charset.defaultCharset() );
			Map<String, String> crop_map = new HashMap<>();
			for (String line : lines) {
				String[] splitted = line.split(",");
				crop_map.put(splitted[0].toLowerCase(), splitted[2]);
			}
			
			//match crops to agrovoc
			List<AceEvent> events = exp.getEvents().asList();
			for (AceEvent event : events) {
				//search all events for now
				//if (event.getEventType().equals(AceEventType.ACE_PLANTING_EVENT)) {
					String crid = event.getValue("crid");
					if (crid != null) {
						String agrovoc = crop_map.get(crid.toLowerCase());
						if (agrovoc != null) {
							triples.add(ex_uri + " <http://purl.org/dc/terms/subject> <" + agrovoc + "> . \n");
						}
						else {//find human name and check agrovoc
							String crid_text = LookupCodes.lookupCode("crid", crid, "crid");
							if (crid_text != null){
								//find matching agrovoc uris
								List<String> crop_uris = SearchUtils.findAgroVoc(crid_text);
								for (String uri : crop_uris) {//if it matches agrovoc write it
									triples.add(ex_uri + " <http://purl.org/dc/terms/subject> <" + uri + "> . \n");
								}	
							}	
						}
					}
				//}	
			}
			
		}
		
		//weather
		URL resourceUrl = AgmipToRDF.class.getResource("/icasa_variables_to_agrovoc.txt");
		Path path = Paths.get(resourceUrl.toURI());
		
		List<String> lines = Files.readAllLines(path, Charset.defaultCharset() );
		Map<String, String> vars_map = new HashMap<>();
		for (String line : lines) {
			String[] splitted = line.split(",");
			vars_map.put(splitted[0].toLowerCase(), splitted[1]);
		}

		List<String> exp_uris = new ArrayList<String>();
		for (AceExperiment exp : dataset.getExperiments()) {
			exp_uris.add("<http://www.semagrow.eu/agmip/experiment/" + exp.getId() + ">");
		}
		
		for (AceWeather station : dataset.getWeathers()) {
			for (AceRecord daily_weather : station.getDailyWeather()) {
				for (String key : daily_weather.keySet()) {
					String agrovoc = vars_map.get(key);
					if (agrovoc != null) {
						for (String exp_uri : exp_uris) {
							triples.add(exp_uri + " <http://purl.org/dc/terms/subject> <" + agrovoc + "> . \n");
						}
					}
				}
			}		
		}
		
		for (String triple : triples) {
			writer.write(triple);
		}
		
	}
	
	private String escapeLiteral(String value) {
		if (value != null) {
			return StringEscapeUtils.escapeXml10(value.replaceAll("\\r", " ").replaceAll("\\n", " "));
		} else {
			return null;
		}
		
	}

	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	public static void main(String[] args) throws IOException, URISyntaxException {
		
		File aceb = new File("/home/gmouchakis/ace.aceb");
		File aceb1 = new File("/home/gmouchakis/ace1.aceb");
		File aceb2 = new File("/home/gmouchakis/ace2.aceb");
		File aceb3 = new File("/home/gmouchakis/ace3.aceb");
		File aceb4 = new File("/home/gmouchakis/ace4.aceb");
		
		BufferedWriter rdf = null;
		BufferedWriter json = null;
		BufferedWriter agrovoc = null;
		
		try {
						
			rdf = new BufferedWriter(new FileWriter("/home/gmouchakis/agmip.ttl"));
			json = new BufferedWriter(new FileWriter("/home/gmouchakis/agmip_json.ttl"));
			agrovoc = new BufferedWriter(new FileWriter("/home/gmouchakis/agmip_agrovoc.ttl"));
			
			AgmipToRDF transform = new AgmipToRDF(aceb);
			transform.triplify(rdf);
			transform.exportJSON(json);
			transform.exportAgroVoc(agrovoc);
			
			transform = new AgmipToRDF(aceb1);
			transform.triplify(rdf);
			transform.exportJSON(json);
			transform.exportAgroVoc(agrovoc);
			
			transform = new AgmipToRDF(aceb2);
			transform.triplify(rdf);
			transform.exportJSON(json);
			transform.exportAgroVoc(agrovoc);
			
			transform = new AgmipToRDF(aceb3);
			transform.triplify(rdf);
			transform.exportJSON(json);
			transform.exportAgroVoc(agrovoc);
			
			transform = new AgmipToRDF(aceb4);
			transform.triplify(rdf);
			transform.exportJSON(json);
			transform.exportAgroVoc(agrovoc);
			
		} finally {
			if (rdf != null) rdf.close();
			if (json != null) json.close();
			if (agrovoc != null) agrovoc.close();
		}
	}

}
