/**
 * 
 */
package gr.demokritos.iit.agmip;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

import gr.demokritos.iit.netcdftoolkit.commons.SearchUtils;
import nl.wur.alterra.semagrow.file.SemagrowQueryBlock;

/**
 * @author Giannis Mouchakis
 *
 */
public class AgmipMetadataSearch implements Runnable {
	
	static Logger logger = LoggerFactory.getLogger(AgmipMetadataSearch.class);
	
	final private String endpoint;
	final private List<String> keywords_no_agrovoc;
	final private Map<String, Set<String>> agrovoc_map;
	final private boolean and;
	final private int limit;
	final private SemagrowQueryBlock semagrowQueryBlock;
	private List<String> results;
	

	/**
	 * @param endpoint
	 * @param keywords_no_agrovoc
	 * @param agrovoc_map
	 * @param and
	 * @param limit
	 * @param semagrowQueryBlock
	 * @param results
	 */
	public AgmipMetadataSearch(final String endpoint, final List<String> keywords_no_agrovoc, 
			final Map<String, Set<String>> agrovoc_map, final boolean and, final int limit, 
			final SemagrowQueryBlock semagrowQueryBlock, List<String> results) {
		this.endpoint = endpoint;
		this.keywords_no_agrovoc = keywords_no_agrovoc;
		this.agrovoc_map = agrovoc_map;
		this.and = and;
		this.limit = limit;
		this.semagrowQueryBlock = semagrowQueryBlock;
		this.results = results;
	}

	@Override
	public void run() {
				
		query();
	}
	
	public void query() {
		
		String query_str = "PREFIX strdf: <http://strdf.di.uoa.gr/ontology#> "
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> "
				+ "SELECT DISTINCT ?json "
				+ "{ ";
		
		if (and) { //AND Operator
			
			query_str += "?dataset <http://semagrow.eu/agmip/experiment/fl_lat> ?fl_lat . "
					+ "?dataset <http://semagrow.eu/agmip/experiment/fl_long> ?fl_long . "
					+ "?dataset <http://www.w3.org/2003/01/geo/wgs84_pos#geometry> ?exp_point . "
					+ "?dataset <http://semagrow.eu/agmip/experiment/json> ?json . "
					+ "?dataset <http://semagrow.eu/agmip/experiment/minTimeValue> ?min_value . "
					+ "?dataset <http://semagrow.eu/agmip/experiment/maxTimeValue> ?max_value . "
					+ SearchUtils.createANDFilter(agrovoc_map, keywords_no_agrovoc, " ?dataset ?p ?header . ")
					+ createTimeFilter(semagrowQueryBlock)
					+ createGeoFilter(semagrowQueryBlock);
			
		} else { //OR Operator
			
			boolean union = false;
			if ( ! agrovoc_map.isEmpty() &&  ! keywords_no_agrovoc.isEmpty()) {
				union = true;
			}
			
			if (union) query_str += " { ";
			
			if ( ! agrovoc_map.isEmpty() ) {
				
				query_str += "?dataset <http://semagrow.eu/agmip/experiment/fl_lat> ?fl_lat . "
						+ "?dataset <http://semagrow.eu/agmip/experiment/fl_long> ?fl_long . "
						+ "?dataset <http://www.w3.org/2003/01/geo/wgs84_pos#geometry> ?exp_point . "
						+ "?dataset <http://semagrow.eu/agmip/experiment/json> ?json . "
						+ "?dataset <http://semagrow.eu/agmip/experiment/minTimeValue> ?min_value . "
						+ "?dataset <http://semagrow.eu/agmip/experiment/maxTimeValue> ?max_value . "
						+ SearchUtils.createAgroVocFilter(agrovoc_map)
						+ createTimeFilter(semagrowQueryBlock)
						+ createGeoFilter(semagrowQueryBlock);
			}
			
			if (union) query_str += " } UNION { ";
			
			if ( ! keywords_no_agrovoc.isEmpty() ) {
				
				query_str += "?dataset <http://semagrow.eu/agmip/experiment/fl_lat> ?fl_lat . "
						+ "?dataset <http://semagrow.eu/agmip/experiment/fl_long> ?fl_long . "
						+ "?dataset <http://www.w3.org/2003/01/geo/wgs84_pos#geometry> ?exp_point . "
						+ "?dataset <http://semagrow.eu/agmip/experiment/json> ?json . "
						+ "?dataset <http://semagrow.eu/agmip/experiment/minTimeValue> ?min_value . "
						+ "?dataset <http://semagrow.eu/agmip/experiment/maxTimeValue> ?max_value . "
						+ SearchUtils.createKeywordFilter(keywords_no_agrovoc, " ?dataset ?p ?header . " )
						+ createTimeFilter(semagrowQueryBlock)
						+ createGeoFilter(semagrowQueryBlock);
				
			}
			
			if (union) query_str += " } ";
			
		}
		
		query_str += " } ";
		
		if (limit > 0) {
			query_str += "LIMIT "+ limit;
		}
		
		Query query = QueryFactory.create(query_str);
		ARQ.getContext().setTrue(ARQ.useSAX);
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);			
		ResultSet resultSet = qexec.execSelect();				
		while (resultSet.hasNext()) {
			QuerySolution soln = resultSet.next();			
			RDFNode json_n = soln.get("json");
			String json = "";
			if (json_n.isLiteral()) {
				json = StringEscapeUtils.unescapeXml(json_n.asLiteral().getString());
			} else if (json_n.isResource()) {
				json = StringEscapeUtils.unescapeXml(json_n.asResource().toString());
			} else {
				logger.error("Could not handle node"+json_n.toString());
			}
			results.add(json);
		}	
		qexec.close();
		
	}
	
	
	private String createTimeFilter(SemagrowQueryBlock semagrowQueryBlock) {
		//transform
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String from_date = df.format(semagrowQueryBlock.getFrom());
		String to_date = df.format(semagrowQueryBlock.getTo());
		
		String filter = "FILTER ((?min_value >= \""+from_date+"\"^^xsd:dateTime &&"
				+ "?max_value <= \""+to_date+"\"^^xsd:dateTime) "
				+ "||"
				+ "(?min_value >= \""+from_date+"\"^^xsd:dateTime &&"
				+ "?min_value <= \""+to_date+"\"^^xsd:dateTime)"
				+ "||"
				+ "(?max_value >= \""+from_date+"\"^^xsd:dateTime &&"
				+ "?max_value <= \""+to_date+"\"^^xsd:dateTime) "
				+ "||"
				+ "(?min_value <= \""+from_date+"\"^^xsd:dateTime &&"
				+ "?max_value >= \""+to_date+"\"^^xsd:dateTime)"
				+ ") . ";
		
		return filter;
	}
	
	private String createGeoFilter(SemagrowQueryBlock semagrowQueryBlock) {
		
		String south = semagrowQueryBlock.getSouth().toString();
		String north = semagrowQueryBlock.getNorth().toString();
		String west = semagrowQueryBlock.getWest().toString();
		String east = semagrowQueryBlock.getEast().toString();
		
		String filter = "FILTER(strdf:within(?exp_point, \"POLYGON(("
		+east+" "+north+", "+west+" "+north+", "+west+" "+south+", "+east+" "+south+", "+east+" "+north+
		"))\"^^strdf:WKT)) . ";
		
		return filter;
	}

}
