Instructions on how to install and use the SemaGrow Stack and the rdfCDF Toolkit.


SemaGrow

SemaGrow is available as a .deb distribution. To install on Debian based system you need to execute the following on a terminal:

# add debian sources and key
echo 'deb http://semagrow.semantic-web.at/deb/ lucid free' > /etc/apt/sources.list.d/semagrow.list
wget -q http://semagrow.semantic-web.at/deb/packages.semagrow.key -O- | apt-key add -

# install SemaGrow and dependencies
apt-get update
apt-get install openjdk-7-jdk semagrow

To start the SemaGrow endpoint, issue:

service semagrow start
service semagrow status

At this point, the SemaGrow stack should be up and running and the output of the “service semagrow status” command should be “[ ok ] SemaGrow Stack is running with pid ****.”.
To access SemaGrow Stack endpoint through a browser, enter the following url: http://<ip>:8080/SemaGrow, where <ip> is the IP of your computer. By default, SemaGrow uses the 8080 port.
On the "Getting Started" tab you will find instructions on how to use the stack.



rdfCDF Toolkit

With the rdfCDF Toolkit you can convert a NetCDF file to RDF and combine converted NetCDF files after applying a set of conditions to get a new NetCDF file.

To produce a turtle (TTL) file from NetCDF you can use the gr.demokritos.iit.netcdftoolkit.loader.NetCDFToRDF class. An example can be found at gr.demokritos.iit.netcdftoolkit.ProduceTTLExample
Then the produced file can be imported in a triple store and queried. To query through SemaGrow use the metadata.ttl provided in resources folder and follow the instruction above to add a new source under SemaGrow. Note that you must insert your endpoint in line 196 and 522 of the metadata.ttl file.

There is also the option to use the NetCDFStore which is a Sesame SAIL (http://rdf4j.org/sesame/2.7/docs/system.docbook?view#chapter-sailapi). When using the SAIL the NetCDF file is transformed to RDF on the fly each time a query is made.
An example on how to set up a NetCDFStore repository can be found at gr.demokritos.iit.netcdftoolkit.loader.NetCDFStoreExample.
To use the NetCDFStore under the SemaGrow Stack you must first set up a NetCDFStore repository on a openrdf-workbench server. To do so follow the instructions at resources/INSTALL.md and again use the resources/metadata.ttl file.

At gr.demokritos.iit.netcdftoolkit.CreatorExample you can find an example on how to query the transformed file, apply filters and produce a NetCDF file that is a subset of the original file.